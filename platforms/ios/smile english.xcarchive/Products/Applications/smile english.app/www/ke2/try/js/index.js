var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        // 画面を横にロック
        this.receivedEvent('deviceready');

    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {
        screen.lockOrientation('landscape');
        progress.use('try');
        setInterval(function () {
            $(".contents").addClass("active");
        }, 300);

        /*
         * sound set
         */
        var path;
        if (device.platform == 'iOS') {
            path = "ke2/try/sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke2/try/sound/";
        }

        var mediaSuccess = function () {
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if (status != Media.MEDIA_NONE) {
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if (status == Media.MEDIA_STARTING) {
                // 再生が開始されたタイミング
                console.log("media: starting");
            }
            if (status == Media.MEDIA_RUNNING) {
                // 再生が開始されたタイミング
                console.log("media: running");
            }
            if (status == Media.MEDIA_PAUSED) {
                // 再生が開始されたタイミング
                console.log("media: paused");
            }
            else if (status == Media.MEDIA_STOPPED) {
                // 停止された時の処理
                console.log("media: stopped");
            }
            console.log('==============================================');
        }

        var sound = [];
        sound[1] = new Media(path + "make01.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[2] = new Media(path + "make02.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[3] = new Media(path + "make03.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[4] = new Media(path + "make04.mp3", mediaSuccess, mediaError, mediaStatus);

        sound[1].play();
        $("#step01").on('swipeleft', function(){
            $('body').pagecontainer('change', '#step02', {transition: 'slide'});
        });

        $(document).on('pagecontainerchange', function (e, d) {
            if (d.toPage[0].id == 'step02') {
                sound[1].stop();
                sound[1].release();

                sound[2].play(); //play sound

                $("#step02").on('swipeleft', function(){
                    $('body').pagecontainer('change', '#step03', {transition: 'slide'});
                });
            }
            if (d.toPage[0].id == 'step03') {
                sound[2].stop();
                sound[2].release();

                sound[3].play(); //play sound

                $("#step03").on('swipeleft', function(){
                    $('body').pagecontainer('change', '#step04', {transition: 'slide'});
                });

            }
            if (d.toPage[0].id == 'step04') {
                sound[3].stop();
                sound[3].release();

                sound[4].play(); //play sound

                $("#step04").on('swipeleft', function(){
                    location.href = 'main.html';
                });
            }


        });

    }
};

app.initialize();