/**
 * Created by mizo on 2017/03/18.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        // 画面を横にロック
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        screen.lockOrientation('landscape');
        setInterval(function(){
            $("#decoratin").addClass("active");
        }, 300);

    }
};

app.initialize();

var draggable, droppable;

(function($){
    $( function() {

        var berry_count = $(".berry").length;
        var droped_count = 0

        $(".wrap.btn").on("touchstart", function(){
            $(this).addClass("on");
        });
        $(".wrap.btn").on("touchend", function(){
            $(this).removeClass("on");
            $(".berry").addClass("off");
            $(".board").addClass("flip");
            $(this).removeClass("active");
            $(".more.btn").addClass("active");

            $(".title img").toggleClass("active");

            $(".crepe_board").removeClass("active");
            $(".crepe_dish").addClass("active");

            $(".berry img").removeAttr("style");
            $(".berry img").removeClass("dropped");
            draggable.draggable( "destroy" );
            droppable.droppable( "destroy" );
            console.log($(".berry img"));
        });

        $(".more.btn").on("touchstart", function(){
            $(this).addClass("on");
        });
        $(".more.btn").on("touchend", function(){
            add_drag_event();
            $(this).removeClass("on");

            $(".crepe_dish").removeClass("active");
            $(".crepe_board").addClass("active");

            $(".title img").toggleClass("active");

            $(".berry").removeClass("off");
            $(".board").removeClass("flip");
            $(this).removeClass("active");
            droped_count = 0;
        });

         var add_drag_event = function(){
             draggable = $( ".berry .pick" ).draggable({
                revert: "invalid",
                stack: '.berry',
                drag: function( event, ui ) {
                    var z = $(this).css('z-index');
                    $(this).parents('.berry').css('z-index', z);
                }
            });

             droppable = $( "#droppable" ).droppable({
                classes: {
                    // "ui-droppable-active": "ui-state-active",
                    // "ui-droppable-hover": "ui-state-hover"
                },
                drop: function( event, ui ) {
                    $(ui.helper).addClass("dropped");
                    //console.log($(ui.helper));

                    droped_count = $("img.dropped").length;
                    if(droped_count >= berry_count){
                        $(".btn.wrap").addClass("active");
                    }
                }
            });
        }

        add_drag_event();

    } );
})(jQuery);