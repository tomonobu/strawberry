(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:



(lib.基本画面 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#898819").s().p("AgsAtQgTgSAAgbQAAgZATgTQASgTAaAAQAaAAAUATQASATAAAZQAAAbgSASQgUATgaAAQgaAAgSgTg");
	this.shape.setTransform(-238.9,288.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#898819").s().p("AgsAtQgTgSAAgbQAAgZATgTQASgTAaAAQAaAAATATQATATAAAZQAAAbgTASQgTATgaAAQgZAAgTgTg");
	this.shape_1.setTransform(-236.1,271.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#898819").s().p("AgtAuQgSgUAAgaQAAgaASgSQATgTAaAAQAbAAASATQATASAAAaQAAAagTAUQgSASgbAAQgaAAgTgSg");
	this.shape_2.setTransform(-251.3,277.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FED831").s().p("AiBEGQAOgsAeg2IAbgsQhIAthQAPQigAfgqiRQgpiPCmgTQBUgKBbATQhGgagygpQhkhTBphOQBphNBUB6QAoA8AVBNQAIhXAbhLQA2iWBeA7QBdA8hCCMQghBGg0A6QAPgOAXgRQAtgjAogSQB/g6AVCEQAUCEiPAOQgsAFg3gIQAqAKAeANQBpAxhSBSQhSBShPg9QgYgTgWgfIgQgcIAIAkQAHAsgEAkQgOBzh8AAQh3AAAwiNg");
	this.shape_3.setTransform(-243,278.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7E6CDD").s().p("Ai1KnQhAgYgxgtQgzgxgTg7QgriHCEiDQilCZiPgoQg/gRgvg1QgsgxgUhFQgUhEAJhDQAJhGAngyQBfh4DVAvQjrhDgZiTQgKg+Aeg/QAdg8A4guQA6gvBEgSQBJgUBCATQCMAnA/C4QgfiJBShBQApggA/gHQA7gGBAARQCPAnArBnQAuBvh4B3QCkiWCNArQA+ASAtA2QAqAzARBFQARBEgNBBQgPBFgtAtQhvBvjqhDQC7A+AQB9QAOBqhoBbQhoBahtgUQh5gWgoidQAzDfhkBbQgrAnhAAGIgYABQgxAAg0gTg");
	this.shape_4.setTransform(-243.7,282);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F07668").s().p("AgmFZIkNndQgNgWAHgaQAGgZAWgMID5iNQAWgMAZAIQAaAHANAXIEDHKQAMAXgIALQgJAKgYgIIisg5QgYgIgVAMQgUAMgFAZIgjDCQgDAZgNACIgBAAQgLAAgNgWg");
	this.shape_5.setTransform(-216.2,344.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F07668").s().p("AA5FkQgNgBgIgYIg2itQgIgYgWgKQgWgJgXALIixBWQgXAMgIgKQgJgJALgXIDjnzQALgXAYgKQAZgKAVAKIEFB3QAXAKAJAaQAJAZgLAYIjaHfQgLAXgNAAIgBAAg");
	this.shape_6.setTransform(-282.4,338.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("ANBOKQm3igmPlLQmPFKm6ChQqJDtppi8QoBidnanVQlulqlOodIAAnPQMxJGO5E1QPZE/QUAAQQXAAPbk/QO+k2MopFIAAGqQlEIil2F5QnhHjoMCgQkSBTkXAAQlfAAloiEg");
	this.shape_7.setTransform(0.1,281.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#25C42F").s().p("Eg7SAlWMAAAhKrQMxJGO3E0QPYE/QSAAQQVAAPZk/QO+k2MnpEMAAABKrg");
	this.shape_8.setTransform(-0.4,431);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FD6F71").s().p("AFIOuQi+AAithLQinhHiBiDQiCiDhGipQhKivAAi+QAAi+BKivQBHioCBiDQCBiCCohJQCthKC+AAQBMAABKALQBOALAxAVIAAb5Qg3AXhJAPQhRARhEAAg");
	this.shape_9.setTransform(319.5,-62.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FD6F71").s().p("AnOOgQhMgSgugJIAA8YQBEgOAzgFQA0gFA6gBQC+ABCuBJQCpBICCCCQCDCCBHCpQBLCuAAC+QAAC+hJCvQhICoiCCDQiBCDioBIQitBLi+gBQgxAAg/gMg");
	this.shape_10.setTransform(-321.4,-62);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#8D2A7E").s().p("AEjFhQnAgcirkeQg2hZgThpIgJhZQACgvAjggQAigfAvACQAvACAfAiQAgAigCAuQgBBhBBBXQB4CgExATQAvADAfAjQAfAjgDAvQgDAtghAfQggAegtAAg");
	this.shape_11.setTransform(311.9,-618.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#8D2A7E").s().p("AIbIzQoohZl2m3Qh3iLhXigQgvhYgWg7QgRgsAUgqQATgrAsgQQAsgRAqAUQArATAQAsQAUA0AoBJQBLCJBmB3QE/F1HWBMQAuAIAcAmQAbAmgHAuQgHApggAbQggAbgpAAg");
	this.shape_12.setTransform(269.7,-584);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#8D2A7E").s().p("AtFJqQgfgZgKgmQgLguAZgoQAYgoAugLQIJh8INnGQEIjkCjjRQAdgkAugGQAvgGAlAcQAlAdAGAuQAGAvgdAlQi6DukbDxQouHgo9CJQgMAEgOAAQgnAAgegYg");
	this.shape_13.setTransform(-226,-576.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CA1696").s().p("EAp5AKMQiWi1hzjUQgjhCgdg+IgVgwQkbEPkkAiQjqAcjjh8QikhZiQicQgsgvgngxIgcgpQkjEBkmARQjsANjhiOQiihjiOioQgsg0glg1IgcgrQt5N4zkHXQmHCUl8BXQi+AthwAOMgACgoWMB2vgADMgAHAoZQp8g8ncpFg");
	this.shape_14.setTransform(-0.1,-540.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-380.1,-670,760.2,1340);


(lib.me_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F3817").s().p("AiNAxIiKgFQh7gFh5gKIhigKIhMgJIhBgKICNgTIBigJQB0gKCAgGICKgDQBGgCBHAAQBIAABHACICJADQCCAGByAKIBiAJQAfADAsAHIBCAJIhCAKIhLAJIhiAKQh0AKiAAFIiJAFIiPABIiNgBg");
	this.shape.setTransform(174.9,32.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5F3817").s().p("AiNAxIiKgFQh7gFh5gKIhigKIhMgJIhBgKIDvgcQB0gKCAgGICKgDQBGgCBHAAQBIAABHACICJADQCCAGByAKIBiAJQAfADAsAHIBCAJIhCAKIhLAJIhiAKQh0AKiAAFIiJAFIiPABIiNgBg");
	this.shape_1.setTransform(-175.2,32.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5F3817").s().p("Ag9g2QgTgrgMgqIgIgiICtAAQghB9AbB7QAOA+AUAlQhnhcg7iIg");
	this.shape_2.setTransform(210.1,50.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5F3817").s().p("AhNAEQgVgtgMg3IgIgvICYAAQgcCcA2BRQAaAoAhAJIgLABQh4AAhBiMg");
	this.shape_3.setTransform(228.1,47.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5F3817").s().p("Ah4ALQgkg2gZhDIgQg4ICBAAQAaB9B6BzQA+A6A4AhQgUACgUAAQisAAhqicg");
	this.shape_4.setTransform(252.2,49.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5F3817").s().p("AhDBLQAch7ghh9ICtAAIgIAiQgMAqgSArQg8CIhnBcQAUglANg+g");
	this.shape_5.setTransform(-210.1,50.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#5F3817").s().p("Ah2CPQAhgJAbgoQA2hRgcicICXAAIgIAvQgMA3gVAtQhACMh5AAIgLgBg");
	this.shape_6.setTransform(-228,47.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#5F3817").s().p("AjFClQA4ghA+g6QB6hzAah9ICBAAIgQA4QgYBDglA2QhqCcirAAQgUAAgVgCg");
	this.shape_7.setTransform(-252.1,49.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E7BF32").s().p("Ar6H1QAAjHA8i6QA6iyBqiLQBqiNCIhNQCOhRCaAAQCbAACOBRQCIBNBqCNQBqCLA6CyQA8C6AADHg");
	this.shape_8.setTransform(-175.3,-17.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E7BF32").s().p("Ar6H1QAAjHA8i6QA6iyBqiLQBqiNCIhNQCOhRCaAAQCbAACOBRQCIBNBqCNQBqCLA6CyQA8C6AADHg");
	this.shape_9.setTransform(174.8,-17.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-271.9,-67.4,544,134.9);


(lib.me_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F3817").s().p("AhCBLQAbh7ghh9ICtAAIgIAiQgMAqgSArQg8CIhnBcQAVglANg+g");
	this.shape.setTransform(-212.3,16.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5F3817").s().p("AALB4Qh6hEgRjHIBxAAIgEAZQgCAhAJAiQAaBvBzBcIgRAAQgyAAgzgcg");
	this.shape_1.setTransform(224.2,13.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5F3817").s().p("AANB0QhihbgVjBICFAAIgHAsQgFA2ACAxQAHCZBTAlQgtgHgxgug");
	this.shape_2.setTransform(240,15.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5F3817").s().p("AAaBtQiKhJgri0IB6AAIAEAcQAJAjAQAjQAyBxBuBOQg8AAhGgkg");
	this.shape_3.setTransform(258.6,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5F3817").s().p("Ah2CPQAhgJAbgoQA2hRgcicICXAAIgIAvQgMA3gVAtQhACMh5AAIgLgBg");
	this.shape_4.setTransform(-230.2,13.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5F3817").s().p("AjFClQA4ghA+g6QB6hzAah9ICBAAIgQA4QgYBDglA2QhqCcirAAQgUAAgVgCg");
	this.shape_5.setTransform(-254.3,15.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E7BF32").s().p("Ar6H1QAAjHA8i6QA6iyBqiLQBqiNCIhNQCOhRCaAAQCbAACOBRQCIBNBqCNQBqCLA6CyQA8C6AADHg");
	this.shape_6.setTransform(-170.3,-51.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E7BF32").s().p("Ar6H1QAAjHA8i6QA6iyBqiLQBqiNCIhNQCOhRCaAAQCbAACOBRQCIBNBqCNQBqCLA6CyQA8C6AADHg");
	this.shape_7.setTransform(179.8,-51.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#5F3817").s().p("AkTGiQhzitAAj1QAAj0BzitQByitChAAQCiAAByCtQBzCtAAD0QAAD1hzCtQhyCtiiAAQihAAhyitg");
	this.shape_8.setTransform(179.8,23.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AkoObQiJhNhpiKQhqiLg6izQg8i6AAjMQAAjKA8i7QA6izBqiLQBpiKCJhNQCOhOCaAAQCbAACOBOQCJBNBpCKQBqCLA6CzQA8C7AADKQAADMg8C6Qg6CzhqCLQhpCKiJBNQiOBOibAAQiaAAiOhOg");
	this.shape_9.setTransform(179.8,1.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#5F3817").s().p("AkTGiQhzitAAj1QAAj0BzitQByitChAAQCiAAByCtQBzCtAAD0QAAD1hzCtQhyCtiiAAQihAAhyitg");
	this.shape_10.setTransform(-170.3,32.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AkoObQiJhNhpiKQhqiLg6izQg8i6AAjMQAAjKA8i7QA6izBqiLQBpiKCJhNQCOhOCaAAQCbAACOBOQCJBNBpCKQBqCLA6CzQA8C7AADKQAADMg8C6Qg6CzhqCLQhpCKiJBNQiOBOibAAQiaAAiOhOg");
	this.shape_11.setTransform(-170.3,1.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-274.1,-101.4,548.3,203);


(lib.me_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F3817").s().p("AkTGiQhzitAAj1QAAj0BzitQByitChAAQCiAAByCtQBzCtAAD0QAAD1hzCtQhyCtiiAAQihAAhyitg");
	this.shape.setTransform(175.1,24.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AkoObQiJhMhpiLQhqiLg6izQg8i6AAjMQAAjKA8i7QA6izBqiLQBpiKCJhNQCOhOCaAAQCbAACOBOQCJBNBpCKQBqCLA6CzQA8C7AADKQAADMg8C6Qg6CzhqCLQhpCLiJBMQiOBOibAAQiaAAiOhOg");
	this.shape_1.setTransform(175.1,24.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5F3817").s().p("AiThXQAAiCAehkQgIBQCHDpQBEB0BGBlIiLBpQiaiTgCkCg");
	this.shape_2.setTransform(127.4,-76.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5F3817").s().p("AgJh5QBAhxBFhJQgXBbgKEKQgFCGAAB0IjPAIQgSjLCCjig");
	this.shape_3.setTransform(174.4,-93.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5F3817").s().p("AiREUQBZhjBYjsQAsh2AahiQBTCOhHDbQgkBtg0BRg");
	this.shape_4.setTransform(207.1,-77.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5F3817").s().p("AkTGiQhzitAAj1QAAj0BzitQByitChAAQCiAAByCtQBzCtAAD0QAAD1hzCtQhyCtiiAAQihAAhyitg");
	this.shape_5.setTransform(-175,24.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AkoObQiJhMhpiLQhqiLg6izQg8i6AAjMQAAjKA8i7QA6izBqiLQBpiLCJhMQCOhOCaAAQCbAACOBOQCJBNBpCKQBqCLA6CzQA8C7AADKQAADMg8C6Qg6CzhqCLQhpCKiJBNQiOBOibAAQiaAAiOhOg");
	this.shape_6.setTransform(-175,24.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#5F3817").s().p("AivE4QCTgmBlkoQAziUAWiNQBDEHhVDNQgrBog4Azg");
	this.shape_7.setTransform(-146,-86.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#5F3817").s().p("Ag/DyQg0j/A9iMQAfhFAogTQgoBoAzDHQAaBkAiBQg");
	this.shape_8.setTransform(-176.7,-91.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#5F3817").s().p("AhdBwQhFjUC/i7QgbBIgCBkQgFDIB2CMIhkA/QhIhGgihqg");
	this.shape_9.setTransform(-210.6,-76.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-251.3,-124.5,502.7,249.2);


(lib.mayuge = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F3817").s().p("AlHBUQkKAeieA6QAZhWBohWQDQirGPAAQGNAADhCrQBwBWAhBWQojiSoUA6g");
	this.shape.setTransform(-175.6,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5F3817").s().p("Ak9BSQiiARiMAkIhtAfIAZg0QAkg+BAg0QDKilGCAAQGCAADZClQBtBTAgBTQoSiNoEA5g");
	this.shape_1.setTransform(177.9,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-250.8,-17.1,501.6,34.3);


(lib.kuchi2_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F07668").s().p("AzzAyQgfgIgTgaQgTgZACghQACggAWgYQAWgXAggEQGFg0HngXQPDgtKmB6QAeAGAVAXQAVAXACAfQACAfgSAYQgRAagdAJQhPAaiAAcQj9A5kdAcQj6AYj+AAQqYAApzijgApggFQIvBFIpg2QBfgJBcgMQkRgUkvAAQlWAAl9Aag");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#474747").s().p("AzcglQFhgwHogYQPPgvKhB5QhFAWh9AbQj7A4kYAcQj/AZj8AAQp8AAptigg");
	this.shape_1.setTransform(0,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-133.5,-21.3,267,42.6);


(lib.kuchi2_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F07668").s().p("A0HAAQgYgNgNgYQgNgYAEgcQADgbARgVQASgVAagHQBjgcCXggQEsg+EzgfQGjgrF9AYQH1AgGECSQAXAJAQAUQAPAUADAZQADAZgKAXQgLAXgUAOQhQA1iDA9QkCB4khA8Qj/A1kDAAQqjAAp8lbgAhoigQmwAUm7BbQFiCbFtAtQF/AuF6hPQETg5D0hyQmRhxn2AAQhsAAhxAGg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#474747").s().p("AkPDwQnxg8nckDQBYgZCVgfQErg/ExgfQPPhjKhD+QhFAvh9A5Qj7B2kZA6Qj3A0j5AAQiSAAiUgSg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-133.4,-34.6,267,69.4);


(lib.kuchi2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AmMBcQiyhDhrhtQA9gNBkgNICzgWQI2hIHJBuQhnB0i0BFQi6BHjYAAQjPAAi6hGg");
	this.shape.setTransform(3.9,-8.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F07668").s().p("AkcGGQoEhNnvlSQgVgOgKgYQgKgWADgaQADgZAQgUQAQgUAYgIQBjgjCYgoQEshPE1gnQGlg2F/AeQH4ApGGC5QAUAKAOASQANATADAWQAEAXgJAVQgIAVgRAQQhRBCiCBNQkECYkjBMQkCBDkHAAQiaAAiXgXgAiEjfQnBAdnBB8QF/DgGGA6QF7A6F3hiQEwhQEGimQmgieoTAAQh4AAiAAJg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#474747").s().p("AkOEuQnyhLnclGQBZgfCVgoQEqhOExgnQPPh9KhE/QhFA7h9BIQj7CUkYBJQj5BBj5AAQiSAAiSgWg");
	this.shape_2.setTransform(0,-0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-133.5,-41.3,267,82.6);


(lib.kuchi1_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#474747").s().p("ApqB2QmVghmChIQjCgjh0geQgMgDgGgKQgHgLADgMQADgMALgGQALgHAMADQBzAeDAAkQF/BFGSAhQIgAtIGggQKqgqI8isQAMgDALAFQALAGADAMQAEAMgGALQgGALgMAEQr8DmutAAQk7AAk/gbg");
	this.shape.setTransform(5.3,0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F07668").s().p("A6XBiQi9hChyhOQg9gqgWggQgagjABgiQAAgWALgRQALgRATgKQAdgQAsAGQEaAoGDAoQMGBPI0AAQI1AALthgQF3gwENgyQArgIAfAPQATAJAMARQALARACAWQADAhgXAlQgUAig7AuQhvBWi6BHQpIDgxUAAQxSAApQjNgA9ShbQBkA4COAvQDyBQFbAwQHIA/JWAAQHCAAF2gnQHtg0E9h0QCRg1BjhAQA+gpAiglQlJA6l9AvQrIBYocAAQoVAArThGQmWgolTgvQAlAjBDAlg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F07668").s().p("A6GA0Qi2g+hshLQg3gmgSgZQgXgiAMgTQAMgUAoAGQETAoGFAoQMLBPI8AAQI+AALyhhQF5gxEGgwQApgIANAUQAOAUgVAjQgRAbg1ApQhpBSizBEQo9DbxLAAQxKAApHjKg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-209.9,-30.3,420,60.7);


(lib.hana = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C5A228").s().p("AltEYQiZhRAAicQAAijCYhzQCYh1DWAAQDXAACYB1QCYBzAACjQAACciXBRQiIBIjmAAQjmAAiJhIg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.9,-35.2,103.8,70.5);


(lib.pakupaku_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2
	this.instance = new lib.kuchi2_2("synched",0);
	this.instance.parent = this;

	this.instance_1 = new lib.kuchi2_3("synched",0);
	this.instance_1.parent = this;

	this.instance_2 = new lib.kuchi2_1("synched",0);
	this.instance_2.parent = this;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},5).to({state:[{t:this.instance_2}]},5).to({state:[{t:this.instance_1}]},5).to({state:[{t:this.instance_2}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance_2}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance_1}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance_1}]},5).to({state:[{t:this.instance_2}]},5).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-133.4,-34.6,267,69.4);


(lib.pachipachi = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F3817").s().p("AkTGiQhzitAAj1QAAj0BzitQByitChAAQCiAAByCtQBzCtAAD0QAAD1hzCtQhyCtiiAAQihAAhyitg");
	this.shape.setTransform(175.1,24.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AkoObQiJhMhpiLQhqiLg6izQg8i6AAjMQAAjKA8i7QA6izBqiLQBpiKCJhNQCOhOCaAAQCbAACOBOQCJBNBpCKQBqCLA6CzQA8C7AADKQAADMg8C6Qg6CzhqCLQhpCLiJBMQiOBOibAAQiaAAiOhOg");
	this.shape_1.setTransform(175.1,24.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5F3817").s().p("AiThXQAAiCAehkQgIBQCHDpQBEB0BGBlIiLBpQiaiTgCkCg");
	this.shape_2.setTransform(127.4,-76.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5F3817").s().p("AgJh5QBAhxBFhJQgXBbgKEKQgFCGAAB0IjPAIQgSjLCCjig");
	this.shape_3.setTransform(174.4,-93.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5F3817").s().p("AiREUQBZhjBYjsQAsh2AahiQBTCOhHDbQgkBtg0BRg");
	this.shape_4.setTransform(207.1,-77.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5F3817").s().p("AkTGiQhzitAAj1QAAj0BzitQByitChAAQCiAAByCtQBzCtAAD0QAAD1hzCtQhyCtiiAAQihAAhyitg");
	this.shape_5.setTransform(-175,24.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AkoObQiJhMhpiLQhqiLg6izQg8i6AAjMQAAjKA8i7QA6izBqiLQBpiLCJhMQCOhOCaAAQCbAACOBOQCJBNBpCKQBqCLA6CzQA8C7AADKQAADMg8C6Qg6CzhqCLQhpCKiJBNQiOBOibAAQiaAAiOhOg");
	this.shape_6.setTransform(-175,24.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#5F3817").s().p("AivE4QCTgmBlkoQAziUAWiNQBDEHhVDNQgrBog4Azg");
	this.shape_7.setTransform(-146,-86.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#5F3817").s().p("Ag/DyQg0j/A9iMQAfhFAogTQgoBoAzDHQAaBkAiBQg");
	this.shape_8.setTransform(-176.7,-91.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#5F3817").s().p("AhdBwQhFjUC/i7QgbBIgCBkQgFDIB2CMIhkA/QhIhGgihqg");
	this.shape_9.setTransform(-210.6,-76.4);

	this.instance = new lib.me_1("synched",0);
	this.instance.parent = this;
	this.instance._off = true;

	this.instance_1 = new lib.me_3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,48);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance_1,p:{y:48}}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance_1,p:{y:47}}]},6).wait(5));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({_off:false},0).wait(5).to({startPosition:0},0).to({_off:true},5).wait(5).to({_off:false},0).wait(5).to({startPosition:0},0).wait(5).to({startPosition:0},0).wait(5).to({startPosition:0},0).wait(5).to({startPosition:0},0).wait(5).to({startPosition:0},0).wait(5).to({startPosition:0},0).wait(5).to({startPosition:0},0).wait(5).to({startPosition:0},0).to({_off:true},6).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-251.3,-124.5,502.7,249.2);


// stage content:
(lib.ke1_07 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{psst:0});

	// timeline functions:
	this.frame_0 = function() {
        audio[7].play();
	}
	this.frame_168 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(168).call(this.frame_168).wait(1));

	// mayu
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F3817").s().p("AlHBUQkKAeieA6QAZhWBohWQDQirGPAAQGNAADhCrQBwBWAhBWQojiSoUA6g");
	this.shape.setTransform(279.6,272.3,1,1,5.7,0,0,75,17.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5F3817").s().p("Ak9BSQiiARiMAkIhtAfIAZg0QAkg+BAg0QDKilGCAAQGCAADZClQBtBTAgBTQoSiNoEA5g");
	this.shape_1.setTransform(485.7,271.8,1,1,-7.7,0,0,-72.5,17);

	this.instance = new lib.mayuge("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(380.3,242.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.instance}]},8).wait(161));

	// me
	this.instance_1 = new lib.pachipachi();
	this.instance_1.parent = this;
	this.instance_1.setTransform(378.2,407.2);

	this.instance_2 = new lib.me_2("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(374,433);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1,p:{x:378.2,y:407.2}}]}).to({state:[{t:this.instance_2}]},8).to({state:[{t:this.instance_1,p:{x:378.8,y:408.9}}]},10).wait(151));

	// hana
	this.instance_3 = new lib.hana("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(375.4,601.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(169));

	// kuchi
	this.instance_4 = new lib.pakupaku_2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(375.3,775);

	this.instance_5 = new lib.kuchi2_3("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(375,775.4);

	this.instance_6 = new lib.kuchi1_3("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(377,764);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4,p:{x:375.3,y:775}}]}).to({state:[{t:this.instance_5,p:{x:375,y:775.4}}]},8).to({state:[{t:this.instance_4,p:{x:374.5,y:780.5}}]},18).to({state:[{t:this.instance_5,p:{x:374.5,y:777}}]},13).to({state:[{t:this.instance_4,p:{x:373.5,y:781.5}}]},22).to({state:[{t:this.instance_5,p:{x:373.5,y:778}}]},14).to({state:[{t:this.instance_6}]},23).wait(71));

	// BG
	this.instance_7 = new lib.基本画面("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(375,664);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(169));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(369.9,661,760.2,1344);
// library properties:
lib.properties = {
	width: 750,
	height: 1334,
	fps: 24,
	color: "#FFCE00",
	opacity: 1.00,
	manifest: [
		{src:"sounds/_07.mp3?1489004110584", id:"_07"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;