var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // 画面を横にロック
        screen.lockOrientation('landscape');

        /*
         * sound set
         */
        var path;
        if(device.platform == 'iOS'){
            path = "/ke1/learn/sounds/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke1/learn/sounds/";
        }

        var mediaSuccess = function(){
            console.log("success");
        };
        var mediaError = function(err){
            console.log("error :");
            console.log(err);
        };


        var mediaStatus = function(status){
            console.log("status :");
            console.log(status);
        };

        var mediaStatus_start = function(status){
            if (status == Media.MEDIA_STOPPED){
                sound_bgm.play();
            }
        }

        var sound_orange = new Media(path + "oneorange.mp3", mediaSuccess, mediaError, mediaStatus);
        var sound_pudding = new Media(path + "onepudding.mp3", mediaSuccess, mediaError, mediaStatus);
        var sound_bgm = new Media(path + "kokekko.mp3", mediaSuccess, mediaError, mediaStatus);

        var sound_start_orange = new Media(path + "letsfindeoneorange.mp3", mediaSuccess, mediaError, mediaStatus_start);
        var sound_start_pudding = new Media(path + "letsfindeonepudding.mp3", mediaSuccess, mediaError, mediaStatus_start);

        (function($){
            $(function(){

                if($("body").hasClass("orange")){
                    sound_start_orange.play();
                }
                if($("body").hasClass("pudding")){
                    sound_start_pudding.play();
                }


                $(".chara_list li").each(function(i, elem){
                    var i = i;
                    var elem = elem;
                    setTimeout(function(){
                        $(elem).addClass("on");
                    }, Math.floor(Math.random() * 300));
                });


                var move = function(){
                    $(".chara_list").animate({'left':'-25%'},1500,'linear',function(){
                        $(this).css({'left':'0'});
                        $('li',this).eq(0).appendTo(this);
                        move();
                    });
                }
                move();

                //click orange
                $(".chara_list .target").on('click', function(){
                    sound_bgm.setVolume(0.3);

                    $(this).animate({'top': '-500%', 'right': '-10%'}, 500, 'linear', function(){
                        $(".target_frame .orange").animate({'top': '2px'}, 300, 'linear', function(){
                            $(".chara_list_wrapper").remove();
                            $(".title").remove();
                            $(".target_frame").addClass("result");
                            var target = this;
                            setTimeout(function(){
                                $(".shelf").addClass("on");
                                $('.one, .txt_orange, .btn_repeat').addClass("active");
                                if($(target).parents(".orange").length){
                                    console.log(sound_orange);
                                    sound_orange.play();
                                }else{
                                    console.log(sound_pudding);
                                    sound_pudding.play();
                                }
                            },500);
                        });
                    });
                });

                //button action
                $(".btn_repeat").on("touchstart",function(){
                    $('img', this).eq(0).hide();
                });
                $(".btn_repeat").on("touchend",function(){
                    $('img', this).eq(0).show();
                });

            });
        })(jQuery);


    }
};

app.initialize();