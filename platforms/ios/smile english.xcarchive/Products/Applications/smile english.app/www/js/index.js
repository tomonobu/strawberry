var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        // 画面を横にロック
        screen.lockOrientation('portrait');
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {

        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);


        (function ($) {
            $(function () {
                $('.btn_a').on('touchstart', function () {
                    $('img', this).addClass('on');
                });
                $('.btn_a').on('touchend', function () {
                    $('img', this).removeClass('on');
                });
            });
            $(document).bind('mobileinit', function () {
                $.mobile.page.prototype.options.keepNative
                    = ".data-role-none, .data-role-none *";
            });
            $(document).bind("mobileinit", function () {
                $.mobile.pushStateEnabled = false;
            });

        })(jQuery);

        // strage
        register.init();



    }
};

app.initialize();

