/**
 * Created by mizo on 2017/03/01.
 */
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
var animeArray = [
    "ke1_00", // 0 : 静止用
    "ke1_01", // 1 : イントロ
    "ke1_02", // 2 : 本編開始
    "ke1_03", // 3 :
    "ke1_04", // 4 :
    "ke1_05", // 5 :
    "ke1_06", // 6 :
    "ke1_07", // 7 :
    "ke1_08"]; // 8 :
var count_anime;
var current_frame;
var pause = false;
var play_first = true;
function init() {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    handleComplete();
}
function handleComplete() {
    count_anime = 0;
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    exportRoot = new lib[animeArray[count_anime]]();
    stage = new createjs.Stage(canvas);
    stage.addChild(exportRoot);

    //Registers the "tick" event listener.
    fnStartAnimation = function () {
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
        createjs.Ticker.addEventListener("tick", onCheckAnimationEnd); //アニメーションが最後のフレームに到達したか確認
    }

    //first =========================================================================
    $(".btn_wrapper .first").on("click", function(){
        if(!$(this).hasClass('disable')){
            console.log('==============================================');
            console.log('[[[first tapped]]]');

            /* 現在の音声を停止 */
            audio[count_anime].stop();
            audio[count_anime].release();
            current_frame = 0;

            console.log('current_num : ' + count_anime);

            stage.removeChild(exportRoot);
            stage.update();
            count_anime = 2;
            exportRoot = new lib[animeArray[count_anime]]();
            stage.addChild(exportRoot);
            if(pause){
                createjs.Ticker.addEventListener("tick", stage);
                $(".btn_wrapper .play").addClass("off");
                $(".btn_wrapper .stop").removeClass("off");
                pause = false;
            }
            pager('first');
        }
    });

    //prev =========================================================================
    $(".btn_wrapper .prev").on("click", function(){
        if(!$(this).hasClass('disable')){
            console.log('==============================================');
            console.log('[[[prev tapped]]]');
            console.log('current_num : ' + count_anime);

            if(count_anime > 2){

                /* 現在の音声を停止 */
                audio[count_anime].stop();
                audio[count_anime].release();
                current_frame = 0;

                stage.removeChild(exportRoot);
                stage.update();
                count_anime -= 1;
                exportRoot = new lib[animeArray[count_anime]]();
                stage.addChild(exportRoot);

                if(pause){
                    createjs.Ticker.addEventListener("tick", stage);
                    $(".btn_wrapper .play").addClass("off");
                    $(".btn_wrapper .stop").removeClass("off");
                    pause = false;
                }

                pager('prev');
            }
        }
    });

    // play =========================================================================
    $(".btn_wrapper .play").on("click", function(){
        if(!$(this).hasClass('disable')){
            console.log('==============================================');
            console.log('[[[play tapped]]]');
            console.log('current_num : ' + count_anime);

            $(".balloon_play").removeClass("on");

            /*-- 静止画像の切り替え start --*/
            stage.removeChild(exportRoot);
            exportRoot = new lib[animeArray[count_anime]]();
            exportRoot.gotoAndPlay(current_frame);
            stage.addChild(exportRoot);
            /*-- 静止画像の切り替え end --*/

            createjs.Ticker.addEventListener("tick", stage);
            /* 音声を再開 */
            audio[count_anime].play();
            stage.update();

            $(this).addClass("off");
            $(".btn_wrapper .stop").removeClass("off");

            pager('play');
        }
    });

    // stop =========================================================================
    $(".btn_wrapper .stop").on("click", function(){
        if(!$(this).hasClass('disable')){
            console.log('==============================================');
            console.log('[[[stop tapped]]]');
            console.log('current_num : ' + count_anime);
            //console.log(exportRoot.timeline.position);

            current_frame = exportRoot.timeline.position; //現在のフレームを保存
            createjs.Ticker.removeEventListener("tick", stage); //再生停止

            /* 音声を停止 */
            audio[count_anime].pause();
            pause = true;

            /*-- 静止画像の差し込み start --*/
            stage.removeChild(exportRoot);
            exportRoot = new lib[animeArray[0]]();
            stage.addChild(exportRoot);
            /*-- 静止画像の差し込み end --*/

            stage.update();
            $(this).addClass("off");
            $(".btn_wrapper .play").removeClass("off");
            pager('stop');
        }
    });

    //next =========================================================================
    $(".btn_wrapper .next").on("click", function(){
        if(!$(this).hasClass('disable')){
            console.log('==============================================');
            console.log('[[[next tapped]]]');
            console.log('current_num : ' + count_anime);

            if(count_anime < animeArray.length - 1){

                /* 現在の音声を停止 */
                audio[count_anime].stop();
                audio[count_anime].release();
                current_frame = 0;

                stage.removeChild(exportRoot);
                stage.update();
                count_anime++;
                exportRoot = new lib[animeArray[count_anime]]();
                stage.addChild(exportRoot);
                if(pause){
                    createjs.Ticker.addEventListener("tick", stage);
                    $(".btn_wrapper .play").addClass("off");
                    $(".btn_wrapper .stop").removeClass("off");
                    pause = false;
                }
                pager('next');
            }
        }
    });

    //last =========================================================================
    $(".btn_wrapper .last").on("click", function(){
        if(!$(this).hasClass('disable')){
            console.log('==============================================');
            console.log('[[[last tapped]]]');
            console.log('current_num : ' + count_anime);

            /* 現在の音声を停止 */
            audio[count_anime].stop();
            audio[count_anime].release();
            current_frame = 0;

            count_anime = animeArray.length - 1;
            stage.removeChild(exportRoot);
            exportRoot = new lib[animeArray[count_anime]]();
            stage.addChild(exportRoot);
            if(pause){
                createjs.Ticker.addEventListener("tick", stage);
                $(".btn_wrapper .play").addClass("off");
                $(".btn_wrapper .stop").removeClass("off");
                pause = false;
            }
            stage.update();
            pager('last');
        }
    });


    //再生後にアニメーションを入れ替える =========================================================================
    function onCheckAnimationEnd(event) {
        if (exportRoot.timeline.position >= (exportRoot.timeline.duration - 1)) {

            /**
             * 音声をリリースする
             * 音声データは1から始まっているので２以上の場合のみ
             */
            if(count_anime > 1){
                audio[count_anime].release();
            }
            count_anime++;

            if (count_anime < (animeArray.length)) {

                if(count_anime == 2 && play_first){ // 再生しようとしているムービーがチャプター1でかつムービーが停止状態
                    $(".disable").removeClass("disable");
                    $(".btn_wrapper .stop").addClass("off");
                    $(".btn_wrapper .play").removeClass("off");
                    createjs.Ticker.removeEventListener("tick", stage); //再生停止
                    play_first = false;
                    pause = true;
                    count_anime--;
                }else{// 自動再生

                    if(count_anime == 1){
                        setTimeout(function(){
                            $(".balloon_play").addClass("on");
                        }, 13000);
                    }

                    stage.removeChild(exportRoot);
                    exportRoot = new lib[animeArray[count_anime]]();
                    stage.addChild(exportRoot);
                    pager('自動再生');
                }

            }else {// 最後のムービーを再生し終わってたら

                createjs.Ticker.removeEventListener("tick", stage); // 再生停止
                createjs.Ticker.removeEventListener("tick"); // リスナー
                pause = true;
                count_anime = 1; // 最初のアニメの番号
                $(".btn_wrapper .stop").addClass("off");
                $(".btn_wrapper .play").removeClass("off");
                pager('end');
            }
        }
    }

    //ページャー設定 =========================================================================
    function pager(action){
        console.log(action + ' : ' + count_anime);
        $(".chap li").removeClass("active");
        $(".chap li").each(function(i, elem){
            if(i < count_anime - 1){
                $(this).addClass("active");
            }
        });
    }

    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS=1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();
        function resizeCanvas() {
            var w = lib.properties.width, h = lib.properties.height;
            var iw = window.innerWidth, ih=window.innerHeight;
            var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
            if(isResp) {
                if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                    sRatio = lastS;
                }
                else if(!isScale) {
                    if(iw<w || ih<h)
                        sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==1) {
                    sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==2) {
                    sRatio = Math.max(xRatio, yRatio);
                }
            }
            canvas.width = w*pRatio*sRatio;
            canvas.height = h*pRatio*sRatio;
            canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
            canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
            stage.scaleX = pRatio*sRatio;
            stage.scaleY = pRatio*sRatio;
            lastW = iw; lastH = ih; lastS = sRatio;
        }
    }
    makeResponsive(true,'width',true,2);
    fnStartAnimation();
}