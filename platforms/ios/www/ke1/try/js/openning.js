var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        // 画面を横にロック
        screen.lockOrientation('landscape');
        progress.use('try');
        $("#openning").fadeIn();
    }
};

app.initialize();

(function($){
    $(function(){
        $('a.btn').on('touchstart', function(){
            $(this).addClass('on');
        });
        $('a.btn').on('touchend', function(){
            $(this).removeClass('on');
        });
    });
})(jQuery);