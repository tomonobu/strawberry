/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var sound= [];
var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        // 画面を横にロック
        screen.lockOrientation('portrait');
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {
        progress.use('talk');
        /*
         * sound set
         */
        var path;
        if(device.platform == 'iOS'){
            path = "ke1/talk/sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke1/talk/sound/";
        }

        var mediaSuccess = function(){
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if(status != Media.MEDIA_NONE){
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if(status == Media.MEDIA_STARTING){
                // 再生が開始されたタイミング
                console.log("media: starting");
            }
            if(status == Media.MEDIA_RUNNING){
                // 再生が開始されたタイミング
                console.log("media: running");
            }
            if(status == Media.MEDIA_PAUSED){
                // 再生が開始されたタイミング
                console.log("media: paused");
            }
            else if(status == Media.MEDIA_STOPPED){
                // 停止された時の処理
                console.log("media: stopped");
            }
            console.log('==============================================');
        }

        sound[1] = new Media(path + "hi_i_am_motti.mp4", mediaSuccess, mediaError, mediaStatus);
        sound[2] = new Media(path + "hi.mp4", mediaSuccess, mediaError, mediaStatus);
        sound[3] = new Media(path + "friends_hiimmotty.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[4] = new Media(path + "seeyounext_effect.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[5] = new Media(path + "say_hi.mp3", mediaSuccess, mediaError, mediaStatus);
    }
};

app.initialize();

(function($){

    //遷移後のアクション設定
    $(document).on('pagecontainerchange', function (e, d) {
        console.log(d.toPage[0].id);
        if (d.toPage[0].id == 'scene1') {
            $('body').pagecontainer('load', 'scene2.html');

            setTimeout(function () {
                $('body').pagecontainer('change', 'scene2.html', {transition: 'fade'});
            }, 3000);

        }
        if(d.toPage[0].id == 'scene2'){
            sound[1].play();
            setTimeout(function(){
                $(".btn_next.scene2.btn").addClass("active");
            }, 5000);

            $("#scene2").on('click', ".btn_next.scene2.btn", function(){
                $(".btn_next.scene2.btn").removeClass("active");

                if($('.motti1').hasClass('active')){
                    $('.motti1').removeClass('active');
                    $('.motti2').addClass('active');
                }


                if($('.message1').hasClass('active')){
                    $('.message1').removeClass('active');
                    $('.message2').addClass('active');

                    setTimeout(function(){
                        $(".btn_next.scene2.btn").addClass("active");
                        $('.motti2').removeClass('active');
                        $('.motti3').addClass('active');
                    }, 1000);
                }

                if($('.motti3').hasClass('active')){
                    sound[5].play();
                    $('.motti3').removeClass('active');
                    $('.motti4').addClass('active').on('animationend, webkitAnimationEnd', function(){
                        $('.motti4').removeClass('active');
                        $('.motti5').addClass('active');
                        $('.tina').addClass('active');
                        $('.message2').removeClass('active');
                        $('.message3').addClass('active');
                        $(".btn_answer.scene2.btn").addClass("active").on('click', function(){
                            $('body').pagecontainer('change', 'scene3.html', {transition: 'fade'});
                        });
                    });
                }

            });
        }

        if(d.toPage[0].id == 'scene3'){
            $('body').pagecontainer('load', 'scene4.html');
            setTimeout(function () {
                $('body').pagecontainer('change', 'scene4.html', {transition: 'fade'});
            }, 3000);
        }

        if(d.toPage[0].id == 'scene4'){
            sound[3].play();
            $('body').pagecontainer('load', 'scene5.html');
            setTimeout(function () {
                $('body').pagecontainer('change', 'scene5.html', {transition: 'slidedown'});
            }, 8000);
        }

        if(d.toPage[0].id == 'scene5') {
            sound[4].play();
            $('body').pagecontainer('load', 'index.html');
            $('body').pagecontainer('load', 'scene6.html');
            $('.btn_more').on('click', function(){
                $(".btn_more.scene5.btn").removeClass("active");
                $('body').pagecontainer('change', 'index.html', {transition: 'slide'});
            });
            $('.btn_go').on('click', function(){
                $(".btn_next.scene5.btn").removeClass("active");
                $('body').pagecontainer('change', 'scene6.html', {transition: 'fade'});
            });

        }

        if(d.toPage[0].id == 'scene6') {
            $('body').pagecontainer('load', 'index.html');
            $('.btn_more').on('click', function(){
                $(".btn_next.scene9.btn").removeClass("active");
                $('body').pagecontainer('change', 'index.html', {transition: 'slide'});
            });
            $(".word-list a").on("touchstart", function(){
                $(this).addClass("on");
            });
            $(".word-list a").on("touchend", function(){
                $(this).removeClass("on");
                sound[2].play();
            });

        }

        //button
        $(".page").on("touchstart",".btn", function(){
            $(this).addClass("on");
        });
        $(".page").on("touchend",".btn", function(){
            $(this).removeClass("on");
        });
    });
})(jQuery);
