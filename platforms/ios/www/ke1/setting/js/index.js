var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        // 画面を横にロック
        screen.lockOrientation('portrait');
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {

        console.log('Received Event: ' + id);

    }
};

app.initialize();

(function($){
    $(function(){
        $('form').on('submit', function(){
            console.log($('[name="name"]').val());

            var form = $('form');
            var data = form.serialize(); //フォームの内容を全て取得する
            $.ajax({
                type: "POST",
                url: "https://smile-en.jp/contact/index.php",
                data: data,
                dataType: 'json',
                crossDomain: true,
            }).done(function(msg){
                if(msg.status == 'success'){
                    form.html('<div class="text-success">送信しました。</div>');
                }else{
                    form.html('<div class="text-success">送信に失敗しました。</div>');
                }
                console.log(msg);
            }).fail(function(){
                form.html('<div class="text-danger">エラーが発生しました</div>');
            });

            return false;
        });
    });
})(jQuery);