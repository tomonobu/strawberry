var register = {
    db : null,
    open : function(){
        var _self = this;
        console.log("成功");
        _self.db = sqlitePlugin.openDatabase({name: 'kse.db', location: 'default'});
        console.log(this.db);

        _self.db.executeSql("CREATE TABLE IF NOT EXISTS options (id integer primary key autoincrement, okey text, ovalue text)", [], function(res) {
            // テーブル作成成功
            // データ取得のSQL実行
            _self.db.executeSql("SELECT * FROM options WHERE okey = 'smilecode' ORDER BY id desc", [], function(rs) {
                // 取得したデータを表示
                var registered = false;
                for (var i = 0; i < rs.rows.length; i++) {
                    row = rs.rows.item(i);
                    console.log(row);
                    registered = true;
                }
                console.log("registered : " + registered);
                if(registered){
                    $(".event.received .after_register").addClass('active');
                }else{
                    $(".event.received .register").addClass('active');
                    _self.register();
                }

            }, function(error) {
                // データ取得失敗
                alert(error.message);
            })
        }, function(error) {
            // テーブル作成失敗
            alert(error.message);
        });
    },
    init: function(){
        var _self = this;
        sqlitePlugin.selfTest(function () {
                _self.open();
            }, function (error) {
                // プラグインテスト失敗
                console.log("失敗");
            });
    },
    register: function(){
        //スマイルコード登録
        var _self = this;
        var name_check = false;
        var code_check = false;
        $('.register input#submit').on("click", function(){
            var nickname = $('[name="nickname"]').val();
            var smilecode = $('[name="smilecode"]').val();

            //ニックネームチェック
            if(nickname.match(/[A-Za-z]+/)){
                $(".register .nickname .err").removeClass("active");
                name_check = true;
            }else{
                $(".register .nickname .err").addClass("active");
                console.log('nickname fail');
            }

            //スマイルコードチェック
            if(smilecode == 'KSE72930' || smilecode == 'KSE84235' ){
                code_check = true;
                $(".register .smilecode .err").removeClass("active");
            }else{
                $(".register .smilecode .err").addClass("active");
                console.log('smilecode fail');
            }

            if(code_check && name_check){
                // データ追加のSQL実行
                _self.db.executeSql("INSERT INTO options (okey, ovalue) values (?, ?)", ['nickname', nickname], function(res) {
                    // SQL成功。表示処理に
                    console.log('nickname : success');

                    _self.db.executeSql("INSERT INTO options (okey, ovalue) values (?, ?)", ['smilecode', smilecode], function(res) {
                        // SQL成功。表示処理に
                        console.log('smilecode : success');
                        console.log('resistered');
                        location.reload();
                    }, function(error) {
                        // SQL失敗
                        alert(error.message);
                    });

                }, function(error) {
                    // SQL失敗
                    alert(error.message);
                });
            }

            return false;
        });
    }
}
