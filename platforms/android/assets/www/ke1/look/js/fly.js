(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.シンボル1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("AgTgkIAABJQAKgCAJgHQAUgNAAgUQAAgVgUgHQgJgDgKAAg");
	this.shape.setTransform(2,3.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgTgkQAKAAAJADQAUAHAAAVQAAAUgUANQgJAHgKACg");
	this.shape_1.setTransform(2,3.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,7,10.4);


(lib.てんとう虫本体 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#050101").s().p("AhKBLQg7gmAPg6QAQhABjgPQAqgGAtAuQAuAvgKAwQgJAyhCANQgRADgRAAQgtAAgogag");
	this.shape.setTransform(-17.5,15.2,0.5,0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#912027").ss(3).p("AAAlPIAAKf");
	this.shape_1.setTransform(-17.5,27,0.5,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#050101").s().p("AgRAzQgsggAhgvQANgSARgHQASgGAMANQAYAbgaArQgTAggQAAQgGAAgGgFg");
	this.shape_2.setTransform(-29.7,20.2,0.5,0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#050101").s().p("AABA2QgQgHgNgSQghgvAsggQAVgQAZArQAaArgXAbQgIAJgLAAQgFAAgHgCg");
	this.shape_3.setTransform(-5,21.1,0.5,0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#050101").s().p("AhCBKQglgqA6hFQAXgbAZgPQAbgOAPAJQAeATAFAVQAFAVgNArQgNAqg0ATQgVAIgQAAQgXAAgNgPg");
	this.shape_4.setTransform(-8.1,37.4,0.5,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#050101").s().p("AgGBRQg0gTgNgqQgOgrAFgVQAGgVAegTQAPgJAbAOQAZAPAXAbQA6BFglAqQgNAPgXAAQgQAAgVgIg");
	this.shape_5.setTransform(-25.9,37.4,0.5,0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#050101").s().p("AgfBbQgvgXgEhJQgDhIApgTQAkgQAmAhQAkAeANAtQANAtghAlQgVAYgcAAQgSAAgXgLg");
	this.shape_6.setTransform(-11.3,27.8,0.5,0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#050101").s().p("AgSBYQg6gPgCg0QAFgdAOgcQAdg6AsAEQAdACAXAvQAaAzgaAtQgVAlgmAAQgLAAgOgEg");
	this.shape_7.setTransform(-24.7,27,0.5,0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CF4742").s().p("Aj3EIQhmhQAAiTQAAiaBmhtQBnhtCQAAQCRAABnBtQBmBtAACaQAACThmBQQheBIiaAAQiZAAhehIg");
	this.shape_8.setTransform(-17.5,27,0.5,0.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#050101").ss(3,1).p("AA1goQgDALgPAPQgeAdg5Aa");
	this.shape_9.setTransform(-14,2.1,0.5,0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#050101").ss(3,1).p("Ag0goIASAaQAeAdA5Aa");
	this.shape_10.setTransform(-20.9,2.1,0.5,0.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#050101").s().p("AiRB8Qg5gRAAg0QAAgkAZgfQAYgeApgTIAAgEQAAgdAfgUQAggVAsAAQArAAAgAVQAfAUABAcQAuATAbAgQAcAfAAAnQAAA0g+ARQgpAMhpAAQhmAAgmgMg");
	this.shape_11.setTransform(-17.4,9.5,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-35,-1.4,35.1,46.8);


(lib.flower = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#458B3D").s().p("AAEBUQgzgGgYgdQgXgaAJgeQAJggAlAGQgJglAfgLQAdgLAcAUQAfAWALAyQAGAeAAA2QgcACgWAAQgUAAgOgCg");
	this.shape.setTransform(-90.6,135);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#458B3D").s().p("AgoBJQgegWgLgyQgHgfAAg1QA3gEAeAEQAyAGAZAcQAXAbgJAeQgJAgglgGQAJAlgfALQgLAEgJAAQgTAAgTgNg");
	this.shape_1.setTransform(-72.9,151.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#458B3D").s().p("AgHBWQgggJAGglQglAJgLgfQgLgdAUgcQAWgfAygKQAfgHA1AAQAEA2gEAeQgGAzgdAYQgTAQgVAAQgIAAgIgCg");
	this.shape_2.setTransform(-89.7,151.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#458B3D").s().p("AhTAEQAGgzAdgYQAagXAeAJQAgAJgGAlQAlgJALAfQALAdgUAcQgWAfgyALQgeAGg2AAQgEg2AEgeg");
	this.shape_3.setTransform(-73.3,134.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#458B3D").s().p("AguA9QgtgogEgrQgFgoAdgYQAegZAfAeQAPgpAlAKQAkAJAOAmQAOApgWA2QgOAjgjAyQg1gegcgYg");
	this.shape_4.setTransform(-130.3,129.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#458B3D").s().p("AgpByQgkgJgNgnQgPgpAWg2QAPgjAjgyQA1AfAbAYQAtAoAFAqQAEAogcAYQgeAZgggeQgMAhgbAAQgGAAgHgBg");
	this.shape_5.setTransform(-123.3,152.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#458B3D").s().p("AhWBJQgZgeAfggQgqgOAKgmQAJgkAmgNQApgPA2AXQAiAOAzAjQgeA0gYAcQgoAtgrAFIgKAAQghAAgVgYg");
	this.shape_6.setTransform(-137.8,144.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#458B3D").s().p("AgdBTQgjgOgygjQAeg0AZgcQAngtArgFQAogEAYAcQAZAegeAgQApAOgKAmQgJAjgmAOQgQAGgSAAQgcAAghgOg");
	this.shape_7.setTransform(-115.2,138.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#7FAE3A").s().p("AgBBtQgfAsgqgXQgogWgEgyQgFg2Atg6QAagjA6gzQA3A1AZAnQApA8gIA2QgIAxgpAUQgOAGgMAAQgbAAgSggg");
	this.shape_8.setTransform(-106.7,147.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#7FAE3A").s().p("AgOB5QgvgChIgTQAMhMARgoQAbhFAwgYQAsgXAoAXQApAYgVAyQA2AAAFAvQAFAtgnAgQgnAghBAAIgKAAg");
	this.shape_9.setTransform(-94,126.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#7FAE3A").s().p("AhfBkQgqgagBguQgBgwA2gHQgcguAmgdQAkgcAvARQAzARAkBBQAWAmAWBKQhJAcgrAIQgWADgTAAQgtAAgggUg");
	this.shape_10.setTransform(-119.4,126.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#458B3D").s().p("AhZBdQgngaAAgrQAAgsAygGQgZgrAkgbQAigZArAQQAvARAhA9QAUAkATBFQhEAZgoAHQgTADgQAAQgsAAgfgUg");
	this.shape_11.setTransform(7.1,68.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#458B3D").s().p("AgJBqQgwgRggg9QgTgkgUhFQBCgZAqgHQBEgKAqAcQAnAZAAArQAAAsgyAGQAZAsgkAaQgVAPgYAAQgQAAgQgGg");
	this.shape_12.setTransform(32.8,84.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#458B3D").s().p("AgaBPQgrAZgbgkQgZgiAQgrQARgvA9ghQAkgUBFgTQAZBDAHAqQAKBDgbAqQgaAngrAAQgsAAgGgyg");
	this.shape_13.setTransform(12.6,88.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#458B3D").s().p("AhtAVQgKhEAcgqQAZgnArAAQAsAAAGAyQAsgZAaAkQAZAigQArQgRAwg9AgQgkAUhFATQgZhCgHgqg");
	this.shape_14.setTransform(27.9,63.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#7FAE3A").s().p("Ag5BvQgvgFgTgmQgVgpArgbQgrgbAVgoQATgmAvgFQAygGA3AoQAjAYAwA0QgwA0gjAZQgwAjgtAAIgMgBg");
	this.shape_15.setTransform(117.9,139.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#7FAE3A").s().p("AhPBeQgigmABhDQAAgrAPhFQBHAJAnAOQBBAXAYAsQAWAogUAmQgVAogvgTQACAzgsAGIgLABQgkAAgageg");
	this.shape_16.setTransform(138,151);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#7FAE3A").s().p("AhqAXQgOhCAZgrQAYgpArgCQAsgDAIAyQAqgbAdAjQAbAhgOArQgPAwg7AkQgkAWhDAWQgdhBgIgqg");
	this.shape_17.setTransform(136.5,127.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#7FAE3A").s().p("Ag5BvQgvgFgTgmQgVgpArgbQgqgbAUgoQATgmAvgFQAygGA3AoQAjAZAwAzQgwA0gjAZQgwAjgtAAIgMgBg");
	this.shape_18.setTransform(-1.7,161);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#7FAE3A").s().p("AhQBeQghglAAhEQAAgrAQhFQBHAJAnAOQBBAXAYAsQAWApgUAlQgWAogvgTQACAzgrAGIgMABQgjAAgbgeg");
	this.shape_19.setTransform(18.4,172.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#7FAE3A").s().p("AhqAXQgOhDAZgrQAYgoArgCQAsgDAIAyQArgbAcAiQAbAhgOAsQgPAwg7AjQgjAWhEAXQgdhBgIgqg");
	this.shape_20.setTransform(16.9,148.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#458B3D").s().p("AhZBcQgngZAAgrQAAgtAygFQgZgsAkgaQAjgZAqAQQAwARAgA9QAUAlATBEQhCAZgqAHQgTACgPAAQgtAAgfgUg");
	this.shape_21.setTransform(84.1,137.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#458B3D").s().p("AgJBqQgvgRghg8QgUglgThFQBDgZAqgHQBDgKAqAcQAnAaAAArQAAAsgyAFQAZAsgkAaQgVAPgYAAQgQAAgQgGg");
	this.shape_22.setTransform(109.8,152.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#458B3D").s().p("AgaBPQgsAZgagkQgZgiAQgrQARgvA9ghQAkgUBFgTQAZBDAHAqQAKBDgcAqQgZAngrAAQgsAAgGgyg");
	this.shape_23.setTransform(89.6,157.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#458B3D").s().p("AhtAUQgKhDAbgqQAagnArAAQAsAAAGAyQArgZAbAkQAZAigQArQgRAvg9AhQgkAUhFATQgZhCgHgrg");
	this.shape_24.setTransform(105,132.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#458B3D").s().p("AgKB5QhNgYgegzQgcguAWgtQAXgwA4AUQgEg7AygJQAxgJAmAoQApAqADBQQACAygPBRQhTgHgvgPg");
	this.shape_25.setTransform(32.2,132.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#458B3D").s().p("AhYBvQgpgqgDhQQgCgyAPhRQBTAHAvAPQBNAYAeAzQAcAugWAtQgXAvg4gUQAEA7gzAKQgJABgJAAQgmAAgeggg");
	this.shape_26.setTransform(56.1,160.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#458B3D").s().p("AgtB8QgvgXAUg4Qg7AEgJgzQgJgxAnglQArgpBPgDQAygCBRAPQgHBUgPAuQgYBNgyAeQgbAPgZAAQgUAAgUgJg");
	this.shape_27.setTransform(30.5,158.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#458B3D").s().p("AiOB4QAHhTAPgvQAYhNAzgeQAugcAtAWQAvAXgUA4QA7gEAKAzQAJAxgoAlQgqAphQADIgJAAQgwAAhKgNg");
	this.shape_28.setTransform(58.6,134.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#F8F7E8").s().p("AASCuQgwgIgqgaQgigVgUgcIhDhMQgdgfgJgVQgGgQACgQQABgSALgLQAFgGAGgBQAEgBAJAFQAgASAeAiQASAVAgArQgShHAGhKQABgSAJgNQAKgPAOAEQAJADAIANQAiA6gNBAQAehOA2hAQAQADAKAPQAJAOgBARQAAAYgWAlIgQAdQgIAQgBAPQAlhIBGgpQASAQAEAcQAEAagJAZQgIAWgRAVIgTAXQAegeAqgMQAcgJAKAPQAEAHgCANQgGAwgjAnQghAmgwAQQgeAKggAAQgRAAgRgDg");
	this.shape_29.setTransform(63.4,4.3,0.748,0.748);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#BDCF73").s().p("AAcDjQg8gOgcgQQgtgcgHgqQhrhGhKhoQgQgVgEgTQgGgXAMgQQAJgKAOgDQANgDAOAFQASAFAbAYQAfAdAZAcQgXgbgOgjQgQgmgDgqQgCgTAJgIQAGgHAOgBQAWgBAWANQAUALAPAUQANAQAKAYQAGAOAKAeQADALABAIQAEgoANgnQALgkARgSQAMgLANgFQAQgEAOAFQAUAHAJAYQAHAUgCAaQgCATgEATQALgXAQgVQAUgZASgIQANgEANACQAOADAHAKQAKAOgGAfQgLAzgUAvQAOggAbgZQAdgaAkgMQAcgKARALQARAMgCAkQgDA7gkAxQgaAlgjAWIAQgEQAZgEAYAJQAQAFAHALQAGALgDAOQgFAcgeARQgbAPgggCQgWgCglgKIgogJIAHAFQALANgDAPQgDAOgMAKQgMAKgPADIgPACQgSAAgagHg");
	this.shape_30.setTransform(62.9,-4,0.748,0.748);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#F8F7E8").s().p("ABJFGQgQATgbgCQgagCgMgUQhSAEhQgWQhEgSg7gjIANAIQhHglg8g1QgjgfABgaQAAgRARgKQAJgGAIABQgMgIgJgLQgNgTABgUQABgVAUgOQATgOATAIQgUgegIgTQgNgdADgZQADgdAbgSQAcgSAWAQQABgZACgMQADgVAJgOQAKgRARgIQATgIAQAHIATALQAMAGAHgEQAJgDADgOIAEgYQAHgYAggHQAZgGAiAGQAMADAIAJIAEAFQAFgMALgIQAPgNATABQASAAAPANQAOAOACASQAZgKAOgDQAXgGASAEQAVAEAOARQAOASgGATQAFgPAUgDQAPgCAVAGQAnAKAQANQAMALAFAPQAEAQgHANQAPgIARACQASACANALQAMALAEARQAFARgGAQQAlAEATAoQASAngWAgQASgNAWAPQAWANAAAXQAAAWgPATQgOARgWALIAZAPQgIAYgRAUQgSATgYAKQAQAHACAUQACAUgNAMQgMAMgTACQgRADgSgHQAIAMgEAPQgDAOgMAHQgLAHgPgDQgOgEgHgMQgJAWgZAJQgaAJgUgMQgLAYgfACIgHAAQgZAAgNgTg");
	this.shape_31.setTransform(66.5,-10,0.748,0.748);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#F8F7E8").s().p("AgUDOQgvgMgggWIhvg9IgigVQgTgMgLgOQgOgQgFgTQgFgVAIgRQADgIAGgEQAHgDALACQArAJAxAaQAdAQA3AmQgxhLgYhYQgFgVAFgTQAFgWASAAQAMAAAOAMQAeAZATAjQATAjAGAmQADhnAlhgQATgDASAOQAQAMAGAUQAGARgCAXQgBANgFAaQgFAdgBALQgDAWAFARQANhiBBhLQAcALAQAfQAPAcgBAhQAAAcgLAiIgNAfQAYgtAqgfQAMgJAMgDQAOgEAJAIQAHAGAEARQAMA5gYA7QgYA5gxAlQguAkg+ALQgbAFgbAAQghAAgggHg");
	this.shape_32.setTransform(-10.7,-118.8,0.777,0.777);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#BDCF73").s().p("AALEMQgdgGgXgQQgYgPgMgWQiZgnh/hbQgagSgNgUQgRgZAIgVQAGgPAPgKQAOgIASgBQAYgBAoARQAtATAuAYQgngWgggiQgigmgUgvQgKgWAHgNQAFgKAPgGQAZgLAfAGQAbAFAaARQAVAOAWAXQAMAOAYAfQAIAKAEAIQgLgwgCgxQgCgtAOgcQAIgRAPgMQARgMASABQAZAAAVAYQAQAUAIAfQAGAWABAWQAFgeAJgdQAMglAUgQQAMgLAQgCQARgCAMAJQASANAFAlQAIBCgEA7QAFgrAUgnQAXgqAlgdQAdgWAWAGQAaAHAMAqQATBGgVBIQgPA1gfAnQAIgGAJgFQAbgOAfAAQAXAAAKAJQALAJADATQAFAjgbAeQgZAcgnAMQgaAHgvADIgyAFIAKAEQASAKADASQADASgKARQgKAQgQAJQgOAJgUAEQgMADgZADQgkAEgdAAQgdAAgUgEg");
	this.shape_33.setTransform(-14.3,-128.8,0.777,0.777);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#F8F7E8").s().p("AjuGQIASADQhggPhcglQg0gVgKgeQgHgUAQgTQAJgKAJgCQgRgEgPgKQgXgQgHgYQgHgZARgYQAQgZAaACQgkgcgRgSQgagdgHgeQgIgiAYgfQAYggAhAJQgJgbgDgRQgEgZAEgUQAEgXASgQQASgSAWACIAaAFQAPACAIgHQAJgHgCgRIgFgeQgCgfAigVQAbgQAqgHQAPgCANAHIAGAFQABgPAJgPQANgVAVgHQAWgGAWAJQAXAJAKAUQAagWAOgJQAYgPAWgEQAZgEAWAPQAYAPABAYQAAgTAWgLQARgIAagDQAZgCANABQAUAAAQAGQASAHALAQQAMARgDASQANgQAVgEQAWgFATAIQATAHAMASQAMATgBAUQAtgLAlAnQAmAngNAtQAQgXAfAJQAfAIAKAbQAJAYgLAdQgJAagVAVIAjAHQABAfgNAdQgMAdgYAVQAVACAKAWQAKAWgJAUQgJATgVAKQgTAKgXAAQANAKACASQACASgKANQgLANgSACQgTABgMgLQgBAegaAUQgaAUgdgFQgDAggiAPQgiAQgagTQgLAbgfAJQggAIgXgRQhdAlhlAHQgYACgYAAQg9AAg7gLg");
	this.shape_34.setTransform(-12.8,-135.4,0.777,0.777);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#7FAE3A").s().p("AgTBfQgqAhgigiQggghALgxQALg0A8grQAlgbBEgfQAlBEANArQAVBGgXAxQgVAtguAHIgKABQgmAAgMgvg");
	this.shape_35.setTransform(-59,164.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#7FAE3A").s().p("AghBrQgqgOhCgoQAihDAcgkQAug6A0gJQAxgJAgAiQAhAjgkApQA0AQgJAuQgIAtguATQgXAKgcAAQgfAAglgNg");
	this.shape_36.setTransform(-40.9,147.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#7FAE3A").s().p("AAGB5QhJgJgjgpQghgmANgrQANgvA1AJQgNg0AtgRQAqgQAoAeQAsAfAPBIQAKAtAABLQgqAEggAAQgbAAgUgDg");
	this.shape_37.setTransform(-65.6,139.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#7FAE3A").s().p("AiEB1QgzgrAIg8QAHg/BIABQgchCA4gfQA1geA6AfQA/AhAjBbQAVA3APBkQhjAYg7ACIgKAAQhaAAgzgsg");
	this.shape_38.setTransform(47.1,108.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#7FAE3A").s().p("AgpCRQhAghgihbQgWg4gPhjQBjgYA7gCQBhgCA3AuQAyArgHA8QgIA/hIgBQAcBCg3AfQgaAPgcAAQgbAAgegQg");
	this.shape_39.setTransform(82.4,135.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#7FAE3A").s().p("AAOCxQg/gIABhIQhCAcgfg3Qgdg1Aeg6QAhhABbgjQA3gVBkgPQAYBjACA7QACBhguA3QgmArgzAAIgOAAg");
	this.shape_40.setTransform(51.9,139.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#7FAE3A").s().p("AigATQgChhAug3QArgyA9AHQA+AIgBBIQBDgcAeA3QAeA1gfA6QggBAhcAjQg3AVhkAPQgYhigCg8g");
	this.shape_41.setTransform(78.5,104.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#458B3D").s().p("Ah2B7Qg0gigBg5QAAg7BDgIQgig6AwgjQAugiA6AWQA+AXArBQQAbAxAaBbQhZAig4AJQgZAEgVAAQg7AAgogbg");
	this.shape_42.setTransform(-26.7,122.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#458B3D").s().p("AgMCNQg/gXgrhQQgbgxgahcQBagiA3gIQBagOA3AlQA0AiABA5QAAA7hDAIQAiA6gwAjQgcAVghAAQgUAAgWgJg");
	this.shape_43.setTransform(7.5,143);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#458B3D").s().p("AgiBpQg7AigjgwQgiguAWg5QAXg/BRgrQAwgbBcgaQAhBZAJA4QAOBaglA3QgiA0g5ABQg7AAgHhDg");
	this.shape_44.setTransform(-19.4,149.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#458B3D").s().p("AiRAbQgOhaAlg3QAig0A5gBQA7AAAHBDQA7giAjAwQAhAugVA6QgXA+hRArQgwAbhcAaQghhZgJg4g");
	this.shape_45.setTransform(1,115.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#7FAE3A").s().p("ABsUMQgHgFgDgIQhGjLhbyGQgvpJgkpaQgBgKAHgIQAHgHAKgBQALgBAHAHQAIAHABAKQAiI2AvJPQBcSbBEDHQAEAKgEAJQgFAJgKAEIgIABQgIAAgGgEg");
	this.shape_46.setTransform(2.7,19.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#458B3D").s().p("AkPK+QERlBCJpJQAri4AYi9IAQidIAyADIgQCfQgZDBgsC8QiMJUkYFKg");
	this.shape_47.setTransform(42.4,83.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#458B3D").s().p("AhZBcQgngZAAgrQAAgsAzgGQgagsAkgaQAjgZAqAQQAwARAgA9QAUAlATBEQhCAZgqAHQgTACgPAAQgtAAgfgUg");
	this.shape_48.setTransform(-65.7,116.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#458B3D").s().p("AgJBqQgvgRghg8QgUglgThEQBCgaArgGQBDgLAqAcQAnAaAAArQAAAsgyAFQAZAsgkAaQgVAPgYAAQgQAAgQgGg");
	this.shape_49.setTransform(-40,132.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#458B3D").s().p("AgaBOQgsAagagkQgZgjAQgrQARguA9ghQAlgVBEgSQAZBCAHArQAKBCgcAqQgZAngrABQgsAAgGgzg");
	this.shape_50.setTransform(-60.2,136.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#458B3D").s().p("AhtAUQgKhDAbgqQAagnArAAQAsAAAGAyQArgZAbAkQAZAigQArQgRAvg9AhQgkAUhFATQgZhCgHgrg");
	this.shape_51.setTransform(-44.9,111.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-149.3,-167.3,298.7,352.1);


(lib.ClipGroup_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjMF8IAAr3IGZAAIAAL3g");
	mask.setTransform(20.5,38);

	// レイヤー 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E30B21").s().p("AhvlQQA1gFCoAFQABAAAAAAQABAAAAABQABAAAAABQAAAAAAABQACAEAAAEIAABUIgCADIhxAFIgFAAQgFAAAAADIgDAVIAAClQAAAJADAIQAAAAAAAAQAAABABAAQAAABAAAAQABABAAABIADACIBsAAQAGAAABAGIAAAKIgBAuIgDATIgGAEQgTAChlAEIgEAEIAADKQACAXAAAZIAAAIIgFAGIgkgBIgjABIgPACIAAAAQgEAAAGqkg");
	this.shape.setTransform(20.5,41);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_4, new cjs.Rectangle(9,7,23.1,68), null);


(lib.ClipGroup_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AklEtIAApZIJLAAIAAJZg");
	mask.setTransform(29.4,30.1);

	// レイヤー 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E30B21").s().p("AikEAQgFgDgBgKIAAgOIAAgGQAAgcADgMQAFgZASAAIA8ACIBcABQAfAAATgTQASgRAEgeQAKhHgFgLQgVAYgsAMQgjAKgpAAQgzAAgqgTQg2gYAAgrIAAgGQAAgtAJg2QAFgkAOg8IAEgQIAEgKIAGgGIAZAAQArAAAbAEIACAPQAAAmgFA0QgEA0gHAkIgBAJQAAAZAaAJQAXAJAggGQAigHAXgUQAagWAAgdIAAgGIAQh/QAKgHAkgBIAyAAIAHAEQACACAAAEQgFAigMC0IgCAWQgFBigQAtQgcBMhJATQhlANgsAAQgxAAghgFg");
	this.shape.setTransform(29.4,33.1);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_3, new cjs.Rectangle(9,7,40.8,52.2), null);


(lib.ClipGroup_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AikF6IAArzIFJAAIAALzg");
	mask.setTransform(16.5,37.8);

	// レイヤー 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E30B21").s().p("AhGFOQAAAAgBAAQAAAAgBAAQAAgBAAAAQAAgBAAAAQgBgHAAgiQAEhfAFhfQAMi9AJAAQAFABBPACQAJAAAAALIgBAJQgCBBgdFBQAAAIgCADQgDAEgHABIglABIgIAAQgSAAgNgEgAgYjGQgVgLgHgSIgBgYQAAgfARgXQAPgWAWgHQAYgHATALQAWANAIAhIAAANQAAAggNAXQgRAbgdAAQgUAAgTgJg");
	this.shape.setTransform(16.4,40.8);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_2, new cjs.Rectangle(9,7,14.9,67.6), null);


(lib.ClipGroup_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AllGbIAAs1ILLAAIAAM1g");
	mask.setTransform(35.8,41.1);

	// レイヤー 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E30B21").s().p("ADBFuQAAgCAFgHQAFgHAAgEQACgYAFgLIgIABQgXAAgPgMQgPgOAAgWQAAgWAPgUQAQgXAWAAQBCAAAABIQAAAcgVAiQgXAmgaAAQgFAAAAgFgAj2ElIgCgWIgGk7QgEi7gJiBIAAgKIBcAAQABAAABAAQAAAAABAAQAAABAAAAQAAABAAAAQADALAAAKIAAAVQAFBEACBMQACBhADAuQAAADAEAAIACgBQAggKA0gJQA4gKAcAAQAhAAATAUQARAUAAAgIADDuIACAYIAAAEQAAARhdAAIgJAAIgDgDIgGhjQgFhOgEggIgDgSQgDgNgGAAQgdAAgdAEQgqAGgKAMIAABvIAABvIgCADIgsAEIgSAAQgVAAgKgEg");
	this.shape.setTransform(35.8,44.1);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_1, new cjs.Rectangle(9,7,53.6,74.3), null);


(lib.wolk2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 本体
	this.instance = new lib.てんとう虫本体("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,0,0,0,-17.5,21.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.5,-23.4,35.1,46.8);


(lib.wolk = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 本体
	this.instance = new lib.てんとう虫本体("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,0,0,0,-17.5,21.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(13));

	// あし
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#050101").ss(3,1,1).p("AB7BwQgEgEiYhWIhYiF");
	this.shape.setTransform(-16.9,-4.8,0.503,0.503);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#050101").ss(3,1,1).p("ABCiFIiDB8IAACO");
	this.shape_1.setTransform(-14.8,20.3,0.503,0.503);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#050101").ss(3,1,1).p("ACbAhIirhBIiKBB");
	this.shape_2.setTransform(-18.6,4.8,0.503,0.503);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#050101").ss(3,1,1).p("Ag0CvQAEgDBdi+IAIic");
	this.shape_3.setTransform(15.3,-11,0.503,0.503);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#050101").ss(3,1,1).p("Ag9iaIB7ClIgxCQ");
	this.shape_4.setTransform(16.2,16.8,0.503,0.503);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#050101").ss(3,1,1).p("Ah2BOIB2ibIB3BD");
	this.shape_5.setTransform(20.5,0.9,0.503,0.503);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#050101").ss(3,1,1).p("ABvCKQgEgDjVhcIgEi0");
	this.shape_6.setTransform(-13.3,-8.9,0.453,0.453);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#050101").ss(3,1,1).p("ABXh/IitB8IACCD");
	this.shape_7.setTransform(-14.9,19.6,0.453,0.453);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#050101").ss(3,1,1).p("ACMgNIjng3IgwCI");
	this.shape_8.setTransform(-17,7.7,0.453,0.453);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#050101").ss(3,1,1).p("Ag3CqQACgBA3hZQAbgsAcgrIAAij");
	this.shape_9.setTransform(14.7,-7.2,0.453,0.453);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#050101").ss(3,1,1).p("AhdigIC7CSIhdCv");
	this.shape_10.setTransform(15.2,16.7,0.453,0.453);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#050101").ss(3,1,1).p("AiLBJIC+iRIBYBc");
	this.shape_11.setTransform(18.5,2.3,0.453,0.453);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#050101").ss(3,1,1).p("ABWCxIiRiFIgbjc");
	this.shape_12.setTransform(-15.1,-8.2,0.419,0.419);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#050101").ss(3,1,1).p("ABqiZIitB8IgmC3");
	this.shape_13.setTransform(-16.9,18.2,0.419,0.419);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#050101").ss(3,1,1).p("ACQAWIi7hBIhkBX");
	this.shape_14.setTransform(-19.7,7.3,0.419,0.419);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#050101").ss(3,1,1).p("AhLjTICXDVIhUDS");
	this.shape_15.setTransform(14.4,19,0.453,0.453);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#050101").ss(3,1,1).p("AhICTICRiEIAAih");
	this.shape_16.setTransform(15.5,-6.2,0.453,0.453);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#050101").ss(3,1,1).p("AiVArICkhVICHBI");
	this.shape_17.setTransform(19,3.7,0.453,0.453);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},4).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12}]},4).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.9,-23.4,55.9,51.9);


(lib.fly_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.シンボル1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-5.9,-4.5,0.719,0.823,0,61.7,-120.3,1.9,3.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#050101").s().p("AABA2QgQgHgNgSQghgvAsggQAVgQAZArQAaArgXAbQgIAJgLAAQgFAAgHgCg");
	this.shape.setTransform(-12.5,-9,0.36,0.412,0,61.7,-120.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#050101").s().p("AhCBKQglgqA6hFQAXgbAZgPQAbgOAPAJQAeATAFAVQAFAVgNArQgNAqg0ATQgVAIgQAAQgXAAgNgPg");
	this.shape_1.setTransform(-23.2,1.9,0.36,0.412,0,61.7,-120.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#050101").s().p("AgfBbQgvgXgEhJQgDhIApgTQAkgQAmAhQAkAeANAtQANAtghAlQgVAYgcAAQgSAAgXgLg");
	this.shape_2.setTransform(-14.3,-2.9,0.36,0.412,0,61.7,-120.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#CF4742").ss(3,1,1).p("ACAgiQgNgSgTgNQgIgGgJgFQg/gjhEAkQhGAlgFBFQgCAjAMAcQAAAAB6hAQAXgLASgKQBSgqAAgBg");
	this.shape_3.setTransform(-13.6,-2.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CF4742").s().p("Ah1BeQgMgcACgiQAFhGBGgkQBEglA/AkQAJAEAIAHQATANANARQAAABhSAqIgpAVIh6BAIAAAAg");
	this.shape_4.setTransform(-13.6,-2.2);

	this.instance_1 = new lib.シンボル1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(5.9,-4.2,0.705,0.81,-58.2,0,0,1.8,3.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#050101").s().p("AABA2QgQgHgNgSQghgvAsggQAVgQAZArQAaArgXAbQgIAJgLAAQgFAAgHgCg");
	this.shape_5.setTransform(12.5,-8.3,0.352,0.405,-58.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#050101").s().p("AhCBKQglgqA6hFQAXgbAZgPQAbgOAPAJQAeATAFAVQAFAVgNArQgNAqg0ATQgVAIgQAAQgXAAgNgPg");
	this.shape_6.setTransform(22.5,3.2,0.352,0.405,-58.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#050101").s().p("AgfBbQgvgXgEhJQgDhIApgTQAkgQAmAhQAkAeANAtQANAtghAlQgVAYgcAAQgSAAgXgLg");
	this.shape_7.setTransform(14,-2.1,0.352,0.405,-58.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#CF4742").ss(3,1,1).p("Ah6grQANgRATgNQAIgFAJgEQA+ggBBAoQBCAoACBFQABAjgMAbQAAABh0hHQgWgMgRgLQhOguAAgBg");
	this.shape_8.setTransform(13.3,-1.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CF4742").s().p("AgFAbIgngXIhOgvQANgRATgNIARgJQA+ggBBAoQBCAoACBFQABAjgMAbIAAAAIh0hGg");
	this.shape_9.setTransform(13.3,-1.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#999966").s().p("AD7BkQgrgFgogLIgLgEQgygQgSgSQgegfgbgvQgXgvgIgMIgBgCIAAACIgfA7QgbAvgeAfQgSASgyAQIgLAEQgoALgrAFQg0AHgcgGQgegHAOgTQAVgdBwg2IA3gbQBRglBNgfIAAABIABgBQBNAfBRAlQBBAeArAXQBAAjAPAWQAPATgeAHQgNADgTAAQgUAAgcgEg");
	this.shape_10.setTransform(0,3.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#050101").ss(3,1).p("AA1goQgDALgPAPQgeAdg5Aa");
	this.shape_11.setTransform(3.7,-14.4,0.5,0.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#050101").ss(3,1).p("Ag0goIASAaQAeAdA5Aa");
	this.shape_12.setTransform(-3.2,-14.4,0.5,0.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(3,1,1).p("AAfAAQAAAGgDAFQgCADgEADQgJAHgNAAQgMAAgJgHQgEgDgBgDQgEgFAAgGQAAgJAJgHQAJgHAMAAQANAAAJAHQAJAHAAAJg");
	this.shape_13.setTransform(0.3,-9.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhIBsQgMgQgDgIQgDgHAAgXIgBgKQAAgxAbglQAbgkAlAAIAEAAQAkACAZAiQAbAlAAAxQAAAZgEANQgFALgOATQgWAcg2AAQgpABgYghgAgWhiQgDgDgBgDQgFgFAAgGQABgLAIgGQAKgIAMAAQANAAAIAIQAKAGgBALQABAGgEAFIgGAGQgIAGgNAAQgMAAgKgGg");
	this.shape_14.setTransform(0.3,2.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.instance_1},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fly_1, new cjs.Rectangle(-35.1,-17.9,70.3,34.5), null);


(lib.Group_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.ClipGroup_1();
	this.instance.parent = this;
	this.instance.setTransform(35.8,41.1,1,1,0,0,0,35.8,41.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Group_3, new cjs.Rectangle(0,0,71.6,82.3), null);


(lib.Group_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.ClipGroup_2();
	this.instance.parent = this;
	this.instance.setTransform(16.4,37.8,1,1,0,0,0,16.4,37.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Group_2, new cjs.Rectangle(0,0,32.9,75.6), null);


(lib.Group_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.ClipGroup_3();
	this.instance.parent = this;
	this.instance.setTransform(29.4,30.1,1,1,0,0,0,29.4,30.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Group_1, new cjs.Rectangle(0,0,58.8,60.2), null);


(lib.Group = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.ClipGroup_4();
	this.instance.parent = this;
	this.instance.setTransform(20.5,38,1,1,0,0,0,20.5,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Group, new cjs.Rectangle(0,0,41.1,76), null);


(lib.ClipGroup_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgpAAVAMAAAgp/MBSBAAAMAAAAp/g");
	mask.setTransform(262.5,134.4);

	// レイヤー 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8563A").s().p("AmYGJQgFgQgDgaIgWA2QgBAFgEADQgEAIgLABQgFAAgHgKQgHgKgEgSQgEgPgIgyIgBgRQAAgHAFgEQAFgFAHAAQANAAADANIAJBBQAMggAJgdQAFgQANAAQAOAAAEARIANBDIAThKQACgKANAAQAHAAAFAEQAFAFAAAGIAAAEQgOAygMAkQgDALgJALQgGAIgJAAQgPAAgJgdgAo/GfQgWgBgQgQQgRgSABgcQAAgaARgVQAUgYAhACQAYABANAUQAMASgBAaQgCAcgQATQgQAUgZAAIgFAAgApLFHQgHALgBAPQgBAQAIAHQAFAGAJABQALABAIgHQAJgIACgOQAEgngXgBIgCAAQgNAAgJAMgAqnGWQgHgBgFgFQgEgFABgHIAEg1IACgMQgTgBgcABIgvADIgKAzQgBAGgFAEQgEAEgIgBQgIgCgDgEQgEgFABgHQABgNAFgWQAFgVACgOIAMhaQABgHAFgEQAFgDAIAAQAGABAFAFQADAFAAAHIgKBCQgCAHAAAFIAvgDQAbgBAUABIAHgrQABgJAHgMQAJgOAIABQAHAAAEAGQAFAFgBAHIgBADQgDAJgBAGIgEAVIgFAiIgGAiIgCAaIgCAbQAAAHgGADQgEAEgFAAIgDAAgAjGGHQgUgPgEgbQgEggAQgXQAPgWAegEQAJgBAHABQAHABAGADQgEgmAAgTQABgOANgCQAHgBAFAEQAFAEABAHQACASACAjQACAgACAUQAGArAEAOIABAEQABAHgEAFQgFAFgGAAQgJACgFgIQgGAFgKAEIgSAEIgJAAQgWAAgQgMgAihErQgSACgHALQgHAMACAUQABANALAIQALAIANgBQAKgCAEgCIAKgJIADgFIgGgsQgEgHgHgDQgGgCgFAAIgFABgADXF+QgGgCgDgHQgEgKAEg9QgPgOg1grIgJgHQgEgDgCgFQgDgFADgIQADgGAHgDQAIgCAFADQAWANApAlIACgmQABgQADgUQACgKAIgDQAGgDAHADQAGACADAGIABAIIgHBgQgDAjAAAWIAAAYQAAANgKADQgDACgDAAIgHgBgAgqF1QgUgOgHgbQgGgaALgXQANgdAggGQAXgFARAQQARAPAFAZQAFAcgKAWQgMAagbAFIgMABQgPAAgOgIgAgOEYQgPADgGAPQgEAMADAPQADAPAJAGQAIAEAIgBQAKgCAGgJQAHgKgCgOQgGgjgRAAIgEABgAE0EIQgXgHgOgYQgNgXADgaQAFgeAdgQQAVgLAWAKQAUALAMAWQANAagEAXQgFAcgZANQgMAGgNAAQgIAAgIgCgAE1CnQgOAIgBAPQgBANAHAPQAHAMALADQAJADAHgEQAJgFAEgKQAEgMgGgNQgNgbgOAAQgFAAgEACgAGbC+QgVgUgPgVQgIgMgKgTQgGgMAMgJQAFgDAHAAQAGABAFAFIAIAPIAIAPQAKANAJAKQAHAJAIAGIAGgCIAFgCQANgLAIgKIgYghQgQgXgHgNQgIgNANgJQAFgEAHABQAHABADAFQAaAngDgDIAnAzQAEAHgCAFQgBAHgGAEQgIAGgJgFQgKAOgNAKQgOAJgOACIgCAAQgNAAgLgLgAJsA1IgCgCQgJgLgOgMIgwglIgIAMQgEAGgHABQgHABgFgFQgIgGAEgLIAMgSIgKgKQgTgUgDgRQgEgVATgXQAOgRAMAKQAMAKgNAQQgJAMAEALQAEAKAKAKIADADIANgTQAOgRAMAJQANAKgQASIgOATIAzAoQASAOAIAKQAIALgIAKQgFAGgGAAIgDABQgEAAgFgEgAKyg9IiOhaQgGgDgBgHQgBgHAEgGQADgGAHgBQAGgCAGAEICIBWQATAMgJAOQgDAGgHACIgEAAQgEAAgEgCgAMgh2QgKgEgpguIhjAIQgFABgFgCQgGgCgDgHQgCgHADgGQADgIAHgBQAdgHAzgCIgagcQgOgRgIgMQgGgIAEgIQADgGAGgDQAHgDAGACIAGAFIA+BKQAWAbAQAQIARAQQAJAJgFAKQgDAGgGADIgGACIgGgCgAMjkhQgIgCgFgHQgHgHADgKQACgIAHgDQAIgCAFACQAHACAFAFQAHAIgDAIQgCAHgGAEQgEADgFAAIgEAAgAL0k3QgJgDgHgSIgMgiQgGgSgKgDQgLgDgNAIQgMAJgEALQgBAHADANQAFANgCAEQgCAHgGADQgHADgGgBQgLgDgEgYQgDgTADgMQAHgZAZgPQAZgPAXAHQAZAHALAdIAIAXQAEAMAFALQAFAHgCAHQgCAHgGADQgEACgFAAIgEAAg");
	this.shape.setTransform(405.3,189.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8563A").s().p("AhDEJQgQgDgIgLQgIgMADgQQAEgXANgNQAOgQAVAEQAPADAIAMQAIANgDAPQgEAUgMAOQgNANgPAAIgHAAgAgsCYQgcgFACgNQAUhyA6kMIAEgLQAEgIAeAEIAvAIIAFADIgBAJQgEAVgcBZQgnB8gqCkQgKAAgSgDg");
	this.shape_1.setTransform(430.9,167.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E8563A").s().p("AgmFSQgHgBgCgVQAAgYgBgEIgHpxIBsAAIACAoIABAnIgVJMQgBAFgUACIgsABg");
	this.shape_2.setTransform(134.6,158);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E8563A").s().p("AiPDUQgXgagCgpIAAgFQAAg+AdgsQAggvA7gVQAVgIAkgEQAcgDAXAAQAEAAgCgQQgCgRgGgSQgUgzgiAAQgbAAgdAVQgfAbgPALIg8gxQAAgFAHgJIALgNQAngoApgRQAmgQAzAAQCGAAAHCzIAAAHQAGCBgGCCQgCAJgNAHQgSAKgjAEIgEAAQgRAAAAgSQAAgVgHgDQgFADgKALQgIAIgJAFQgVAOgfAFQgXAEgkAAQgsAAgagegAhHCJQADAZBCgOQBAgNAAgNIAAgVQAAhIgPAAQh5AAADBsg");
	this.shape_3.setTransform(164.6,171.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E8563A").s().p("ABhFbQgIgCAAgSIADgXQAAgPgIgBQgxAWgVAHQggALgpAAQg3AAglglQglglAAg2IgBgQQAAhgBYg8QBUg5BfATIAElLQABgFAOgBQAUgBBGADQADAEAAAJQAABDgFCHQgFCFABBDIABCAIACCBIAAAKQgHALhDAAgAhCB2QgQAZgBA1IAAAHQAAAnBCgLQAtgHAtgWIAAgTIgChAIgDgsIgWgBQhSAAgeAsg");
	this.shape_4.setTransform(207.9,162.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E8563A").s().p("AijD/QgGgCAAgLIAAgNIAAgGQAAgeACgLQAGgYASAAIA8ACIBbABQAgAAASgTQARgRAFgeIAFgvQADgegDgFQgVAZgtAMQgiAJgpAAQgzAAgqgTQg1gYAAgrIAAgGQAAhIAbh6IAFgQIADgKIAGgGIAaAAQArAAAbAEIACAOQAAAngGA0QgEA0gHAjIgBAKQAAAYAaAKQAYAIAfgGQAjgHAWgUQAagWAAgdIAAgFIAQh/QAKgHAkgBIAygBIAGAFQACACAAAEQgDAVgGBQIgHBxIgCAVQgFBigRAsQgbBNhJATQhpAMgoAAQg0AAgdgFg");
	this.shape_5.setTransform(254.7,176.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E8563A").s().p("Ah7FUIgjgCIgGAAIgGgCIgBgOQgGhlADjVQADjNgJhsQAAgYACAAIAFgDIBfgHIAAFfQAqgyBbgGQA/AAAhAqQAeAmAABHQAABQgkA1QgnA5hJAGQgkAAgVgFQgbgGgcgRQAAAzgBALQAAABAAAAQgBABAAAAQAAAAgBABQgBAAAAAAIgFAAIgQAAIgTAAgAARBAQhQAIAAA1IAABGQAwAeAsgOQAmgMAUglQAUgkgNgeQgQgggyAAg");
	this.shape_6.setTransform(300.5,163.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E8563A").s().p("AhYC4QhDgSgVguQgRghAAhQQACghAHhOQAIhIAAgSIACgDIBjAAQABAAAAAAQABAAAAABQABAAAAABQAAAAAAABIAAAFQAAAggEA+QgFA/AAAfQAAAwASAWQAVAZAwAAIAEAAQAwAAASgbQAQgYAAg4QAAgXgDgvQgCgwAAgYQAAgLAjgKQAdgIARAAIAJAAQAIAJAGA6QADAqAAAcIAAAeQAABFgKAiQgPAvgsAmQgwAbhBAAQgtAAg3gOg");
	this.shape_7.setTransform(347.4,169.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E8563A").s().p("AiPENQgEgRAAgTQAAgNABAAIAFgDICPgOQAcgFALgWQAOgeAFhTIgvADIgwACQhNAAgmg1QgiguALhEQALhEAygvQA4g0BRAAIAFAAQAaAAAOAFQAFgBAAgGIACgMQAGgQAmAAQAUAAAUADIAEAAQAKABACASIAAAZIAAAVIgEC2IgEC0QgCBEgyAtQgwAqhFADIh/AFQgLAAgFgcgAgwiOQgdAZAAAzQAAAYAaALQASAIAaAAQARAAAegHIADAAIACgDIgCiDIgDgCIgKAAQgyAAgcAYg");
	this.shape_8.setTransform(392.8,167.3);

	this.instance = new lib.Group();
	this.instance.parent = this;
	this.instance.setTransform(120.3,81,1,1,0,0,0,20.5,38);
	this.instance.alpha = 0.801;

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F8BD06").s().p("AgmFTQgIgBgCgVQAAgYAAgEIgHpzIBsAAIACAoIABAoIgVJNQgBAFgUACIgsABg");
	this.shape_9.setTransform(148.5,80.4);

	this.instance_1 = new lib.Group_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(184.3,96,1,1,0,0,0,29.4,30.1);
	this.instance_1.alpha = 0.801;

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F8BD06").s().p("AidFKIgDgWIgFk7QgEi7gJiBIAAgKIBcAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQADALAAAKIgBAVQAFBEACBMQADBhACAuQABACADAAIADAAQAfgKA0gJQA3gKAeAAQAhAAASAUQASATAAAhIACDuIACAYIAAAEQAAARheAAIgJAAIgCgDIgGhjQgFhOgFggIgCgSQgDgNgHAAQgdAAgbAEQgqAGgKAMIAABvIAABvIgCADIgsAEIgSAAQgWAAgJgEg");
	this.shape_10.setTransform(253.9,82.7);

	this.instance_2 = new lib.Group_2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(286.7,79.7,1,1,0,0,0,16.4,37.8);
	this.instance_2.alpha = 0.801;

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F8BD06").s().p("AiPEOQgEgSAAgSQAAgNABgBIAFgCICQgPQAbgFALgWQAPgeAEhTIgvADQgfACgQAAQhOAAgmg0QgjgvALhEQALhFAzgvQA4g0BSAAIAEAAQAaAAAOAGQAFgCABgGIACgMQAFgQAnAAQAUAAAUADIADAAQAKABADASIAAAZIAAAVIgEC3QgCBygDBDQgBBEgzAsQgvArhGADIh/AFQgLAAgFgcgAgviOQgeAZAAAzQAAAYAaALQASAIAbAAQAPAAAggHIACgBIADgCIgDiDIgCgCIgLAAQgyAAgbAYg");
	this.shape_11.setTransform(318.7,97.5);

	this.instance_3 = new lib.Group_3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(375.8,83.4,1,1,0,0,0,35.8,41.1);
	this.instance_3.alpha = 0.801;

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AL2TXIgBgHIgMABIgBgGIg4gOIgIgMIgSACIgHgMIgugiIgBgMIgMABQgJgZgNgRIgTgLIgJgeIgGAAIgIgSIAHAAIgBgMIgNABQgBgMgKgXQgIgTABgTIAKgmIgNgFIgBgTIgGABIgJgLIgGgNIgTgFIABAGIglADIgFANIgSAIIgEAZIgHAAIACAZIgGAAIABAMIgMAIIACASIgHAAIgbAoIgNABIgQAaIgNABIgFAMIgMABIgFANIgZAIIgFANIgMABIAAAGIgYAIIABAGIgTACIABAGIgNABIABAGIhEAFIABAGQgHACgDgDIgDgEIgfACIAAgGIgxAEIgBgGIgZACIAAgGIgYABIAAgGIgZACIgmgPQgYgJgUAAIgBgNQgPgCgPgKQgNgJgJgMIgBgMIgTgLIgCgTQgNgVgGgOQgLgbAFgWIAGgBIgBgMIAHgBIgLg9IgkgKQgagHgOAFIABAGIgrAJIgLAZQgHAQgDAKQgCAMAHATQAHATgBAFIgMAHIABATIgLAHIABAMIguAvIABAMIgdAbIgiAuIgMABIgLATIgMABIgGANIgYACIgFANIgNABIABAGIgZACIABAGIgTABIABAHIgrADIAAAGIhKgBIgBgGIgrgDIgBgGIgmgJIgbgdIgMgFIgCgTIgGABIgBgNIgMABIgKgwIgGAAIgBgMIgGAAIgMhDIgGABIgDglIAHgBIgDgfIAGAAIgBgMIAMgHQAEgKgCgTQgCgRgEgEIgFgNQgCgCgMgCIgBgMQgMgCgEgEQgCgEgCgBIgSAIIAAgHIgZACIABAHQgWARgHAQIABAMIgGAAIABATIgGAAIghAuIgYAIIgiAuIgMABIgLATIgTACIABAGIgSAIIgFAMIgTACIABAGIgZAIIABAGIgfACIgFANIg3ALIAAAGIgSABIABAGIg+AFIAAAGIhiAIIAAgGIgTABIAAgGIgNABIAAgGIgTABIAAgGIgTABIgHgMIgZgEIgHgMIgagQIgBgNIgTgKIgBgNIgMABQgGgSgRgeIgGABIgBgTIgGABIgCgZIgGABIgBgGIAFgEQADgCgDgHIgGAAIgFhDIAMgHIADgsIAGAAIgBgNIAMgHIgBgMIApgiIASgCIAEgFQAEgGADgCIgCglIAGAAIgBgGIgGAAIgBgMIAGgBIAAgGIgNgFIgBgMQgMgCgEgDQgEgDgGgJIg9ALIgFATIgXAUIgLAUIgYABIgGANIgSACIAAAGIgSAHIgFANIgSACIAAAGIgMABIAAAGIgkAJIAAAGIgMABIgFANIg9AKIAAAHIgSABIAAAGIhQAGIAAgGIgygCIAAgGIgTABIAAgGIgTABIAAgGIgTACIAAgHIgTACIAAgGIgZgFIgBgGIgZgEIgHgMIgTgFIAAgGIgTACIgHgMIgtgcIgBgMQgKgCgIgFIgOgJIgBgMQgSgEgHgLQgHgQgIgKIgUgKIgBgNIgGABIgPgkIgGAAIgCgYIgGAAIgCgfIgHABIgDgxIAGgBIgEg3QABgIAMgnQAJgdABgTIASgIQABgQAHgJQAGgJABgQQAOgFAGgHQAGgHACgOIANgBIgBgMIAkAEQAVAEAYgCIAEgTIANAFQAGgDAUgjQAVglgEgJIgGABQgEgvgkgQQgfgOg6AGIgIgSIgZgEQgEgNgEgEQgEgEgOgCIgehHIgNgFIgBgMIgGAAIgBgMIgGAAIgDglIgGABIgDglIgGAAIgCgSIgGABIACgyIgGAAIgCgeIgGAAIgCgSIgGAAQgDgIAEgGIAEgFIgCgZIgGABIARheIAdgbQAEgEAIgRQAIgQACgCIAMgBIAGgNIAYgIIALgTQAHgFAQgEQASgEAHgDIgBgGIATgCIgBgGIATgBIgBgGIATgCIgBgGQAQgGAjAAQAhAAAPAFIABAGIAfAEIAvgdIAPg5IgHABIgBgTIgGABIgIgSIgMABQgDgIgEgHQgFgGgDgJQgagFABgQQACgcgGgLIgNgFIgCgfQgIACABgNIAGgBIgBgSIgHAAQgEgMAEgTQADgTgBgGIgGABIAEhwIASgIIgBgSIASgIIAFgMIAMgBIARgaIAYgIIAFgNIAegJIAAgGIAMgBIAAgGIAegCIAAgGIAMgBIAAgGIASgCIAAgGQASgHA4ACQAvACAVAEIAZgBIAAAGIAGgBQAFgBgCgDQAAgBAAAAQAAgBABAAQAAAAABAAQAAAAABAAIABAGIASgCIAAAHIAlgDIABAGIAYgCIABAGIAZgCIAHAMIASgBIABAGIAZAEIAAAGIATgBIAHAMIAmAJQAFANAMAHIAWAOQANAMAOAZQAOAaADAVIATAEIAAAGIAMAAIAIALIAkgCIAGgNIASgCQAFgDAEgRIAGgYIAGgBIgCglIAGAAIAJgmIANgBIgCgSQAOgFANgQQALgPAPgFIgBgNIANgGIAKgIIAAgGIAMgBIAAgGIASgCIAAgGIAkgPIgBgGIAZgCIgBgGIATgBIgBgHIAfgCIAAgGQANgFAYACQAQABAPADIAlgDIAAAGIAZgCIAAAHIAMgIIAmAXQAXAMAWACQAFAPAKAGQAIAGARABIABANIAOADQABABAEANIANgBIAHAMIATAEIAHASIANAFIAJAlIAGgBIABANIAGgBIADAlIgGABIARA2IAMAFIAPAYIAlADIgBgGIAHAAIAAAGIAGgBIAAgGIASgBIAAgGIAMgBQAFgDgBgIQgCgKACgEIAGgBQANgVgDgLIgKgVQgIgQACgLIAGAAIAAgNIAGAAIADgfIANgBIgBgNIAMgHIAQgUQAJgMACgNQAVgHAPgOIAFgNIASgCIALgTIATgBIgBgGIATgCIgBgGIATgBIgBgHIArgDIAAgGIAfgCIAAAGQAKABACgIQANgDAAAIIA3gEIAAAGIAsADIAAAGIATgCIAAAHIAZgCIAAAGIANgBIAHAMIAMgBIAHAMIANgBIAAAGIAMgBIAOARIAMgBIAVAYIAGgBIABAMIATALIABANIANAFIABASIAGAAQAFANgJAGIAFBDIAGAAIABAMIAGAAIACASIAGAAIANARQAKAGAigDQAggCAKgHIgBgGQATgHAJgQQALgSgHgYQgBgEgDACQgBAAAAAAQAAAAgBAAQAAgBAAgBQAAAAAAgBIAGgBIgCgYQAAgBgBgBQAAgBgBAAQAAgBgBAAQAAAAgBABQgBAAAAAAQAAAAgBAAQAAgBAAgBQAAAAgBgBIAMgHIgBgTIAMgHIgBgMIALgHIAcgoIANAAIgBgHIASgHIALgUQAhgZA+gKQA7gJAqALIAYgCIABAGIAMgBIAAAGIAmAEIAAAGIANgBIAZANQAPAHARAAIABANQASACAUANIABAGIAZAFIANARIAGAAIABAMIAaARIABAMIANAFIABAMIAGAAIAIAYIAGAAIACAYIAGAAIAIBcIAGAAIABAMIAaAQIAAAHQAEACAJABQAGAAAHAHQAhgBAOgKQAOgLADgaIAMgBQgCghgIgQQAFgFAHgPQAGgPAEgEIANgBIAQgaIAkgPIAFgNIATgBIgBgGIANgBIgBgGIAfgDIgBgGIANgBIgBgGIAfgCIAAgGQAPgGA9gCQA6gBAOAEIAAAGIATgCIAAAGIAZAFIABAGIASgCIAHAMIAMgBIAIAMIAMgBIAHAMIAMgBIAoAoIANgBIAHAMIAgAXIABAMIANAFIACASIAMAGIACASIAGAAIABAMIAGAAIAJAkIAGgBIADAsIAHgBIgKAmIgGAAIAAAHIAHgBIAIAYIAGAAIACAYIAaARIAAAGIAMgHIACADQAEADAHgBIgBgGIAGAAIABAGIASgCQAKgOAZgNIAFgYQAEgOAAgMQAJgDAFgDIAKgIIgBgHIAMgBIAGgMIAMgBIAFgNIAMgBIAGgNIAYgIIAFgNIAMgBIAAgGIAMgBIAAgGIAkgJIAFgNIAZAFIAAgGIASgCIAAgGIAfgCIgBgGIAGgBIABAGIAMgHIAAAGQANgBAqgKQAogIAWAEIAAAGIAMgHIAAAGIAmAEIAAAGIAmAJIABAHIAMgBIAAAGIAZAEIAHAMIANgBIAHAMIAZAEIAAAGIAMAHQAEACAKACIABAMQAIABAIAEQAHADAJACIAHASIAGgBIAIASIANAGIAPAjIAGAAQANAaADAnQABAWgBAwIghAuIgMABIAAAGIgMABIAAAGIgfACIABAGIgMABIgEAGQgEAFgEADIgBA+IANAFIgGANIAGgBQAIALAFAEQAIAGAMABQAXASAmgVQAsgaAeAHIAAAGIA4AIIABAGIAMgBQAJAEAPAIQAOAHANACIABAMQAQACAFAEQAHAEAFANQAOACAOAMQARAPAHAEIABAMIAMgBQADAOAHAPIANAZQAHASgFAYQgDANgGAUIACAfIgGAAIACAZIgGAAIACAfIgGAAIABANIgRANIABANQgGAMgQAaIgNABIABANIgSAHIgbAoIgxAJIAAAHIgMABIAAAFIgGgFQgBAAgBABQAAAAgBAAQAAABAAAAQAAAAAAAAQACADgFABQgLACgMgJQgMgKgJADIAAAGIgZACIABAFQgUACgGAEQgHAFgCAQQgFAHAAASIADAfIAMgBQADABAIAPQAKAPAHAEIAAAGIAfgCIAAAGIAlgJIAAAGIANgBIANASIANgBIAAAGIAGAAIABAMIAOADQACACAEAMIAMgBIAIASIAaAQIABANIANAFIABAMIAGAAQAQARAFASQAIAIAEAcQAEAbgEARIgGAAIgDAsIgGAAIgEAgIgGAAIABAMIgMAHIABANIgRANIABATIgGAAIgKAPQgEAHgCAKIgMABQgCALgCAEIgHALIgGAAIgFATQgEAFgHABQgDAAgDAOIgSAIIABAGQgFADgIACQgCAAgCAPQgOAEgKAJIgRAPIgZAIIABAGIgMABIAAAGIgZACIABAGIgTABIABAHQgSAHgngDQgkgDgNgGIgHgMIg3AFQgKAOgTANIgBAgQAAAYAGALIANAGIALA9IgGAAIABAMIgHABIACASIgMAHIgEAZIgdAbIgEAaIgMAHIgiAuIgMAAQgEATgNAKQgTAKgKAIIgFANIgNABIABAGIgMABIgLATIgYAIIAAAHIgMABIgFAMQgIAGgPADQgTAEgHADIABAHIgfACIAAAGIgkADIAAAGQgVAIgfgEQgMgCgdgHIgZACIAAgGIgNABIAAgHIgMABIgHgMIg5gOIgHgMIgNABIgHgLIgZgFIgOgRIgMABIgFgGQgEgEgEgCIgBgMQgNgCgNgIQgOgJgLgCQgEgMgGgKIgMgTIgGAAIgJgkIAGgBIAAgGIgGAAIgNgWQgHgNgIgGIgBgGIgSABIgBgGIgMABIgBgGIgGABIABAGIglADIgbAzIACAfIgGAAIACAZIgGAAIgEAaIgYAUIABAMIgGABQgDAFAAAIQAAABgOAFQgDASgLAKIgaATIABAGIgNABIgWAhIgSABIgFANIgNABIABAGIgNABIABAGIgTABIABAGIgTACIABAGIglADIAAAGIgrADIABAGIhKAGIAAgGQgBAAgBAAQgBAAAAAAQgBAAAAABQAAAAAAAAQABADgDADg");
	this.shape_12.setTransform(262.1,134.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AMfU6IgTABIAAgGIggACIAAgGIgMABIAAgGIgaACIAAgGIgugVQgdgNgVgCIgBgNQgSgJgSgSIgegkIgTgLIgBgMIgNgFIgIgYIgNgFIgBgNIgGABIgBgNIgGABIgJglIgNgFIAAgMIgHAAIAFgTIgGABIgFgEIgIgBIgFATQgNAEgKANQgLANgMAEQgEASgMAIQgPAHgKAHIgFANIgNABIgQAaIgfAJIgEAMIgNABIAAAHIgSABIAAAGIgSACIAAAGIgYACIABAGIgHAAIAAgGIgGAAIAAAHIgeACIAAAGQgLAEgggDQgUgDgSgEIhbABIgBgGIgSABIgBgGIgNABIAAgGIgZACIAAgHIgfgDIAAgGIgNABIgBgHIgSACIgBgGIglgKIgBgGIgMABIAAgGIgSgNQgKgHgMgCQgDgKgFgHIgMgMIgGAAIAFgNIgNgFIgBgMIgMgFIgBgNIgHABQgCgEAAgNQAAgLgGgDIgTACIgVAtIgMABQgCAMgKANIgQAUIgGABIABAMQgDAFgNALQgJAHgDAQIgMABQgPAqhBAgQgLAGhoAoIglADIABAGQgRAGgqAAQgmABgPgEIAAgHIgTACIAAgGIgMABIgBgHIg5gOIghgiIgUgLIgBgNIgagQIgIgeIgNgGIgCgYIgGAAIgIgYIgGABQgKgTgFggQgDgUgDgnQgSAFgSAQIABAGIgMABIgLAUIgNABIgLATIgXAIIgFANIgZAIIAAAGIgkAPIABAGIgTACIABAGIgMABIAAAGIgeAIIABAHIggACIABAGIgSABIAAAHIglACIABAHIhKALIABAHQgBAAgBAAQAAAAgBAAQAAgBAAAAQAAgBAAAAQABgEgEAAQgJgBgCAEIgCAEIgMgFIAAAGIhjABIAAgGIgZACIgBgGIgLABIgBgGIg5gPIgHgLIgNAAIgTgXIgNABIgUgXIgGABIgIgZIgagQIgQgkIgGAAIgHgSIgNgFIgCgZIgGABIgCgTIgGABIgCgfIgGABIgBgNIgGABIAAgGIAFgBIgDgxQAAgEADABQABAAAAAAQABAAAAgBQAAAAABgBQAAgBAAgBIgHABIgBgMIAKgsIgHAAIgEgEIgIgBQgJALgSAGIgiAMIABAGIgMABIABAGIgsAKQgeANgpAKQg5AOgegGIgBgHIgeADIAAgGIgTABIAAgGIgTABIAAgGIgZACIAAgGIgMABIgBgGIgmgEIAAgGIgMABIgIgMIgegEIgIgLIgMABIgBgHIgTgEIgBgNQgggFhHg9IgRgRQgJgIgOgDIgBgSIgLgGQgFgDgKgCIgIgeIgNgFIgBgTIgHABIgBgNIgGABIgPgkIgGAAIgCgeIgGAAQgIgSAAgsQAAgpAFgUIgEgrIAGgBIgBgZIAFAAIgCgZIAHAAIgBgMIAGgBIgHgMIAGAAIgDgfIAMgHIgBgMIAGgBIAAgMIALgNIgBgNIARgNIAAgNQAGgNAMgLQANgMAPgEQACgLAGgIQAGgJACgLIgtgbIgBgMIgOgGIgBgMQgDgEgPgJQgKgGgFgPQgNgDgBgBIgGgOIgGABIgQg2IgTgEQACgIAAgFQAAgJgFgJIgMgFIABg+IgGAAIgCgSIgGAAIAFgTIgGABIgCgTIgGABIgDglIgGAAIAEgfIAGAAIgFhEIAGAAIgHgMIAEhqIAGAAIAFggIALgHIgBgMIAGAAIgBgTIASgHIALgUIATgBIAEgNIAZgIIAKgTIANgBIAQgaIAkgPIAGgNIASgCIAAgGIAMgBIgBgGIA3gKIAAgGIAMgBIALgUIAYgCQAWgGADgHQAEgGgBgaQgXgTgChFQAAhQgCgcIgEgxQgDgdAGgPIAGAAIAFgZIAFgBIgBgSIAGgBIAGgLIAFgOIAMgBIAtg7IAYgIIAWghIArgJIgBgHIAMgBIAAgGIArgJIgBgGIA3gLIAAgGIA9gFIAAgGQAEgBAuAKIAxgEIAAAGIATgBIAAAGIAlgDIABAGIAYgCIABAHIASgCIABAGIArADIABAGIAegCIABAGIATgBIAAAGIAmAJIABAGIAMAAIAHALIAMgBIAAAHIAUAEIABANIATAEIANASIATAFIABAMIAGgBIAHAMIAZgCIAVgtIASgHIgBgMIALgOIANgBIAhguIAYgIIAGgMIAMgBIAAgGIAMgBIgBgGIATgCIALgTIATgCIAEgMIArgKIAAgGIASgBIAAgHIAegCIABgEQADgEAIABIABAGIAGAAQANgBgBgHQAOgEAgADQAUADAVAEIAfgCIAAAGIAfAEIAHAMIAMgBIABAGIASgCIABAHIANgBIAHALIAYAFIABAGIATAFIAHALIAZAFIAbAdIAaAQIAHAMIAMgBIAOALIABANIAMAFQAHAHAFANIAKAWIA5hJIARgNIgBgNIAYgIQAEgUAFgBIAUgGIAAgGIAMgBIAFgNIAYgIIAAgGIAMgBIgBgGIAfgIIAFgNIAfgDIAAgGIAFAAIAHAGIgBgHQAYgHApAAQAmgBAVAFIAxgEIABAGIASgBIABAGIAYgCIABAHIAMgBIABAGIAegDIABAHIAFADIACAIQAeABAbAUIAHAMIAmAJIAHAMIATAFIABAMIAOALIALgBIAIASIANAGIABAMIANgBQAEACAFANQAFAKATADIAEgZQAHgCAHgFQAHgEAHgDIgBgNIANgBIgBgMQAKgEAGgFIAOgMIgBgGIAMgBIAGgNIAFAAIAGgNIAegIIgBgHIANAAIAFgNIASgCIAAgGIASgBIAAgGIAZgCIgBgGQAYgKAmAEQAQACAiAHIAkgCIABAGIANgBIAAAGIAZgCIAAAGIAmAEIAAAGIAZAEIABAGIAlADIAHAMIAZAFQAFACAjAZQAZASATAEIABAMQAZAMAjAqQAlAtAGAdIAZAFIAFgTIAGgBIAFgNIAMAAIALgUIANgBIgBgMQAUgDAGgFIAPgOIAZgIIgBgGIB+gpIANAGIAAgHIANAGIAAgHQARgGAtAGQAdAEAbAGIAegCIAHALIATgBIAAAGIANgBIAHAMIASgBIAHALIANAAIAGALIANgBIAOASIAMgBIAbAdIAMgBIAUAXIAuAiIAIAYIAOAFIABAMIAMAGIAHASIAHgBIAJAeIARAhQAJATADAUQATgCAPgKQAQgNAKgEIgBgMQANgDAFgDQAEgBAIgIIAAgGIAXgIIAGgNIAMgBIAAgGICEg1IAMAFIAGgHIAAAGIAHAAIAAgGIBDgFIAEgFQAGgEAIABQABAAABAAQABABAAAAQABAAAAABQAAAAAAABQgBABAAAAQAAABABAAQAAAAABAAQABABABAAIALgHIABAGIAlgDIABAGIAqgDIABAGIASgBIABAGIAmAJIAAAGIAMgBIABAHIAmAJIAHAMIASgCIAOASIAMgBIABAGIAMgBIABAGIASgBIAoAoIANAFIABANIAgAWIABAMIAOAFIABANIAMAFIABAMIAGAAIABASIAHAAIAAAMIAHAAIAIAYIAGgBQAJARAEAdQAFApACALIAGgBIAAAGIgFABIgBBKIgGABIgEAfIgGAAQgBAIADABIAFADIABAGIArADIAAAGIANgBIAAAHIATgCIAAAGIATgBIAHAMIAZAEIAGAMIANgBIAHAMIANgBIANARIANgBIA1A0IAGAAIABAMIANgBQADAPACAGQADAIAHAHIAMAFIABAMIAGAAIAJAeIAGAAIACAYIAGAAQACAKgHADIAAAGIAGgBIABAGIgGABIAHASIgGAAIAIAZIgGAAIgEAZIAEADQAEADgBAGIgMAHIAIASIgHAAIABANIgGAAIACAfIgGAAIACATIgGAAIABATIgGAAIABAZIgLAHIABAMIgLAOIgFAZIgRANIgFAYIgWAVIgGATQgFAFgkAQQgYAMgFAWQATAIAPATIAbAkIATALIACASIAaARIABAMIAGAAIAIAYIAGgBIACAZIANAFQAGANAAAnQAAAjgFASIADAfIgGABIAAAMIgGAAIACATIgGAAIABATIgGAAIABAMIgGABIABAMIgMAHIACATIgSAOIgJAlIgNABQgEAUgRAUIgdAgIgXAhIgLABIgLATIgNABIgLAUIgMAAIgQAbIgNAAIgFANIgeAJIgGAMIgYACIAAAHIgMAAIAAAHIgeACIAAAGIgfACIABAHIgfACIABAGIgZACIgBgGIgSAHQACAjgOAnQgSAxgCAQIgNABQgBAKgCAFIgHALIgGAAIABAMIgMAHIABANIgMAHIgaAfQgPAVgDANQgMAEgJAKQgJAKgMAEIABAMQgNAEgDAFQgEAEgCAOQgMAEgLAJIgTAPIgMABIgFAMIgYACIgGANIgSACIAAAGIgMABIABAGIgfAIIAAAGIgYACIABAGIgTACIABAGIg4AEIABAGIhdAHQgegGglgQIg9geIgfgDIgHgMIgMABIAAgHIgNABIgNgRIgNABIgBgGQgRgNgVgDIAAgMIgNABIgBgNQgNgCgBgBIgGgOQgMgCgKgIQgKgIgNgDIgBgMIgagRIgbA6IgLABQgDAMgIAKQgIALgDAMIgMABIABAMIgMAHIABAGIgNABIgWAhIgMABIgGANQgDADgIABQgCABgEAOIgeAJIgFANIgYAIIAAAGIgZACIABAGIgTABIABAGIgMABIAAAHIgfACIABAGIgrADIABAHQgOAEgWgCIgVgEIhDAFg");
	this.shape_13.setTransform(262.5,134.3);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.instance,this.shape_9,this.instance_1,this.shape_10,this.instance_2,this.shape_11,this.instance_3,this.shape_12,this.shape_13];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.instance_3},{t:this.shape_11},{t:this.instance_2},{t:this.shape_10},{t:this.instance_1},{t:this.shape_9},{t:this.instance},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_0, new cjs.Rectangle(0.1,0,525,268.7), null);


(lib.ClipGroup = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgpAAVAMAAAgp/MBSBAAAMAAAAp/g");
	mask.setTransform(262.5,134.4);

	// レイヤー 3
	this.instance = new lib.ClipGroup_0();
	this.instance.parent = this;
	this.instance.setTransform(262.5,134.3,1,1,0,0,0,262.5,134.3);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup, new cjs.Rectangle(0.1,0,525,268.7), null);


(lib.トゥイーン1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.fly_1();
	this.instance.parent = this;
	this.instance.setTransform(0.5,-0.6,1,1,26.2,0,0,0.1,-0.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AgehNIhBADIgXAkABggQIAXA7IgtAj");
	this.shape.setTransform(-7.6,14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-24.1,72.3,52.5);


// stage content:
(lib.fly = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_275 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(275).call(this.frame_275).wait(1));

	// ladybug
	this.instance = new lib.wolk();
	this.instance.parent = this;
	this.instance.setTransform(421.6,934.6,0.652,0.652,-38.8,0,0,-0.1,2.6);

	this.instance_1 = new lib.wolk2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(385.1,905.4,0.664,0.664,-1.8,0,0,-0.7,0.2);
	this.instance_1._off = true;

	this.instance_2 = new lib.トゥイーン1("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(371.8,441.5,0.675,0.675,-11,0,0,0.1,0.2);

	this.instance_3 = new lib.fly_1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(383.2,427.2,0.701,0.701,26.2,0,0,0.2,-0.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},10).to({state:[{t:this.instance}]},8).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},104).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance_2}]},8).to({state:[{t:this.instance_3}]},13).to({state:[{t:this.instance_3}]},92).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:385.6,y:905.6},10).to({rotation:-7.5},8).to({_off:true,regX:-0.7,regY:0.2,scaleX:0.66,scaleY:0.66,rotation:-1.8,x:385.1,y:905.4},1).wait(105).to({_off:false,regX:0.1,regY:2.6,scaleX:0.59,scaleY:0.59,rotation:10.7,x:362.1,y:558},0).wait(1).to({regY:2.7,rotation:28.2,x:372.7,y:549.3},0).wait(2).to({regX:0.2,rotation:36.9,x:377.1,y:548.1},0).wait(3).to({regY:2.8,rotation:37.9,x:384.7,y:542.9},0).wait(2).to({regX:0.1,regY:2.6,x:390.9,y:536.8},0).wait(2).to({x:395.1,y:534},0).wait(2).to({regX:0.3,regY:2.8,rotation:33.2,x:409.2,y:523.2},0).wait(2).to({regX:0.1,regY:2.6,rotation:21.8,x:412.1,y:517.6},0).wait(2).to({x:416.9,y:511.2},0).wait(1).to({regX:0.3,rotation:17,x:418.9,y:501.9},0).wait(2).to({regX:0.2,regY:2.7,rotation:4.8,x:421.1,y:488.9},0).wait(2).to({regX:0.4,regY:2.8,rotation:-15.9,x:417.4,y:475.5},0).wait(3).to({regX:0.3,regY:2.9,rotation:-34.3,x:407.9,y:462},0).wait(2).to({rotation:-43.9,x:401.2,y:456.8},0).wait(3).to({regX:0.2,regY:3.1,rotation:-46.8,x:393.6,y:449.8},0).wait(3).to({rotation:-51.5,x:384.8,y:445.1},0).wait(3).to({rotation:-19.6,x:371.7,y:445.6},0).wait(3).to({regX:0.4,rotation:5.6,x:372.4,y:445.2},0).to({_off:true},8).wait(106));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(18).to({_off:false},1).to({rotation:-1.8,x:362.1,y:572.5},104).to({_off:true},1).wait(152));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(183).to({_off:false},0).to({regX:0.1,scaleX:0.6,scaleY:0.6,x:778,y:88.3},92).wait(1));

	// BG
	this.instance_4 = new lib.ClipGroup();
	this.instance_4.parent = this;
	this.instance_4.setTransform(388.9,218.9,1.205,1.205,0,0,0,262.6,134.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(276));

	// レイヤー 4
	this.instance_5 = new lib.flower("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(380.7,774.6,2.065,2.065);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(276));

	// レイヤー 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9EC260").s().p("EgsjAIOIAdu5QBMAPBjAGQDFALBrguQB8g0C8AAQCvABCYAtQB3AjDFgdQBsgQC6glQBwgODdgJQDAgICdAAQByAAEzAXIHoAnQCrAOC2gQQBogJC+gZQCUgQD0gBQD8gCC8APQCfANF3BFQC7AiCbAfIgfNyg");
	this.shape.setTransform(374,1195.4,1.458,2.894);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(276));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(333.1,723.9,831.8,1290.9);
// library properties:
lib.properties = {
	width: 750,
	height: 1334,
	fps: 24,
	color: "#66CCFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;