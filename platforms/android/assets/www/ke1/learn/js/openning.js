/**
 * Created by mizo on 2017/02/27.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // 画面を横にロック
        screen.lockOrientation('landscape');
        progress.use('learn');
        (function($){
            $(function(){
                /**
                 * ボタンアクション
                 */

                $(".scean2 ul li a").on({
                    "touchstart": function(){
                        $("img", this).addClass("on");
                    },
                    "touchend": function(){
                        $("img", this).removeClass("on");
                    }
                });

                $(".btn_start").on({
                    "touchstart": function(){
                        $("img", this).addClass("on");
                    },
                    "touchend": function(){
                        $("img", this).removeClass("on");
                    }
                });


                /**
                 * アニメーション
                 */
                setTimeout(function(){
                    $(".cloud").addClass("on");
                }, 300);

                $(".stars li").each(function(i, elem){
                    var i = i;
                    var target = elem;
                    setTimeout(function(){
                        $(target).addClass("on");
                    }, i*100);
                });

                setTimeout(function(){
                    $(".title").addClass("on");
                }, 400);

                setTimeout(function(){
                    $(".mother").addClass("on");
                }, 800);

                setTimeout(function(){
                    $(".scean1").addClass("on");
                    $(".scean2").addClass("on");
                }, 2000);

            });
        })(jQuery);


    }
};

app.initialize();

// (function($){
//     $(function(){
//         /**
//          * ボタンアクション
//          */
//
//         $(".scean2 ul li a").on({
//             "touchstart": function(){
//                 $("img", this).addClass("on");
//             },
//             "touchend": function(){
//                 $("img", this).removeClass("on");
//             }
//         });
//
//         $(".btn_start").on({
//             "touchstart": function(){
//                 $("img", this).addClass("on");
//             },
//             "touchend": function(){
//                 $("img", this).removeClass("on");
//             }
//         });
//
//
//         /**
//          * アニメーション
//          */
//         setTimeout(function(){
//             $(".cloud").addClass("on");
//         }, 100);
//
//         $(".stars li").each(function(i, elem){
//             var i = i;
//             var target = elem;
//             setTimeout(function(){
//                 $(target).addClass("on");
//             }, i*50);
//         });
//
//         setTimeout(function(){
//             $(".title").addClass("on");
//         }, 200);
//
//         setTimeout(function(){
//             $(".mother").addClass("on");
//         }, 600);
//
//         setTimeout(function(){
//             $(".scean1").addClass("on");
//             $(".scean2").addClass("on");
//         }, 1500);
//
//     });
// })(jQuery);