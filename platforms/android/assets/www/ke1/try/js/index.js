var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // 画面を横にロック
        screen.lockOrientation('landscape');
        
        /*
        * strawberry set
        */

        var berry = ['sweet', 'sweet', 'sour', 'sour', 'sweet'];
        var count = 0;
        var loop_add_berry = function(){
            setTimeout(function(){
                if(count < 5){
                    $(".sts").append('<li class="st' + (count + 1) + ' ' + berry[count] + '"><img src="img/st_transparency.png" alt="苺"></li>');
                    count++;
                    loop_add_berry();
                }else{
                    $('.sts li').clickSpark({
                        particleImagePath: '../../img/particle-2.png',
                        particleCount: 10,
                        particleSpeed: 5,
                        particleSize: 20,
                        particleDuration: 10,
                        particleRotationSpeed: 10
                    });
                    count = 0;
                }
            }, 200)

        }

        loop_add_berry();

        
        /*
        * sound set
        */
        var path;
        if(device.platform == 'iOS'){
            path = "sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/sound/";
        }

        var s_sweet = new Media(path + "sweet.mp3");
        var s_sour = new Media(path + "sour.mp3");

        var mediaSuccess = function(){
            console.log("success");
        };
        var mediaError = function(err){
            console.log("Error :" +  err.code);
        };

        // アクション後のリセット
        var reset_state = function(){
            setTimeout(function(){
                $(".baloon").removeClass("active");
                $(".taste").hide();
                $(".taste li").removeClass("active");
                $(".childe img").removeClass("active");
                $(".childe img.normal").addClass("active");

                console.log($(".sts li"));
                if(!$(".sts li").length){
                    loop_add_berry();
                }
            }, 2000);
        }


        var mediaStatus = function(status){

            if(status == 4){
                console.log(status);
                reset_state();
                $(".baloon").addClass("active");
                $(".childe img").removeClass("active");

                if(this.taste == "sweet"){
                    s_sweet.play();
                    $(".childe img.sweet").addClass("active");
                    $(".taste").show();
                    $(".taste .sweet").addClass("active");
                }
                if(this.taste == "sour"){
                    s_sour.play();
                    $(".childe img.sour").addClass("active");
                    $(".taste").show();
                    $(".taste .sour").addClass("active");
                }
            }
        };


        var s_picked = new Media(path + "picked.mp3", mediaSuccess, mediaError, mediaStatus);


        $(".sts").on("oTransitionEnd mozTransitionEnd webkitTransitionEnd transitionend", 'li',function(){
            $(this).remove();
        });

        $(".sts").on('touchend', 'li', function(){
            if($(this).hasClass('sweet')){
                s_picked.taste = 'sweet';
            }
            if($(this).hasClass('sour')){
                s_picked.taste = 'sour';
            }

            s_picked.play();
            $(this).addClass("picked");
        });
    }
};

app.initialize();