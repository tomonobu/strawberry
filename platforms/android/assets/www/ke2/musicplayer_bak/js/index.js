var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // 画面を横にロック
        screen.lockOrientation('landscape');


        /*
         歌詞セット
         */

        var lyrics = [
            {word: "♬ 〜", time:5},
            {word: "A, B, C, D, E, F, G", time: 15},
            {word: "H, I, J, K, L, M, N", time: 25},
            {word: "O, p, Q, R, S, T, U", time: 35},
            {word: "V, W, X, Y, Z", time: 45}
        ];
        words = "";
        for(i = 0; i < lyrics.length; i++){
            words += '<li>' + lyrics[i]['word'] + '</li>';
        }
        $(".mp_bottom ul").append(words);


        /*
         * sound set
         */
        var path;
        if(device.platform == 'iOS'){
            path = "sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/sound/";
        }
        var mediaSuccess = function () {
            console.log("media success");
        };
        var mediaError = function (e) {
            window.alert("Error :" +  e.code);
        };
        var mediaStatus = function (status) {

            if (status != Media.MEDIA_NONE) {
                $(".speaker img").removeClass("active");
            }
            if (status == Media.MEDIA_RUNNING) {
                song.setVolume($("#score").val() / 100);
                $(".ui-field-contain").on("change", "#score", function(){
                    console.log($(this).val() / 100);
                    song.setVolume($(this).val() / 100);
                });

                $(".speaker img").addClass("active");

                var lyrics_count = 0;
                var h = 45;
                timer = setInterval(function() {
                    song.getCurrentPosition(function(sec) {
                        if(sec > 0) {
                            $('.time').text(sec);
                            console.log(lyrics[lyrics_count]['time']);
                            if(lyrics_count < lyrics.length && sec > lyrics[lyrics_count]['time']){
                                 $(".mp_bottom ul").animate({top: -1 * h * (lyrics_count + 1)}, 500, 'linear');
                                 lyrics_count++;
                            }
                        }
                    });
                }, 200);
            }
            else if (status == Media.MEDIA_STOPPED) {
                if($(this).hasClass('active')){
                    song.play();
                }
                $(".speaker img").removeClass("active");
            }
            else if (status == Media.Media.MEDIA_PAUSED) {
                $(".speaker img").removeClass("active");
            }
        };

        var song = new Media(path + "/song/ke1/abc_song_2017_0110-1.mp3", mediaSuccess, mediaError, mediaStatus);

        $(".play").on({
            'touchstart' : function(){
                if($(this).hasClass('active')){
                    $('img', this).attr("src", "images/mp2_bt2b.png");
                }else{
                    $('img', this).attr("src", "images/mp2_bt5b.png");
                }
            },
            'touchend': function(){
                if($(this).hasClass('active')){
                    song.pause();
                    $(this).removeClass('active');
                    $('img', this).attr("src", "images/mp2_bt2a.png");
                }else{
                    song.play();
                    $(this).addClass('active');
                    $('img', this).attr("src", "images/mp2_bt5a.png");
                }
            }
        });

        $(".prev").on({
            'touchstart' : function(){
                $('img', this).attr("src", "images/mp2_bt1b.png");
            },
            'touchend': function(){
                if($(".play").hasClass('active')){
                    song.stop();
                    song.play();
                }else{
                    song.stop();
                }
                $('img', this).attr("src", "images/mp2_bt1a.png");
            }
        });

        $(".repeat").on('touchend', function(){
            if($(this).hasClass('active')){
                $(this).removeClass("active");
                $('img', this).attr("src", "images/mp2_bt4b.png");
            }else{
                $(this).addClass("active");
                $('img', this).attr("src", "images/mp2_bt4a.png");
            }
        });




    }
};

app.initialize();