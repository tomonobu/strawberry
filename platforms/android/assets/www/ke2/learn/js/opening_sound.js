/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var sound = [];
var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        console.log("ready");
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {
        // 画面を横にロック
        console.log("start");
        screen.lockOrientation('landscape');

        /*
         * sound set
         */
        var path;
        if (device.platform == 'iOS') {
            path = "ke2/learn/sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke2/learn/sound/";
        }

        var mediaSuccess = function () {
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if (status != Media.MEDIA_NONE) {
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if (status == Media.MEDIA_STARTING) {
                // 再生が開始されたタイミング
                console.log("media: starting");
            }
            if (status == Media.MEDIA_RUNNING) {
                // 再生が開始されたタイミング
                console.log("media: running");
            }
            if (status == Media.MEDIA_PAUSED) {
                // 再生が開始されたタイミング
                console.log("media: paused");
            }
            else if (status == Media.MEDIA_STOPPED) {
                // 停止された時の処理
                console.log("media: stopped");
                location.href = "index.html";
            }
            console.log('==============================================');
        }


        sound['opening'] = new Media(path + "ji_037.mp3", mediaSuccess, mediaError, mediaStatus);
        sound['opening'].play();


    }
};

app.initialize();

