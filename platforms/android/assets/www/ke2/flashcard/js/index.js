/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var sound= [];
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        // 画面を横にロック
        screen.lockOrientation('portrait');
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {


        /*
         * sound set
         */
        var path;
        if(device.platform == 'iOS'){
            path = "ke2/flashcard/sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke2/flashcard/sound/";
        }

        var mediaSuccess = function(){
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if(status != Media.MEDIA_NONE){
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if(status == Media.MEDIA_STARTING){
                // 再生が開始されたタイミング
                console.log("media: starting");
            }
            if(status == Media.MEDIA_RUNNING){
                // 再生が開始されたタイミング
                console.log("media: running");
            }
            if(status == Media.MEDIA_PAUSED){
                // 再生が開始されたタイミング
                console.log("media: paused");
            }
            else if(status == Media.MEDIA_STOPPED){
                // 停止された時の処理
                console.log("media: stopped");
            }
            console.log('==============================================');
        }

        sound[1] = new Media(path + "apple.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[2] = new Media(path + "banana.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[3] = new Media(path + "egg.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[4] = new Media(path + "cherryblossoms.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[5] = new Media(path + "sandwich.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[6] = new Media(path + "tomato.mp3", mediaSuccess, mediaError, mediaStatus);

        sound[7] = new Media(path + "perfect.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[8] = new Media(path + "fabulous.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[9] = new Media(path + "wonderful.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[10] = new Media(path + "great.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[11] = new Media(path + "nice.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[12] = new Media(path + "good.mp3", mediaSuccess, mediaError, mediaStatus);

    }
};

app.initialize();



(function($){
    $(function(){
        $('.btn_a').on('touchstart', function(){
            $('img', this).addClass('on');
        });
        $('.btn_a').on('touchend', function(){
            $('img', this).removeClass('on');
        });
    });

})(jQuery);