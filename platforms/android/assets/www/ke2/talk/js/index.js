/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var sound = [];
var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        // 画面を縦にロック
        screen.lockOrientation('portrait');
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {
        progress.use('talk');
        /*
         * sound set
         */
        var path;
        if(device.platform == 'iOS'){
            path = "ke2/talk/sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke2/talk/sound/";
        }

        var mediaSuccess = function(){
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if(status != Media.MEDIA_NONE){
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if(status == Media.MEDIA_STARTING){
                // 再生が開始されたタイミング
                console.log("media: starting");
            }
            if(status == Media.MEDIA_RUNNING){
                // 再生が開始されたタイミング
                console.log("media: running");
            }
            if(status == Media.MEDIA_PAUSED){
                // 再生が開始されたタイミング
                console.log("media: paused");
            }
            else if(status == Media.MEDIA_STOPPED){
                // 停止された時の処理
                console.log("media: stopped");
            }
            console.log('==============================================');
        }


        sound[1] = new Media(path + "ilike.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[2] = new Media(path + "ilikecherryblossoms.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[3] = new Media(path + "say_ilikefriedchiken.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[4] = new Media(path + "say_ilikericeballs.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[5] = new Media(path + "say_ilikesandwitches.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[6] = new Media(path + "ilikethisone.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[7] = new Media(path + "say_iliketomatoes.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[8] = new Media(path + "KE2_chapter4.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[9] = new Media(path + "seeyounext_effect.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[10] = new Media(path + "sobeautiful.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[11] = new Media(path + "whatfooddoyoulike.mp3", mediaSuccess, mediaError, mediaStatus);
    }
};

app.initialize();

(function($){
    var atsnow = new ATSnow({
        classname : 'sakura',
        count : 10,
        interval : 150,
        maxSize : 34,
        minSize : 20,
        leftMargin : 10,
        rightMargin : 20,
        bottomMargin : 0,
        maxDistance : 100,
        minDistance : 10
    });

    //遷移後のアクション設定
    $(document).on('pagecontainerchange', function (e, d) {
        console.log(d.toPage[0].id);
        if (d.toPage[0].id == 'scene1') {
            //sound[1].play(); //play sound
            $('body').pagecontainer('load', 'scene2.html');

            setTimeout(function () {
                $('body').pagecontainer('change', 'scene2.html', {transition: 'fade'});
            }, 3000);

        }
        if(d.toPage[0].id == 'scene2'){
            sound[10].play(); //play sound
            $('body').pagecontainer('load', 'scene3.html');

            atsnow.load();

            setTimeout(function(){
                $(".btn_next.scene2.btn").addClass("active");
            }, 1000);

            $("#scene2").on('click', ".btn_next.scene2.btn", function(){
                atsnow.clear();
                $(".btn_next.scene2.btn").removeClass("active");
                $('body').pagecontainer('change', 'scene3.html', {transition: 'fade'});
                $(".btn_next.scene2.scene2.btn").removeClass("active");
            });
        }

        if(d.toPage[0].id == 'scene3'){
            sound[11].play(); //play sound
            $('body').pagecontainer('load', 'scene4.html');

            setTimeout(function(){
                $(".btn_next.scene3.btn").addClass("active");
            }, 1000);

            $("#scene3").on('click', ".btn_next.scene3.btn", function(){

                $(".btn_next.scene3.btn").removeClass("active");

                if($('.motti2').hasClass('active')){
                    $('.motti2').removeClass('active');
                    $('.motti4').addClass('active');
                    $('.motti4').addClass('active').on('animationend, webkitAnimationEnd', function(){
                        $('.motti4').removeClass('active');
                        $('.motti5').addClass('active');
                        $('.tina').addClass('active');
                        $(".btn_next.scene3.btn").addClass("active");
                    });
                }

                if($('.motti5').hasClass('active')){
                    $(".btn_next.scene3.btn").removeClass("active");
                    $('body').pagecontainer('change', 'scene4.html', {transition: 'fade'});
                }
            });
        }

        if(d.toPage[0].id == 'scene5a' || d.toPage[0].id == 'scene5b' || d.toPage[0].id == 'scene5c' || d.toPage[0].id == 'scene5d'){
            $('body').pagecontainer('load', 'scene6.html');

            if(d.toPage[0].id == 'scene5a') sound[4].play(); //play sound
            if(d.toPage[0].id == 'scene5b') sound[7].play(); //play sound
            if(d.toPage[0].id == 'scene5c') sound[5].play(); //play sound
            if(d.toPage[0].id == 'scene5d') sound[3].play(); //play sound

            setTimeout(function(){
                $(".btn_next.btn").addClass("active");
            }, 2000);

            $("#scene3").on('click', ".btn_next.scene3.btn", function(){

                $(".btn_next.scene5.btn").removeClass("active");

                if($('.motti2').hasClass('active')){
                    $('.motti2').removeClass('active');
                    $('.motti4').addClass('active');
                    $('.motti4').addClass('active').on('animationend, webkitAnimationEnd', function(){
                        $('.motti4').removeClass('active');
                        $('.motti5').addClass('active');
                        $('.tina').addClass('active');
                        $(".btn_next.scene5.btn").addClass("active");
                    });
                }

                if($('.motti5').hasClass('active')){
                    $(".btn_next.scene5.btn").removeClass("active");
                    $('body').pagecontainer('change', 'scene4.html', {transition: 'fade'});
                }
            });
        }


        if(d.toPage[0].id == 'scene6'){
            sound[1].play();
            $('body').pagecontainer('load', 'scene7.html');
            atsnow.load();
            setTimeout(function(){
                $(".motti_hana1").fadeIn(500, 'linear', function(){
                    setTimeout(function(){
                        $(".motti_hana1").hide();
                        $(".motti_hana2").show();
                        setTimeout(function(){
                            $(".motti_hana2").hide();
                            $(".motti_hana3").show();
                            $(".btn_next.scene6.btn").css('display', 'block');
                        }, 1000);
                    }, 1000);
                });
            }, 800);

            $(".btn_next.scene6.btn").on('click', function(){
                if($(this).hasClass('next')){
                    atsnow.clear();
                    $(".btn_next.scene6.btn").css('display', 'none');
                    $('body').pagecontainer('change', 'scene7.html', {transition: 'fade'});
                }else{
                    sound[2].play();
                    $(".message1").removeClass("active");
                    $(".message2").addClass("active");
                    $(this).addClass("next");
                }
            });

        }

        if(d.toPage[0].id == 'scene7') {
            $('body').pagecontainer('load', 'scene7.html');

            $(".motti").animate({'right': '0', 'top': '15%'}, 1000, 'easeInOutQuart', function(){
                $(".balloon").addClass("active");
                $(".arrow").addClass("active");
            });

            setTimeout(function () {
                $(".btn_next.scene7.btn").removeClass("active");
                $('body').pagecontainer('change', 'scene8.html', {transition: 'slidedown'});
            }, 5000);
        }

        if(d.toPage[0].id == 'scene8') {
            sound[9].play();
            $('body').pagecontainer('load', 'index.html');
            $('body').pagecontainer('load', 'scene9.html');
            $('.btn_more').on('click', function(){
                $(".btn_next.scene8.btn").removeClass("active");
                $('body').pagecontainer('change', 'index.html', {transition: 'slide'});
            });
            $('.btn_go').on('click', function(){
                $(".btn_next.scene8.btn").removeClass("active");
                $('body').pagecontainer('change', 'scene9.html', {transition: 'fade'});
            });

        }

        if(d.toPage[0].id == 'scene9') {
            $('body').pagecontainer('load', 'index.html');
            $('.btn_more').on('click', function(){
                $(".btn_next.scene9.btn").removeClass("active");
                $('body').pagecontainer('change', 'index.html', {transition: 'slide'});
            });
            $(".word-list a").on("touchstart", function(){
                $(this).addClass("on");
            });
            $(".word-list a").on("touchend", function(){
                $(this).removeClass("on");
            });

        }

        // common

        //button
        $(".page").on("touchstart",".btn, .button_list a", function(){
            $(this).addClass("on");
        });
        $(".page").on("touchend",".btn, .button_list a", function(){
            $(this).removeClass("on");
        });

        $(".riceballs").on("click", function(){
            sound[4].play(); //play sound
        });
        $(".tomatoes").on("click", function(){
            sound[7].play(); //play sound
        });
        $(".sandwiches").on("click", function(){
            sound[5].play(); //play sound
        });
        $(".chicken").on("click", function(){
            sound[3].play(); //play sound
        });

    });
})(jQuery);
