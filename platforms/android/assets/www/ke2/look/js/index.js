/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var sound_m= [];
var sound_b= [];
var sound_index;
var sound_mom;
var sound_baby;
var sound_poku;
var sound_howmany;

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        // 画面を横にロック
        screen.lockOrientation('portrait');
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        if($("body.index").length){
            progress.use('look');
        }
        /*
         * sound set
         */
        var path;
        if(device.platform == 'iOS'){
            path = "ke2/look/sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke2/look/sound/";
        }

        var mediaSuccess = function(){
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if(status != Media.MEDIA_NONE){
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if(status == Media.MEDIA_STARTING){
                // 再生が開始されたタイミング
                console.log("media: starting");
            }
            if(status == Media.MEDIA_RUNNING){
                // 再生が開始されたタイミング
                console.log("media: running");
            }
            if(status == Media.MEDIA_PAUSED){
                // 再生が開始されたタイミング
                console.log("media: paused");
            }
            else if(status == Media.MEDIA_STOPPED){
                // 停止された時の処理
                console.log("media: stopped");
            }
            console.log('==============================================');
        }
        for(i = 1; i < 15; i++){
            sound_m[i] = new Media(path + "m_" + i + ".mp3", mediaSuccess, mediaError, mediaStatus);
        }
        for(i = 1; i < 13; i++){
            sound_b[i] = new Media(path + "b_" + i + ".mp3", mediaSuccess, mediaError, mediaStatus);
        }

        sound_index = new Media(path + "helloimpillbug.mp4", mediaSuccess, mediaError, mediaStatus);

        sound_howmany = new Media(path + "howmenylegs.mp4", mediaSuccess, mediaError, mediaStatus);

        sound_mom = new Media(path + "momslegs.mp4", mediaSuccess, mediaError, mediaStatus);

        sound_baby = new Media(path + "babyslegs.mp4", mediaSuccess, mediaError, mediaStatus);

        sound_poke = new Media(path + "pokepoke.mp4", mediaSuccess, mediaError, mediaStatus);

        if($("body").hasClass("index")){
            sound_index.play();
        }

        if($("body").hasClass("poke")){
            sound_poke.play();
        }

        if($("body").hasClass("count")){
            sound_howmany.play();
        }

        $(".btn_mom").on("click", function(){
            sound_mom.play();
        });
        $(".btn_baby").on("click", function(){
            sound_baby.play();
        });

        // var w = $(".photo_view img").width();
        // var h = $(".photo_view img").height();
        //
        // var zoom = d3.behavior.zoom()
        //     .scale(1)
        //     .scaleExtent([1, 10])
        //     .on("zoom", zoomed);
        //
        // var svg = d3.select(".photo_view")
        //     .call(zoom);
        //
        // function zoomed() {
        //     var t = "translate(" + d3.event.translate[0] + 'px,' + d3.event.translate[1] +"px) " +
        //         "scale(" + d3.event.scale + ',' + d3.event.scale + ")";
        //     d3.select(".photo_view img")
        //         .style("transform-origin", "0 0")
        //         .style("transform", t);
        // }

        console.log('Received Event: ' + id);

    }
};

app.initialize();