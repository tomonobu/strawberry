(function($){

    /**
     * クリックアクション
     */

    $(".btn").on({
        'touchstart' : function(){
            $(this).addClass('on');
        },
        'touchend' : function(){
            $(this).removeClass('on');
        }
    });


    /**
     * zoo関連
     */
    var wrapper_w = $(".photo_view").width();
    var wrapper_h = $(".photo_view").height();
    var drag = d3.behavior.drag() ;

// ズーム操作に対応するイベントリスナーを指定する
    drag.on( "drag", function(d,i) {

        var img_w = $(".photo_view img").width();
        var img_h = $(".photo_view img").height();

        console.log(wrapper_h);

        // イベントオブジェクト
        d.x += d3.event.dx;
        d.y += d3.event.dy;


        if(d.x > 0){
            d.x = 0;
        }else if(d.x < (wrapper_w - img_w)){
            d.x = wrapper_w - img_w;
        }

        if(d.y > 0){
            d.y = 0;
        }else if(d.y < (wrapper_h - img_h)){
            d.y = wrapper_h - img_h;
        }

        style = 'translate(' + d.x + 'px,' + d.y + 'px  ) scale('+ d.scale +')';
        d3.select(this).style("transform", style);
    });


// div要素に対して、call()メソッドでズーム操作を適用
    var target = d3.select( ".photo_view" );
    target.select('img')
        .data([{"x":0, "y":0, "r":1, "scale":1}])
        .style("transform-origin", "0 0")
        .style("transform","translate(0,0)")
        .call(drag);


    //拡大
    //DOM要素に紐付いているdataのスケールに1.2を乗算して大きくしています。
    $(".zoom.in").on("click",function(d){
        //console.log(target.data("scale"));
        var img_w = $(".photo_view img").width();
        var img_h = $(".photo_view img").height();
        target.select("img").style("transform", function(d,i){
            d.scale = d.scale * 1.2;
            var diff_x = (img_w - (img_w * 1.2)) / 2;
            d.x = d.x + diff_x;
            var diff_y = (img_h - (img_h * 1.2)) / 2;
            d.y = d.y + diff_y;
            return 'translate(' + d.x + 'px,' + d.y + 'px  ) scale('+ d.scale +')';
        });
    });

    //縮小
    //DOM要素に紐付いているdataのスケールに1.2を乗算して大きくしています。
    $(".zoom.out").bind("click",function(d){
        var img_w = $(".photo_view img").width();
        var img_h = $(".photo_view img").height();
        target.select("img").style("transform", function(d,i){
            d.scale = d.scale * 0.8;
            if(d.scale < 1){
                d.scale = 1;
                d.x = 0;
                d.y = 0;
            }else{
                var diff_x = (wrapper_w - (img_w * 0.8)) / 2;
                d.x = diff_x;
                var diff_y = (wrapper_h - (img_h * 0.8)) / 2;
                d.y = diff_y;
            }


            return 'translate(' + d.x + 'px,' + d.y + 'px  ) scale('+ d.scale +')';
        });
    });


    /*
    *  poke
    */

    $(".wrapper_photo .photo").clickSpark({
        particleImagePath: '../../img/particle-2.png',
        particleCount: 10,
        particleSpeed: 5,
        particleSize: 20,
        particleDuration: 10,
        particleRotationSpeed: 10
    });

    $(".wrapper_photo .photo").on('click', function(){
        if(!$(this).hasClass('active')){
            $(this).addClass('active');
            $(".wrapper_photo .photo img").each(function(i, elem){
                var _self = this;
                var _i = i;
                if(i < $(".wrapper_photo .photo img").length) {
                    setTimeout(function () {
                        $(_self).addClass("on");
                        if(_i == 3){
                            $(".btn_more").addClass("active");
                        }
                    }, 1000 * i);
                }
            });
        }


    });
    $(".btn_more").on('click', function(){
        $(".wrapper_photo .photo").removeClass('active');
        $(".wrapper_photo .photo img").removeClass("on");
        $(".wrapper_photo .photo img").eq(0).addClass('on');
        $(this).removeClass("active");
    });



    // leg's count
    //mom's legs
    var timer_id = [];
    $(".btn_mom").on('click', function(){
        $(".mom ul li").removeClass('on');
        sound_howmany.stop();
        sound_howmany.release();
        for(i = 0; i < timer_id.length; i++){
            clearTimeout(timer_id[i]);
        }
        setTimeout(function(){
            $('.title').addClass('on');
            $('.legs_img .mom').removeClass('off').addClass('on');
            $('.legs_img .baby').removeClass('on').addClass('off');
            $(".baby ul li").removeClass('on');
            $(".mom ul li").each(function(i, elem){
                var _self = this;
                var _i = i;
                timer_id[i] = setTimeout(function(){
                    $(_self).addClass('on');
                    sound_m[_i+1].play();
                }, 2000 * _i);
            });
        }, 2000);
    });

    $(".btn_baby").on('click', function(){
        $(".baby ul li").removeClass('on');
        sound_howmany.stop();
        sound_howmany.release();
        for(i = 0; i < timer_id.length; i++){
            clearTimeout(timer_id[i]);
        }
        setTimeout(function(){
            $('.title').addClass('on');
            $('.legs_img .mom').removeClass('on').addClass('off');
            $('.legs_img .baby').removeClass('off').addClass('on');
            $(".mom ul li").removeClass('on');
            $(".baby ul li").each(function(i, elem){
                var _self = this;
                var _i = i;
                timer_id[i] = setTimeout(function(){
                    $(_self).addClass('on');
                    sound_b[_i+1].play();
                }, 2000 * _i);
            });
        }, 2000);
    });




})(jQuery);