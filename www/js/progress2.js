var progress = {
    db : null,
    use: function(section){
        var _self = this;
        var _section = section;
        console.log(section);
        //プラグインテスト
        sqlitePlugin.selfTest(function () {

            _self.db = sqlitePlugin.openDatabase({name: 'kse.db', location: 'default'});
            _self.db.executeSql("CREATE TABLE IF NOT EXISTS progress2 (id integer primary key autoincrement, section text)", [], function(res) {
                // テーブル作成成功
                // データ取得のSQL実行
                _self.db.executeSql("SELECT * FROM progress2 WHERE section = (?)", [_section], function(rs) {

                    if(!rs.rows.length){
                        _self.db.executeSql("INSERT INTO progress2 (section) values (?)", [_section], function(res) {
                            // SQL成功。表示処理に
                            console.log("used : " + _section);
                            console.log(res);
                        }, function(error) {
                            // SQL失敗
                            alert(error.message);
                        });
                    }

                }, function(error) {
                    // データ取得失敗
                    alert(error.message);
                })
            }, function(error) {
                // テーブル作成失敗
                alert(error.message);
            });

        }, function (error) {
            // プラグインテスト失敗
            console.log("失敗");
        });
    }
}
