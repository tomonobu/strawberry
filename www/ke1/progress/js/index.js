/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var message = [];
var rating_img = [];
var sound = [];

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // 画面を横にロック
        screen.lockOrientation('portrait');

        /*
         * sound set
         */
        var path;
        if(device.platform == 'iOS'){
            path = "sound/rating/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/sound/rating/";
        }

        var mediaSuccess = function(){
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if(status != Media.MEDIA_NONE){
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if(status == Media.MEDIA_STARTING){
                // 再生が開始されたタイミング
                console.log("media: starting");
            }
            if(status == Media.MEDIA_RUNNING){
                // 再生が開始されたタイミング
                console.log("media: running");
            }
            if(status == Media.MEDIA_PAUSED){
                // 再生が開始されたタイミング
                console.log("media: paused");
            }
            else if(status == Media.MEDIA_STOPPED){
                // 停止された時の処理
                console.log("media: stopped");
            }
            console.log('==============================================');
        }



        message[1] = "good";
        message[2] = "nice";
        message[3] = "great";
        message[4] = "wonderful";
        message[5] = "fabulous";
        message[6] = "perfect";

        rating_img[0] = "img/rate_tryagain.png";
        rating_img[1] = "img/rate_good.png";
        rating_img[2] = "img/rate_nice.png";
        rating_img[3] = "img/rate_great.png";
        rating_img[4] = "img/rate_wonderful.png";
        rating_img[5] = "img/rate_fabulous.png";
        rating_img[6] = "img/rate_perfect.png";


        sound[1] = new Media(path + "good.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[2] = new Media(path + "nice.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[3] = new Media(path + "great.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[4] = new Media(path + "wonderful.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[5] = new Media(path + "fabulous.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[6] = new Media(path + "perfect.mp3", mediaSuccess, mediaError, mediaStatus);

        sqlitePlugin.selfTest(function () {
            console.log("作成前");
            var db = sqlitePlugin.openDatabase({name: 'kse.db', location: 'default'});
            db.executeSql("CREATE TABLE IF NOT EXISTS progress1 (id integer primary key autoincrement, section text)", [], function(res) {
                console.log(res);
                // テーブル作成成功
                // データ取得のSQL実行
                db.executeSql("SELECT * FROM progress1", [], function(rs) {
                    console.log(rs.rows);

                    console.log(rs.rows.length);
                    //活用度のページの表示img/

                    for(i = 0; i < rs.rows.length; i++){
                        var row = rs.rows.item(i);
                        console.log(row);
                        var class_name = "." + row.section;
                        $(class_name, ".below").addClass("used");
                    }
                    $(".plant .step_img").removeClass("active");
                    $(".plant .step_img").eq(i).addClass("active");

                    $(".message img").attr("src", rating_img[i]);

                    sound[i].play();

                }, function(error) {
                    // データ取得失敗
                    alert(error.message);
                })
            }, function(error) {
                // テーブル作成失敗
                alert(error.message);
            });

        }, function (error) {
            // プラグインテスト失敗
            console.log("失敗");
        });
    }
};

app.initialize();

