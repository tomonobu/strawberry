/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var sound_index, sound_grow, sound_fly;
var grow = [];
var grow_1_running = true;

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        // 画面を横にロック
        screen.lockOrientation('portrait');
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        if($("body.index").length){
            progress.use('look');
        }


        /**
         * sound set
         */

        var path;
        if(device.platform == 'iOS'){
            path = "ke1/look/sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke1/look/sound/";
        }

        var mediaSuccess = function(){
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if(status != Media.MEDIA_NONE){
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if(status == Media.MEDIA_STARTING){
                // 再生が開始されたタイミング
                console.log("media: starting");
            }
            if(status == Media.MEDIA_RUNNING){
                // 再生が開始されたタイミング
                console.log("media: running");
            }
            if(status == Media.MEDIA_PAUSED){
                // 再生が開始されたタイミング
                console.log("media: paused");
            }
            else if(status == Media.MEDIA_STOPPED){
                // 停止された時の処理
                console.log("media: stopped");
            }
            console.log('==============================================');
        }


        var mediaStatus_grow = function(status){
            if(status == Media.MEDIA_STOPPED){
                if(grow_1_running){
                    grow[1].play();
                    grow_1_running = false;
                }
            }
        }

        sound_index = new Media(path + "findmeafield.mp3", mediaSuccess, mediaError, mediaStatus);
        sound_grow = new Media(path + "howdoyougrow.mp3", mediaSuccess, mediaError, mediaStatus_grow);

        grow[1] = new Media(path + "grow1.mp3", mediaSuccess, mediaError, mediaStatus);
        grow[2] = new Media(path + "grow2.mp3", mediaSuccess, mediaError, mediaStatus);
        grow[3] = new Media(path + "grow3.mp3", mediaSuccess, mediaError, mediaStatus);
        grow[4] = new Media(path + "grow4.mp3", mediaSuccess, mediaError, mediaStatus);

        if($("body").hasClass("index")){
            sound_index.play();
        }

        if($("body").hasClass("grow")){
            sound_grow.play();
        }

        sound_fly = new Media(path + "howdoyoufly.mp3", mediaSuccess, mediaError, mediaStatus);

        (function(){
            /**
             * fly
             */
            //button
            $(".fly").on("touchstart",".btn", function(){
                $(this).addClass("on");
            });
            $(".fly").on("touchend",".btn", function(){
                $(this).removeClass("on");

                sound_fly.play();
                if($(this).hasClass('view')){
                    $(this).remove();
                }else{
                    $(this).removeClass("active");
                }
            });
        })(jQuery);
    }
};

app.initialize();
