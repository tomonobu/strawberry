(function($){

    /**
     * クリックアクション
     */

    $(".btn").on({
        'touchstart' : function(){
            $(this).addClass('on');
        },
        'touchend' : function(){
            $(this).removeClass('on');
        }
    });

    /**
     * zoo関連
     */
    var wrapper_w = $(".photo_view").width();
    var wrapper_h = $(".photo_view").height();
    var drag = d3.behavior.drag() ;

// ズーム操作に対応するイベントリスナーを指定する
    drag.on( "drag", function(d,i) {

        var img_w = $(".photo_view img").width();
        var img_h = $(".photo_view img").height();

        console.log(wrapper_h);

        // イベントオブジェクト
        d.x += d3.event.dx;
        d.y += d3.event.dy;

        
        if(d.x > 0){
            d.x = 0;
        }else if(d.x < (wrapper_w - img_w)){
            d.x = wrapper_w - img_w;
        }

        if(d.y > 0){
            d.y = 0;
        }else if(d.y < (wrapper_h - img_h)){
            d.y = wrapper_h - img_h;
        }

        style = 'translate(' + d.x + 'px,' + d.y + 'px  ) scale('+ d.scale +')';
        d3.select(this).style("transform", style);
    });


// div要素に対して、call()メソッドでズーム操作を適用
    var target = d3.select( ".photo_view" );
    target.select('img')
        .data([{"x":0, "y":0, "r":1, "scale":1}])
        .style("transform-origin", "0 0")
        .style("transform","translate(0,0)")
        .call(drag);


    //拡大
    //DOM要素に紐付いているdataのスケールに1.2を乗算して大きくしています。
    $(".zoom.in").on("click",function(d){
        //console.log(target.data("scale"));
        var img_w = $(".photo_view img").width();
        var img_h = $(".photo_view img").height();
        target.select("img").style("transform", function(d,i){
            d.scale = d.scale * 1.2;
            var diff_x = (img_w - (img_w * 1.2)) / 2;
            d.x = d.x + diff_x;
            var diff_y = (img_h - (img_h * 1.2)) / 2;
            d.y = d.y + diff_y;
            return 'translate(' + d.x + 'px,' + d.y + 'px  ) scale('+ d.scale +')';
        });
    });

    //縮小
    //DOM要素に紐付いているdataのスケールに1.2を乗算して大きくしています。
    $(".zoom.out").bind("click",function(d){
        var img_w = $(".photo_view img").width();
        var img_h = $(".photo_view img").height();
        target.select("img").style("transform", function(d,i){
            d.scale = d.scale * 0.8;
            if(d.scale < 1){
                d.scale = 1;
                d.x = 0;
                d.y = 0;
            }else{
                var diff_x = (wrapper_w - (img_w * 0.8)) / 2;
                d.x = diff_x;
                var diff_y = (wrapper_h - (img_h * 0.8)) / 2;
                d.y = diff_y;
            }


            return 'translate(' + d.x + 'px,' + d.y + 'px  ) scale('+ d.scale +')';
        });
    });

    /**
     * 2ページ目
     */

    var select = $( ".slider" );
    if(select.length){
        var slider = $( "<div id='slider'></div>" ).appendTo( select ).slider({
            min: 1,
            max: 4,
            range: "min",
            //value: select[ 0 ].selectedIndex + 1,
            slide: function( event, ui ) {
                console.log(ui.value);
                grow_1_running = false;
                sound_grow.stop();
                sound_grow.release();
                for(i=0; i<4;i++){
                    grow[i+1].stop();
                    grow[i+1].release();
                }
                grow[ui.value].play();
                $(".step_photo ul li").removeClass("active");
                $(".step_photo ul li").eq(ui.value - 1).addClass("active");
            }
        });
    }

})(jQuery);