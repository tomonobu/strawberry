var _nextpage = "";

/**
 * 計算等
 */
var card_func = function () {
    //コンストラクタ
    var _self = this;
    var _total = 6;
    var _score = _total;

    this.fail = function(){
        _score--;
    };

    this.total = function(){
        return _total;
    };
    this.result = function(){
        return _score;
    }

};

var card = new card_func();
card.total();


(function($){

    $(document).on('pagecontainerchange', function (e, d) {
        //スワイプアクション

        var total = card.total();
        $(".denominator").text(total); //ページ右上の分母を出力

        if (d.toPage[0].id == 'card01' || d.toPage[0].id == 'card01_answer') {
            _nextpage = '#card02';
        }
        if (d.toPage[0].id == 'card02' || d.toPage[0].id == 'card02_answer') {
            _nextpage = '#card03';
        }
        if (d.toPage[0].id == 'card03' || d.toPage[0].id == 'card03_answer') {
            _nextpage = '#card04';
        }
        if (d.toPage[0].id == 'card04' || d.toPage[0].id == 'card04_answer') {
            _nextpage = '#card05';
        }
        if (d.toPage[0].id == 'card05' || d.toPage[0].id == 'card05_answer') {
            _nextpage = '#card06';
        }
        if (d.toPage[0].id == 'card06' || d.toPage[0].id == 'card06_answer') {
            _nextpage = 'result.html';
            $('body').pagecontainer('load', 'result.html');
        }

        //答えの表示
        if (d.toPage[0].id == 'card01_answer') {
            card.fail(); // fail
            sound[1].play();
        }
        if (d.toPage[0].id == 'card02_answer') {
            card.fail(); // fail
            sound[2].play();
        }
        if (d.toPage[0].id == 'card03_answer') {
            card.fail(); // fail
            sound[3].play();
        }
        if (d.toPage[0].id == 'card04_answer') {
            card.fail(); // fail
            sound[4].play();
        }
        if (d.toPage[0].id == 'card05_answer') {
            card.fail(); // fail
            sound[5].play();
        }
        if  (d.toPage[0].id == 'card06_answer') {
            card.fail(); // fail
            sound[6].play();
        }



        // if (d.toPage[0].id == 'result'){
        //     var score = card.result();
        //     var total = card.total();
        //     $('#result .result_score').text(score);
        //     $('#result .result_total').text("/" + total);
        //     if(score == total){
        //         $(".message").html("<img src='img/result_perfect.png'>");
        //         sound[7].play();
        //     }
        //     if(score == 5){
        //         $(".message").html("<img src='img/result_fabulous.png'>");
        //         sound[8].play();
        //     }
        //     if(score == 4){
        //         $(".message").html("<img src='img/result_wonderful.png'>");
        //         sound[9].play();
        //     }
        //     if(score == 3){
        //         $(".message").html("<img src='img/result_great.png'>");
        //         sound[10].play();
        //     }
        //     if(score == 2){
        //         $(".message").html("<img src='img/result_nice.png'>");
        //         sound[11].play();
        //     }
        //     if(score == 1){
        //         $(".message").html("<img src='img/result_good.png'>");
        //         sound[12].play();
        //     }
        //     if(score == 0){
        //         $(".message").html("<img src='img/result_tryagain.png'>");
        //     }
        // }
    });

    $(".card, .answer").on('swipeleft', function(){
        $('body').pagecontainer('change', _nextpage, {transition: 'slide'});
    });

})(jQuery);
