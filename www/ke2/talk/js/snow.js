/* Information
----------------------------------------------
File Name : snow.js
URL : http://www.atokala.com/
Copyright : (C)atokala
Author : Masahiro Abe
--------------------------------------------*/
var ATSnow = function(vars) {
	//コンストラクタ
	var _self = this;
	var _snows = [];
	var _scrollHeight = 0;
	var _windowWidth = 0;
	var _windowHeight = 0;
	var _doc = document;
	var _timerID;

	//デフォルトオプション
	var options = {
		classname : 'snow',
		count : 100,
		interval : 40,
		maxSize : 5,
		minSize : 1,
		leftMargin : 50,
		rightMargin : 50,
		bottomMargin : 30,
		maxDistance : 10,
		minDistance : 1
	};

	//オプションの上書き設定
	this.config = function(property) {
		for (var i in property) {
			//設定されていない時は上書きしない
			if (!vars.hasOwnProperty(i)) {
				continue;
			}
			options[i] = property[i];
		}
	}

	//イベント追加
	this.addEvent = function(eventTarget, eventName, func) {
		// モダンブラウザ
		if(eventTarget.addEventListener) {
			eventTarget.addEventListener(eventName, func, false);
		}
	}


	//スクロール幅取得
	this.getScrollWidth = function() {
		return _doc.documentElement.scrollWidth;
	}

	//スクロール高さ取得
	this.getScrollHeight = function() {
		return _doc.documentElement.scrollHeight;
	}

	//ランダム取得
	this.getRandomInt = function(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	//ウインドウ幅取得
	this.getWindowWidth = function() {
		return window.innerWidth;
	}

	//ウインドウ高さ取得
	this.getWindowHeight = function() {
		//モダン
		if (window.innerHeight) {
			return window.innerHeight;
		}
	}

	this.createSnow = function() {
		var body = _doc.getElementsByTagName('body')[0];

		for(var i = 0; i < options.count; i++){
			//雪作成
			var snow = _doc.createElement('div');
			snow.className = options.classname;
			var diameter = _self.getRandomInt(options.minSize, options.maxSize);
			snow.style.width = diameter + 'px';
			snow.style.height = diameter + 'px';

			_snows[i] = {};
			var snows = _snows[i];
			snows.ele = snow;
			snows.distance = _self.getRandomInt(options.minDistance, options.maxDistance);
			snows.degree = _self.getRandomInt(1, 10);
			snows.move = 0;
			snows.size = diameter;

			body.appendChild(snow);
		}
	}

	this.snowsPositionReset = function() {
		for(var i = 0; i < options.count; i++){
			var topPosition = Math.floor(Math.random() * _scrollHeight);
			_self.snowPositionReset(_snows[i], topPosition - _scrollHeight);
		}
	}

	this.snowPositionReset = function(snow, y) {
		var leftPosition = _self.getRandomInt(options.leftMargin, _windowWidth - options.rightMargin);
		snow.ele.style.top = y + 'px';
		snow.ele.style.left = leftPosition + 'px';
		snow.x = leftPosition;
	}

	this.move = function() {
		var fall = function() {
			for(var i = 0; i < options.count; i++){
				var snow = _snows[i];
				var top = parseInt(snow.ele.style.top);
				snow.move += snow.degree;

				if (top + snow.size + snow.size >= _scrollHeight - options.bottomMargin) {
					_self.snowPositionReset(snow, 0);
				}
				else {
					snow.ele.style.top = top + snow.size + 'px';
					snow.ele.style.left = snow.x + Math.cos(snow.move * Math.PI / 180) * snow.distance + 'px';
				}
			}
            _timerID = setTimeout(function(){fall()}, options.interval);
		}
		fall();
	}

	this.clear = function(){
		clearTimeout(_timerID);
		$('.sakura').remove();
	}

	this.load = function() {

		//コンストラクタ
		_self.config(vars);
		_scrollHeight = _self.getScrollHeight();
		_windowWidth = _self.getWindowWidth();
		_windowHeight = _self.getWindowHeight();
		_self.createSnow();
		_self.snowsPositionReset();
		_self.move();

	}
};
