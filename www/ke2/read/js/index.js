
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {

        console.log(id);

        // 画面を横にロック
        screen.lockOrientation('portrait');
        progress.use('read');
        /*
        * sound set
        */
        var path;
        if(device.platform == 'iOS'){
            path = "ke2/read/sounds/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke2/read/sounds/";
        }
        console.log(path);

        var mediaSuccess = function(){
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if(status != Media.MEDIA_NONE){
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if(status == Media.MEDIA_STARTING){
                // 再生が開始されたタイミング
                // (省略)
                console.log("media: starting");
            }
            if(status == Media.MEDIA_RUNNING){
                // 再生が開始されたタイミング
                // (省略)
                console.log("media: running");
            }
            if(status == Media.MEDIA_PAUSED){
                // 再生が開始されたタイミング
                // (省略)
                console.log("media: paused");
            }
            else if(status == Media.MEDIA_STOPPED){
                // 停止された時の処理
                console.log("media: stopped");
            }
            console.log('==============================================');
        }


        audio[1] = new Media(path + "_01.mp3", mediaSuccess, mediaError, mediaStatus);
        audio[2] = new Media(path + "_02.mp3", mediaSuccess, mediaError, mediaStatus);
        audio[3] = new Media(path + "_03.mp3", mediaSuccess, mediaError, mediaStatus);
        audio[4] = new Media(path + "_04.mp3", mediaSuccess, mediaError, mediaStatus);
        audio[5] = new Media(path + "_05.mp3", mediaSuccess, mediaError, mediaStatus);
        audio[6] = new Media(path + "_06.mp3", mediaSuccess, mediaError, mediaStatus);
        audio[7] = new Media(path + "_07.mp3", mediaSuccess, mediaError, mediaStatus);


        (function($){
            var pager = "";
            for(i = 2; i < animeArray.length; i++){
                pager += '<li><a href="#"></a></li>';
            }
            $(".chap").html(pager);
        })(jQuery);

        init();

    }

};

app.initialize();