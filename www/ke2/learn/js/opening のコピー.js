(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"opening_atlas_", frames: [[0,0,1107,508],[0,986,1083,407],[0,510,1021,474],[1109,0,894,457],[1085,968,930,413],[1085,1383,842,424],[0,2219,709,424],[797,1809,711,433],[1023,510,969,456],[0,1804,795,413],[0,1395,990,407],[711,2244,1334,92]]}
];


// symbols:



(lib.nekozakana_1 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_10 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_11 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_2 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_3 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_4 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_5 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_6 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_7 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_8 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_9 = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.nekozakana_base = function() {
	this.spriteSheet = ss["opening_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.トゥイーン3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgldAFwQgRgSAAgZQAAgZARgSQASgRAZAAQAZAAASARQARASAAAZQAAAZgRASQgSARgZAAQgZAAgSgRgEhlOAE2QgagZAAgjQAAgjAagZQAZgZAjAAQAiAAAZAZQAaAZgBAjQABAjgaAZQgZAZgiAAQgjAAgZgZgEBizAEdQgdgdAAgpQAAgqAdgdQAegdApAAQAqAAAdAdQAdAdAAAqQAAApgdAdQgdAegqAAQgpAAgegegEglmAC6QgIgJAAgMQAAgNAIgIQAJgJANAAQAMAAAJAJQAJAIAAANQAAAMgJAJQgJAJgMAAQgNAAgJgJgEBksgAXQgUgTABgbQgBgbAUgTQASgTAcAAQAbAAASATQATATAAAbQAAAbgTATQgSATgbAAQgcAAgSgTgAkBiXQgRgSAAgZQAAgZARgSQASgRAZAAQAZAAASARQARASAAAZQAAAZgRASQgSARgZAAQgZAAgSgRgEgijgDPQgNgNAAgTQAAgTANgNQAOgNASAAQASAAANANQAOANAAATQAAATgOANQgNANgSAAQgSAAgOgNgEgoogDaQgYgXAAghQAAghAYgXQAXgYAhAAQAhAAAXAYQAYAXAAAhQAAAhgYAXQgXAYghAAQghAAgXgYgEBkcgECQgOgNAAgSQAAgSAOgOQANgNASAAQATAAANANQAOAOgBASQABASgOANQgNAOgTAAQgSAAgNgOgEhmLgErQgOgPAAgUQAAgVAOgOQAPgPAUAAQAVAAAOAPQAQAOAAAVQAAAUgQAPQgOAPgVAAQgUAAgPgPg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-655.4,-38.5,1310.9,77);


(lib.トゥイーン2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2E7DDD").s().p("Aj/EzQgdgHgMgYQgGgMAAgQQAAgQAHgTQATgzCflDIAihTIAVgjQANgQAMgGQAfgMAmAHQATADAOAIQAQAJAHAMQAVAjC7HIQAMAZgIAXQgIAVgVAKQgXAKgbgJQgOgEgMgKQgNgKgIgQQgNgXgphmIjjABIgIATQgtBegQAWQgUAcgPAIQgKAGgOAAQgJAAgLgDgAgwADIBygBQgUgrgjhaIg7CGg");
	this.shape.setTransform(589,-3.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2E7DDD").s().p("ACTEeQgOgHgpguIhShkQhAhShNhlIABEdQgBAZgVARQgSAPgZABQgZACgRgRQgVgSgCgiQgEgwgCmLQADgfARgTQARgUAegEIALgBQATAAATAIQAZALAXAYIDvFDIAAkwQAAgdAVgTQATgSAYAAIACAAQAYABATARQAUAUABAaQgDGugCAlQgCAYgUASQgWAUgeAAQgVAAgUgLg");
	this.shape_1.setTransform(523.4,-3.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#2E7DDD").s().p("Aj/EzQgdgHgMgYQgGgMAAgQQAAgQAHgTQATgzCflDIAihTIAVgjQANgQAMgGQAfgMAmAHQATADAOAIQAQAJAHAMQAVAjC7HIQAMAZgIAXQgIAVgVAKQgXAKgbgJQgOgEgMgKQgNgKgIgQQgNgXgphmIjjABIgIATQgtBegQAWQgUAcgPAIQgKAGgOAAQgJAAgLgDgAgwADIBygBQgUgrgjhaIg7CGg");
	this.shape_2.setTransform(457.6,-3.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#2E7DDD").s().p("AirEvQgRAAgNgIQgLgHgJgNQgPgYgCgjIgEjjQAAhnACh8QADgdAWgSQAUgRAZAAQAcAAAUAVQATAaAAAXIAACIIB6hfIAUgQIAIgGQAwgoATgMQAfgUAVAAQAYAAATATQATASAAAXQAAATgQAXQgQAXgdAXQgbAVg9A4IhEA/IDQC0QARARAFAZQAFAZgJARQgIAOgRAIQgRAJgTAAQgWAAgRgLQgYgNgzgpIhFg5QgXgVg8grIgMgJIABB4QgBAngMASQgIAKgMAGQgNAFgTACg");
	this.shape_3.setTransform(398.4,-3.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#2E7DDD").s().p("Aj/EzQgdgHgMgYQgGgMAAgQQAAgQAHgTQATgzCflDIAihTIAVgjQANgQAMgGQAfgMAmAHQATADAOAIQAQAJAHAMQAVAjC7HIQAMAZgIAXQgIAVgVAKQgXAKgbgJQgOgEgMgKQgNgKgIgQQgNgXgphmIjjABIgIATQgtBegQAWQgUAcgPAIQgKAGgOAAQgJAAgLgDgAgwADIBygBQgUgrgjhaIg7CGg");
	this.shape_4.setTransform(335,-3.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#2E7DDD").s().p("AiWEkQghgBgVgUQgLgKgGgNQgFgOAAgSQAAgtBIhYQAig3CKi5IACgCIAAAAIAAAAIgCACQgJAEg8gBIhfgDQghAAgYgQQgZgRgBgcQgBgfAUgTQALgKAQgGQAQgFAVgBQBYgCCjADQAeABAVAPQAUAOAIAWQAGAUgIAbQgFAQgUAoQgRAhi/ELQAFAMDTgKQAdgBASATQAPAQACAZQACAZgNARQgQASggADIkXACIgpAAg");
	this.shape_5.setTransform(276.7,-3.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#2E7DDD").s().p("AheEZQgwgOgggYQgcgWgYgiQgZgigPgqQgRgsgEgvQgDgfAIhDQAJgnAXgnQAXgmAigeQAlggArgSIAvgOQAZgFAZAAIAGAAQAhgBA/ARQAuAPAmAcQAnAdAcAmQAgA4AJAfQANBCgCAhQgCAygRAvQgQAwgcAkQgqAtgZANQghASgmAJQgoAJgrAAQgzAAgwgNgAg2iMQgaAJgSAPQgQANgLASQgKARgGAVQgFAVAAAXQAAAYAGAaQAFAUAMATQANASATAOQASAOAXAIQAXAJAZABQAVABAVgFQAXgGAUgKQAUgMAOgQQAQgRAGgWIAIgnQADgWgDgVQgDgXgJgWQgLgWgTgUQgSgUgcgLQgcgLggAAQgcAAgZAIg");
	this.shape_6.setTransform(218.2,-3.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#2E7DDD").s().p("AirEvQgRAAgNgIQgLgHgJgNQgPgYgCgjIgEjjQAAhnACh8QADgdAWgSQAUgRAZAAQAcAAAUAVQATAaAAAXIAACIIB6hfIAUgQIAIgGQAwgoATgMQAfgUAVAAQAYAAATATQATASAAAXQAAATgQAXQgQAXgdAXQgbAVg9A4IhEA/IDQC0QARARAFAZQAFAZgJARQgIAOgRAIQgRAJgTAAQgWAAgRgLQgYgNgzgpIhFg5QgXgVg8grIgMgJIABB4QgBAngMASQgIAKgMAGQgNAFgTACg");
	this.shape_7.setTransform(158.6,-3.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#2E7DDD").s().p("Ai4EbQgMgIgGgNQgIgOAAgUQAAjzABhyIABhWQAAgoASgTQASgUAlgBIAtAAID1ADQAcAAAPAWQAOASAAAaQAAAcgRARQgRAQgeACIhoABIhTAAIgPAAQgOAAgDAUIgBBLIDOAFQARAAANAOQAUAWgCAbQgBAWgPARQgRASgWABIgkABIikgCIAABWQgBASAdAAIDCACQAYABAPAWQAMATAAAWQAAAVgOAUQgQAWgXABIkbAEQgfAAgRgNg");
	this.shape_8.setTransform(102.3,-2.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#2E7DDD").s().p("ACTEeQgOgHgpguIhShkQhAhShNhlIABEdQgBAZgVARQgSAPgZABQgZACgRgRQgVgSgDgiQgDgwgCmLQADgfARgTQARgUAegEIAKgBQAUAAATAIQAZALAXAYIDvFDIAAkwQAAgdAVgTQATgSAYAAIACAAQAZABASARQAUAUABAaQgDGugCAlQgCAYgUASQgWAUgeAAQgVAAgUgLg");
	this.shape_9.setTransform(41.9,-3.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#2E7DDD").s().p("AitEbQgNgQgEgmQgEgpAAmTQAEgrAGgOQAGgMALgHQAMgHATAAQATAAAPAIQANAIAJANQAPAWAAAgQABAxAGBjQAQgHAcgCQAXgBAaAEQAfAFAgAPQAWAJARAQQAQAQAMAUQALAVAHAbQAHAZADAfQAEA+gFBLQgDATgMANQgQARgbAAQgeAAgRgSQgPgPgCgWQgCgZgBhkQgCgegUgRQgSgQgaABQgYAAgSAQQgUASgBAfQABBSgDAXQgGAlgQARQgPAQgcACIgHABQgZAAgMgQg");
	this.shape_10.setTransform(-56.5,-3.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#2E7DDD").s().p("AgoDsQgNgSgEgrQgDgnABiZQgxABgfgJQgUgHgMgLQgNgMAAgUQAAgUANgRQAKgOAUgKQAZgNBAgHQABg3AMgUQAGgNAMgFQAMgHATAAQASAAAMAKQAKAIAFAQQAHATAAAwIA5ADQASACAOAJQAMAIAIALQAOAVgBAYQAAAagWARQgXARglAAIgqAAIAADDQAAAlgMAUQgOAVgeAAQgfABgNgVg");
	this.shape_11.setTransform(-102.6,1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#2E7DDD").s().p("AgGEPQgUgBgNgIQgMgIgHgNQgIgRgFgqQgEgqADiNQABgXAIgRQAHgQALgKQAUgSAcgCQAPAAAMAGQAMAGAJANQAJAMAFATQAFATAAAYQAECXgIArQgHAhgRARQgRAQgZAAIgGgBgAgdh6QgPgGgMgMQgLgMgFgQQgFgQACgSQADgWANgRQALgOATgIQAQgHAQgBQAQAAALAGQAYAMAMAUQALAUAAAaQAAAbgRATQgJALgNAFQgOAHgTACIgFAAQgPAAgOgGg");
	this.shape_12.setTransform(-136.3,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#2E7DDD").s().p("ABFDKQgIgFgKgPQgDgFgWg4QgUgygFgDQgqBsgLALIgQAPQgHAGgNAAQgTAAgNgKQgKgIgMgTQgQgcgyh8Qgzh8gEgWQgFgeAHgUQAJgVAZgHIAUgEQAbAAAVATQASAPAJAYIAtCBQAahJAIgNQAKgQAMgIQANgJASAAQATAAAOAJQALAJALARQAPAbAXBDIA0iTQAIgVARgLQARgMAXAAQAIAAAOAHQAQAFAMAZQAKAYgIAVIgMAkIg8CoQgdBOgMASQgLARgJAGQgMAIgTAAQgSAAgLgIg");
	this.shape_13.setTransform(-178.9,5.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#2E7DDD").s().p("AgoDsQgNgSgEgrQgDgnABiZQgxABgfgJQgUgHgMgLQgNgMAAgUQAAgUANgRQAKgOAUgKQAZgNBAgHQABg3AMgUQAGgNAMgFQAMgHATAAQASAAAMAKQAKAIAFAQQAHATAAAwIA5ADQASACAOAJQAMAIAIALQAOAVgBAYQAAAagWARQgXARglAAIgqAAIAADDQAAAlgMAUQgOAVgeAAQgfABgNgVg");
	this.shape_14.setTransform(-268.2,1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#2E7DDD").s().p("AgQDHQghgGgagMQgdgPgUgYQgWgagMgmQgMgqAAg1QAAgeAIgeQAKggARgaQATgcAagRQAcgTAhgDQAigEAdAEQAcAEAXALQAWAKASASQARAQAMAXQAMAWADAWQAEAWgFAUQgEAUgNAQQgMARgTAMQgqAbhmgHIAHAJQAHAJAJAGQAMAIATACQAYACAigIQAOgDAPAGQAOAFAKAMQAKAMACAPQACARgJAPQgXAcgaAJQgVAIgbABIgLABQgZAAgdgGgAgKhkQgSARgGAOQgGAQALAIQATAOAkgJQARgFAIgHQAKgJgDgLQgFgTgVgJQgKgEgKAAQgLAAgLAEg");
	this.shape_15.setTransform(-310.1,6.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#2E7DDD").s().p("AiZEeQgSgHgLgQQgMgSgCgeQgIhVAElkQgBgRAHgPQAGgNALgJQAWgSAdAEQAaACATAYQASAVAAAZIACB9QAsgCAqAGQAmAFAdANQAlAQAXAbQAXAdAOAhQAMAhABAiQACAigKAgQgJAggSAZQgRAZgfARQgaAPgkAIQgeAHgiADIg6ABQhAAAgYgKgAhBAkQgKALAAAWIACBQQABANALAHQALAHARACQAjAEAcgQQAbgOAHgeQAIgdgOgbQgVgohFAAQgXAAgKAKg");
	this.shape_16.setTransform(-355.6,-3.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#2E7DDD").s().p("ABVCaQg1AnhNgGQgfgDgbgLQgdgLgWgTQgXgUgNgbQgPgegBglQgBgdADgaQAEgeAMgaQANgdAWgWQAZgaAkgRQAjgRApgCQAkgCAmALQAkALAdATQAdAUAPAYQAVAgAKApQAIAgABAmQABAtgHA7QAAAegUARQgRAPgdAAQgkAAgOgrgAgZhQQgQADgOALQgWATgIAlQgEAOAEAQQAEAPAJANQAKAMAPAHQARAIATAAQAaAAAWgOQAWgNALgXQAGgNABgOQABgQgGgOQgGgQgOgLQgOgNgXgGQgOgEgLAAQgIAAgHACg");
	this.shape_17.setTransform(-407.7,7.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#2E7DDD").s().p("AitEbQgNgQgEgmQgEgpAAmTQAEgrAGgOQAGgMALgHQAMgHATAAQATAAAPAIQANAIAJANQAPAWAAAgQABAxAGBjQAQgHAcgCQAXgBAaAEQAfAFAgAPQAWAJARAQQAQAQAMAUQALAVAHAbQAHAZADAfQAEA+gFBLQgDATgMANQgQARgbAAQgeAAgRgSQgPgPgCgWQgCgZgBhkQgCgegUgRQgSgQgaABQgYAAgSAQQgUASgBAfQABBSgDAXQgGAlgQARQgPAQgcACIgHABQgZAAgMgQg");
	this.shape_18.setTransform(-457.9,-3.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#2E7DDD").s().p("AilEDQgJgJgFgNQgGgOABgSQABgSgFiqQgChRABh/QADgfAKgRQAKgQARgIQAVgKA+AAIA4ACQAgACAcAHQAiAJAZAPQAcARASAYQASAaAJAhQAJAggBAjQgCAjgMAhQgNAigYAdQhCBPiHgPQAFAIgBAYIgEAtQAAAZgPAWQgQAXgZACIgIABQgXAAgQgPgAgjiaQgRACgKAGQgMAIgBAMIgCBQQAAAWAKAKQALALAWAAQAhAAAPgFQAdgJAOgaQAOgagIgeQgIgegagOQgWgMgbAAIgPABg");
	this.shape_19.setTransform(-505.8,12.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#2E7DDD").s().p("AgcEcQgNgIgJgOQgPgZgBgqIgBjSQABitAEghQAEggAWgTQAUgRAYAAQAYAAASAUQATAVgBAjIgEGiQgCAugIAOQgFALgMAGQgMAHgVABIgFAAQgPAAgMgGg");
	this.shape_20.setTransform(-543.4,-2.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#2E7DDD").s().p("Aj/EzQgdgHgMgYQgGgMAAgQQAAgQAHgTQATgzCflDIAihTIAVgjQANgQAMgGQAfgMAmAHQATADAOAIQAQAJAHAMQAVAjC7HIQAMAZgIAXQgIAVgVAKQgXAKgbgJQgOgEgMgKQgNgKgIgQQgNgXgphmIjjABIgIATQgtBegQAWQgUAcgPAIQgKAGgOAAQgJAAgLgDgAgwADIBygBQgUgrgjhaIg7CGg");
	this.shape_21.setTransform(-588.9,-3.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-623.7,-42,1247.5,84);


(lib.トゥイーン1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2E7DDD").s().p("Aj/EzQgdgHgMgYQgGgMAAgQQAAgQAHgTQATgzCflDIAihTIAVgjQANgQAMgGQAfgMAmAHQATADAOAIQAQAJAHAMQAVAjC7HIQAMAZgIAXQgIAVgVAKQgXAKgbgJQgOgEgMgKQgNgKgIgQQgNgXgphmIjjABIgIATQgtBegQAWQgUAcgPAIQgKAGgOAAQgJAAgLgDgAgwADIBygBQgUgrgjhaIg7CGg");
	this.shape.setTransform(589,-3.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2E7DDD").s().p("ACTEeQgOgHgpguIhShkQhAhShNhlIABEdQgBAZgVARQgSAPgZABQgZACgRgRQgVgSgCgiQgEgwgCmLQADgfARgTQARgUAegEIALgBQATAAATAIQAZALAXAYIDvFDIAAkwQAAgdAVgTQATgSAYAAIACAAQAYABATARQAUAUABAaQgDGugCAlQgCAYgUASQgWAUgeAAQgVAAgUgLg");
	this.shape_1.setTransform(523.4,-3.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#2E7DDD").s().p("Aj/EzQgdgHgMgYQgGgMAAgQQAAgQAHgTQATgzCflDIAihTIAVgjQANgQAMgGQAfgMAmAHQATADAOAIQAQAJAHAMQAVAjC7HIQAMAZgIAXQgIAVgVAKQgXAKgbgJQgOgEgMgKQgNgKgIgQQgNgXgphmIjjABIgIATQgtBegQAWQgUAcgPAIQgKAGgOAAQgJAAgLgDgAgwADIBygBQgUgrgjhaIg7CGg");
	this.shape_2.setTransform(457.6,-3.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#2E7DDD").s().p("AirEvQgRAAgNgIQgLgHgJgNQgPgYgCgjIgEjjQAAhnACh8QADgdAWgSQAUgRAZAAQAcAAAUAVQATAaAAAXIAACIIB6hfIAUgQIAIgGQAwgoATgMQAfgUAVAAQAYAAATATQATASAAAXQAAATgQAXQgQAXgdAXQgbAVg9A4IhEA/IDQC0QARARAFAZQAFAZgJARQgIAOgRAIQgRAJgTAAQgWAAgRgLQgYgNgzgpIhFg5QgXgVg8grIgMgJIABB4QgBAngMASQgIAKgMAGQgNAFgTACg");
	this.shape_3.setTransform(398.4,-3.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#2E7DDD").s().p("Aj/EzQgdgHgMgYQgGgMAAgQQAAgQAHgTQATgzCflDIAihTIAVgjQANgQAMgGQAfgMAmAHQATADAOAIQAQAJAHAMQAVAjC7HIQAMAZgIAXQgIAVgVAKQgXAKgbgJQgOgEgMgKQgNgKgIgQQgNgXgphmIjjABIgIATQgtBegQAWQgUAcgPAIQgKAGgOAAQgJAAgLgDgAgwADIBygBQgUgrgjhaIg7CGg");
	this.shape_4.setTransform(335,-3.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#2E7DDD").s().p("AiWEkQghgBgVgUQgLgKgGgNQgFgOAAgSQAAgtBIhYQAig3CKi5IACgCIAAAAIAAAAIgCACQgJAEg8gBIhfgDQghAAgYgQQgZgRgBgcQgBgfAUgTQALgKAQgGQAQgFAVgBQBYgCCjADQAeABAVAPQAUAOAIAWQAGAUgIAbQgFAQgUAoQgRAhi/ELQAFAMDTgKQAdgBASATQAPAQACAZQACAZgNARQgQASggADIkXACIgpAAg");
	this.shape_5.setTransform(276.7,-3.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#2E7DDD").s().p("AheEZQgwgOgggYQgcgWgYgiQgZgigPgqQgRgsgEgvQgDgfAIhDQAJgnAXgnQAXgmAigeQAlggArgSIAvgOQAZgFAZAAIAGAAQAhgBA/ARQAuAPAmAcQAnAdAcAmQAgA4AJAfQANBCgCAhQgCAygRAvQgQAwgcAkQgqAtgZANQghASgmAJQgoAJgrAAQgzAAgwgNgAg2iMQgaAJgSAPQgQANgLASQgKARgGAVQgFAVAAAXQAAAYAGAaQAFAUAMATQANASATAOQASAOAXAIQAXAJAZABQAVABAVgFQAXgGAUgKQAUgMAOgQQAQgRAGgWIAIgnQADgWgDgVQgDgXgJgWQgLgWgTgUQgSgUgcgLQgcgLggAAQgcAAgZAIg");
	this.shape_6.setTransform(218.2,-3.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#2E7DDD").s().p("AirEvQgRAAgNgIQgLgHgJgNQgPgYgCgjIgEjjQAAhnACh8QADgdAWgSQAUgRAZAAQAcAAAUAVQATAaAAAXIAACIIB6hfIAUgQIAIgGQAwgoATgMQAfgUAVAAQAYAAATATQATASAAAXQAAATgQAXQgQAXgdAXQgbAVg9A4IhEA/IDQC0QARARAFAZQAFAZgJARQgIAOgRAIQgRAJgTAAQgWAAgRgLQgYgNgzgpIhFg5QgXgVg8grIgMgJIABB4QgBAngMASQgIAKgMAGQgNAFgTACg");
	this.shape_7.setTransform(158.6,-3.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#2E7DDD").s().p("Ai4EbQgMgIgGgNQgIgOAAgUQAAjzABhyIABhWQAAgoASgTQASgUAlgBIAtAAID1ADQAcAAAPAWQAOASAAAaQAAAcgRARQgRAQgeACIhoABIhTAAIgPAAQgOAAgDAUIgBBLIDOAFQARAAANAOQAUAWgCAbQgBAWgPARQgRASgWABIgkABIikgCIAABWQgBASAdAAIDCACQAYABAPAWQAMATAAAWQAAAVgOAUQgQAWgXABIkbAEQgfAAgRgNg");
	this.shape_8.setTransform(102.3,-2.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#2E7DDD").s().p("ACTEeQgOgHgpguIhShkQhAhShNhlIABEdQgBAZgVARQgSAPgZABQgZACgRgRQgVgSgDgiQgDgwgCmLQADgfARgTQARgUAegEIAKgBQAUAAATAIQAZALAXAYIDvFDIAAkwQAAgdAVgTQATgSAYAAIACAAQAZABASARQAUAUABAaQgDGugCAlQgCAYgUASQgWAUgeAAQgVAAgUgLg");
	this.shape_9.setTransform(41.9,-3.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#2E7DDD").s().p("AitEbQgNgQgEgmQgEgpAAmTQAEgrAGgOQAGgMALgHQAMgHATAAQATAAAPAIQANAIAJANQAPAWAAAgQABAxAGBjQAQgHAcgCQAXgBAaAEQAfAFAgAPQAWAJARAQQAQAQAMAUQALAVAHAbQAHAZADAfQAEA+gFBLQgDATgMANQgQARgbAAQgeAAgRgSQgPgPgCgWQgCgZgBhkQgCgegUgRQgSgQgaABQgYAAgSAQQgUASgBAfQABBSgDAXQgGAlgQARQgPAQgcACIgHABQgZAAgMgQg");
	this.shape_10.setTransform(-56.5,-3.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#2E7DDD").s().p("AgoDsQgNgSgEgrQgDgnABiZQgxABgfgJQgUgHgMgLQgNgMAAgUQAAgUANgRQAKgOAUgKQAZgNBAgHQABg3AMgUQAGgNAMgFQAMgHATAAQASAAAMAKQAKAIAFAQQAHATAAAwIA5ADQASACAOAJQAMAIAIALQAOAVgBAYQAAAagWARQgXARglAAIgqAAIAADDQAAAlgMAUQgOAVgeAAQgfABgNgVg");
	this.shape_11.setTransform(-102.6,1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#2E7DDD").s().p("AgGEPQgUgBgNgIQgMgIgHgNQgIgRgFgqQgEgqADiNQABgXAIgRQAHgQALgKQAUgSAcgCQAPAAAMAGQAMAGAJANQAJAMAFATQAFATAAAYQAECXgIArQgHAhgRARQgRAQgZAAIgGgBgAgdh6QgPgGgMgMQgLgMgFgQQgFgQACgSQADgWANgRQALgOATgIQAQgHAQgBQAQAAALAGQAYAMAMAUQALAUAAAaQAAAbgRATQgJALgNAFQgOAHgTACIgFAAQgPAAgOgGg");
	this.shape_12.setTransform(-136.3,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#2E7DDD").s().p("ABFDKQgIgFgKgPQgDgFgWg4QgUgygFgDQgqBsgLALIgQAPQgHAGgNAAQgTAAgNgKQgKgIgMgTQgQgcgyh8Qgzh8gEgWQgFgeAHgUQAJgVAZgHIAUgEQAbAAAVATQASAPAJAYIAtCBQAahJAIgNQAKgQAMgIQANgJASAAQATAAAOAJQALAJALARQAPAbAXBDIA0iTQAIgVARgLQARgMAXAAQAIAAAOAHQAQAFAMAZQAKAYgIAVIgMAkIg8CoQgdBOgMASQgLARgJAGQgMAIgTAAQgSAAgLgIg");
	this.shape_13.setTransform(-178.9,5.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#2E7DDD").s().p("AgoDsQgNgSgEgrQgDgnABiZQgxABgfgJQgUgHgMgLQgNgMAAgUQAAgUANgRQAKgOAUgKQAZgNBAgHQABg3AMgUQAGgNAMgFQAMgHATAAQASAAAMAKQAKAIAFAQQAHATAAAwIA5ADQASACAOAJQAMAIAIALQAOAVgBAYQAAAagWARQgXARglAAIgqAAIAADDQAAAlgMAUQgOAVgeAAQgfABgNgVg");
	this.shape_14.setTransform(-268.2,1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#2E7DDD").s().p("AgQDHQghgGgagMQgdgPgUgYQgWgagMgmQgMgqAAg1QAAgeAIgeQAKggARgaQATgcAagRQAcgTAhgDQAigEAdAEQAcAEAXALQAWAKASASQARAQAMAXQAMAWADAWQAEAWgFAUQgEAUgNAQQgMARgTAMQgqAbhmgHIAHAJQAHAJAJAGQAMAIATACQAYACAigIQAOgDAPAGQAOAFAKAMQAKAMACAPQACARgJAPQgXAcgaAJQgVAIgbABIgLABQgZAAgdgGgAgKhkQgSARgGAOQgGAQALAIQATAOAkgJQARgFAIgHQAKgJgDgLQgFgTgVgJQgKgEgKAAQgLAAgLAEg");
	this.shape_15.setTransform(-310.1,6.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#2E7DDD").s().p("AiZEeQgSgHgLgQQgMgSgCgeQgIhVAElkQgBgRAHgPQAGgNALgJQAWgSAdAEQAaACATAYQASAVAAAZIACB9QAsgCAqAGQAmAFAdANQAlAQAXAbQAXAdAOAhQAMAhABAiQACAigKAgQgJAggSAZQgRAZgfARQgaAPgkAIQgeAHgiADIg6ABQhAAAgYgKgAhBAkQgKALAAAWIACBQQABANALAHQALAHARACQAjAEAcgQQAbgOAHgeQAIgdgOgbQgVgohFAAQgXAAgKAKg");
	this.shape_16.setTransform(-355.6,-3.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#2E7DDD").s().p("ABVCaQg1AnhNgGQgfgDgbgLQgdgLgWgTQgXgUgNgbQgPgegBglQgBgdADgaQAEgeAMgaQANgdAWgWQAZgaAkgRQAjgRApgCQAkgCAmALQAkALAdATQAdAUAPAYQAVAgAKApQAIAgABAmQABAtgHA7QAAAegUARQgRAPgdAAQgkAAgOgrgAgZhQQgQADgOALQgWATgIAlQgEAOAEAQQAEAPAJANQAKAMAPAHQARAIATAAQAaAAAWgOQAWgNALgXQAGgNABgOQABgQgGgOQgGgQgOgLQgOgNgXgGQgOgEgLAAQgIAAgHACg");
	this.shape_17.setTransform(-407.7,7.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#2E7DDD").s().p("AitEbQgNgQgEgmQgEgpAAmTQAEgrAGgOQAGgMALgHQAMgHATAAQATAAAPAIQANAIAJANQAPAWAAAgQABAxAGBjQAQgHAcgCQAXgBAaAEQAfAFAgAPQAWAJARAQQAQAQAMAUQALAVAHAbQAHAZADAfQAEA+gFBLQgDATgMANQgQARgbAAQgeAAgRgSQgPgPgCgWQgCgZgBhkQgCgegUgRQgSgQgaABQgYAAgSAQQgUASgBAfQABBSgDAXQgGAlgQARQgPAQgcACIgHABQgZAAgMgQg");
	this.shape_18.setTransform(-457.9,-3.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#2E7DDD").s().p("AilEDQgJgJgFgNQgGgOABgSQABgSgFiqQgChRABh/QADgfAKgRQAKgQARgIQAVgKA+AAIA4ACQAgACAcAHQAiAJAZAPQAcARASAYQASAaAJAhQAJAggBAjQgCAjgMAhQgNAigYAdQhCBPiHgPQAFAIgBAYIgEAtQAAAZgPAWQgQAXgZACIgIABQgXAAgQgPgAgjiaQgRACgKAGQgMAIgBAMIgCBQQAAAWAKAKQALALAWAAQAhAAAPgFQAdgJAOgaQAOgagIgeQgIgegagOQgWgMgbAAIgPABg");
	this.shape_19.setTransform(-505.8,12.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#2E7DDD").s().p("AgcEcQgNgIgJgOQgPgZgBgqIgBjSQABitAEghQAEggAWgTQAUgRAYAAQAYAAASAUQATAVgBAjIgEGiQgCAugIAOQgFALgMAGQgMAHgVABIgFAAQgPAAgMgGg");
	this.shape_20.setTransform(-543.4,-2.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#2E7DDD").s().p("Aj/EzQgdgHgMgYQgGgMAAgQQAAgQAHgTQATgzCflDIAihTIAVgjQANgQAMgGQAfgMAmAHQATADAOAIQAQAJAHAMQAVAjC7HIQAMAZgIAXQgIAVgVAKQgXAKgbgJQgOgEgMgKQgNgKgIgQQgNgXgphmIjjABIgIATQgtBegQAWQgUAcgPAIQgKAGgOAAQgJAAgLgDgAgwADIBygBQgUgrgjhaIg7CGg");
	this.shape_21.setTransform(-588.9,-3.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-623.7,-42,1247.5,84);


(lib.movie2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2
	this.instance = new lib.nekozakana_3();
	this.instance.parent = this;
	this.instance.setTransform(-493.6,-215.8,1,1,2.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({rotation:-3,x:-512.1,y:-173.4},0).wait(5).to({rotation:2,x:-494.6,y:-213.8},0).wait(5).to({rotation:-0.2,x:-503,y:-196.1},0).wait(5).to({rotation:-3.2,x:-512.8,y:-171.7},0).wait(5).to({rotation:3,x:-490.7,y:-221.6},0).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-509.6,-215.8,945.3,448.7);


(lib.movie = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.nekozakana_2();
	this.instance.parent = this;
	this.instance.setTransform(-451.7,-219.4,1,1,-1.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({rotation:2,x:-438.9,y:-244.1},0).wait(5).to({rotation:-2.7,x:-457.3,y:-207.7},0).wait(5).to({rotation:2.7,x:-435.5,y:-250.1},0).wait(5).to({rotation:-3,x:-458.3,y:-205.4},0).wait(5).to({rotation:3.7,x:-431.2,y:-257.4},0).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-451.7,-238.5,903.6,476.1);


// stage content:
(lib.opening = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(202));

	// タイトル
	this.instance = new lib.トゥイーン1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(658,703);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.instance_1 = new lib.トゥイーン2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(658,703);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},147).to({state:[{t:this.instance_1}]},8).wait(47));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(147).to({_off:false},0).to({_off:true,alpha:1},8).wait(47));

	// ねこざかな
	this.instance_2 = new lib.nekozakana_1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(114,51);

	this.instance_3 = new lib.movie();
	this.instance_3.parent = this;
	this.instance_3.setTransform(637.1,319,1,1,0,0,0,0.1,-0.6);

	this.instance_4 = new lib.nekozakana_2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(189,93);

	this.instance_5 = new lib.movie2();
	this.instance_5.parent = this;
	this.instance_5.setTransform(598,328,1.078,1.078,0,0,0,-37,8.5);

	this.instance_6 = new lib.nekozakana_3();
	this.instance_6.parent = this;
	this.instance_6.setTransform(96,106,1.073,1.073);

	this.instance_7 = new lib.nekozakana_4();
	this.instance_7.parent = this;
	this.instance_7.setTransform(69,83,1.222,1.222);

	this.instance_8 = new lib.nekozakana_5();
	this.instance_8.parent = this;
	this.instance_8.setTransform(102,111,1.132,1.132);

	this.instance_9 = new lib.nekozakana_6();
	this.instance_9.parent = this;
	this.instance_9.setTransform(152,151,1.045,1.045);

	this.instance_10 = new lib.nekozakana_7();
	this.instance_10.parent = this;
	this.instance_10.setTransform(38,118,1.116,1.116);

	this.instance_11 = new lib.nekozakana_8();
	this.instance_11.parent = this;
	this.instance_11.setTransform(77,118,1.146,1.146);

	this.instance_12 = new lib.nekozakana_9();
	this.instance_12.parent = this;
	this.instance_12.setTransform(-54,115,1.168,1.168);

	this.instance_13 = new lib.nekozakana_10();
	this.instance_13.parent = this;
	this.instance_13.setTransform(-122,128,1.183,1.183);

	this.instance_14 = new lib.nekozakana_11();
	this.instance_14.parent = this;
	this.instance_14.setTransform(62,84,1.135,1.135);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},20).to({state:[{t:this.instance_4}]},23).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_6}]},22).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_8}]},7).to({state:[{t:this.instance_9}]},8).to({state:[{t:this.instance_9}]},8).to({state:[{t:this.instance_10}]},8).to({state:[{t:this.instance_11}]},8).to({state:[{t:this.instance_12}]},8).to({state:[{t:this.instance_13}]},9).to({state:[{t:this.instance_14}]},9).to({state:[{t:this.instance_14}]},18).wait(47));

	// レイヤー 6
	this.instance_15 = new lib.トゥイーン3("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(664.5,700.4);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(155).to({_off:false},0).to({y:644,alpha:0},29).to({y:606.4},8).wait(10));

	// bg
	this.instance_16 = new lib.nekozakana_base();
	this.instance_16.parent = this;
	this.instance_16.setTransform(0,638);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#99C2E6").s().p("EhoRACCIAAkNMDQkAAKIAAENg");
	this.shape.setTransform(667.5,737);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_16}]}).to({state:[{t:this.shape},{t:this.instance_16}]},155).wait(47));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(667,426,1335,700);
// library properties:
lib.properties = {
	width: 1334,
	height: 750,
	fps: 24,
	color: "#F3F2EB",
	opacity: 1.00,
	manifest: [
		{src:"images/opening_atlas_.png?1489341898628", id:"opening_atlas_"},
		{src:"sound/ji_037.mp3?1489341898697", id:"ji_037"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;