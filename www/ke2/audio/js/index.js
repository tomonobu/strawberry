/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var sound = [];
var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        // 画面を横にロック
        screen.lockOrientation('portrait');
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {

        /*
         * sound set
         */
        var path;
        if (device.platform == 'iOS') {
            path = "ke2/audio/sound/";
        }
        else {
            path = cordova.file.applicationDirectory + "www/ke2/audio/sound/";
        }

        var mediaSuccess = function () {
            console.log("play complate");
        }
        var mediaError = function (e) {
            console.log("error : ");
            console.log(e);
        }

        var mediaStatus = function (status) {

            if (status != Media.MEDIA_NONE) {
                // (メディアの状態が初期状態でなくなったとき)
                console.log("media: none");
            }
            if (status == Media.MEDIA_STARTING) {
                // 再生が開始されたタイミング
                console.log("media: starting");
            }
            if (status == Media.MEDIA_RUNNING) {
                // 再生が開始されたタイミング
                console.log("media: running");
            }
            if (status == Media.MEDIA_PAUSED) {
                // 再生が開始されたタイミング
                console.log("media: paused");
            }
            else if (status == Media.MEDIA_STOPPED) {
                // 停止された時の処理
                console.log("media: stopped");
            }
            console.log('==============================================');
        }

        sound[1] = new Media(path + "KE2_chapter1.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[2] = new Media(path + "KE2_chapter2.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[3] = new Media(path + "KE2_chapter3.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[4] = new Media(path + "KE2_chapter4.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[5] = new Media(path + "KE2_chapter5.mp3", mediaSuccess, mediaError, mediaStatus);
        sound[6] = new Media(path + "KE2_chapter6.mp3", mediaSuccess, mediaError, mediaStatus);


        (function ($) {
            $(function () {
                $('.btn_a').on('touchstart', function () {
                    $('img', this).addClass('on');
                });
                $('.btn_a').on('touchend', function () {
                    $('img', this).removeClass('on');
                });
            });

            $(document).on('pagecontainerchange', function (e, d) {
                $('.active').removeClass('active');
                for (i = 1; i < 7; i++) {
                    sound[i].stop();
                    sound[i].release();
                }
                console.log(d);
                $(".sound1").on('click', function () {
                    if ($(this).hasClass('active')) {
                        sound[1].pause();
                        $(this).removeClass('active').text('再生');
                    }else{
                        sound[1].play();
                        $(this).addClass('active').text('停止');
                    }

                });
                $(".sound2").on('click', function () {
                    if ($(this).hasClass('active')) {
                        sound[2].pause();
                        $(this).removeClass('active').text('再生');
                    }else{
                        sound[2].play();
                        $(this).addClass('active').text('停止');
                    }
                });
                $(".sound3").on('click', function () {
                    if ($(this).hasClass('active')) {
                        sound[3].pause();
                        $(this).removeClass('active').text('再生');
                    }else{
                        sound[3].play();
                        $(this).addClass('active').text('停止');
                    }
                });
                $(".sound4").on('click', function () {
                    if ($(this).hasClass('active')) {
                        sound[4].pause();
                        $(this).removeClass('active').text('再生');
                    }else{
                        sound[4].play();
                        $(this).addClass('active').text('停止');
                    }
                });
                $(".sound5").on('click', function () {
                    if ($(this).hasClass('active')) {
                        sound[5].pause();
                        $(this).removeClass('active').text('再生');
                    }else{
                        sound[5].play();
                        $(this).addClass('active').text('停止');
                    }
                });
                $(".sound6").on('click', function () {
                    if ($(this).hasClass('active')) {
                        sound[6].pause();
                        $(this).removeClass('active').text('再生');
                    }else{
                        sound[6].play();
                        $(this).addClass('active').text('停止');
                    }
                });
            });


        })(jQuery);



    }
};

app.initialize();

